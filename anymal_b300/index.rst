ANYmal B300 User Manual
=======================

.. figure:: docs/anymal_b300/images/product_description.png
   :width: 18cm
   :align: center

   ANYmal B300


.. toctree::
   :name: toctop
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/introduction
   docs/anymal_b200/safety
   docs/anymal_b300/product_description
   docs/anymal_b300/specifications

.. toctree::
   :name: tochard
   :caption: Robot
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/anymal_b300/system_overview
   docs/anymal_b300/installation
   docs/anymal_b200/operation
   docs/anymal_b200/network
   docs/anymal_b100/operator_pc
   docs/anymal_b200/onboard_computers
   docs/anymal_b200/rpsm
   docs/anymal_b100/hri_remote_controller
   Main Body <docs/anymal_b300/main_body>
   Legs <docs/anymal_b200/leg>
   Feet <docs/anymal_b200/foot>
   Battery Pack <docs/anymal_b300/battery_pack>
   Troubleshooting <docs/anymal_b100/troubleshooting>

.. toctree::
   :name: tocmodule
   :caption: Modules
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   IMU  <docs/anymal_b200/imu_xsens_mti100>
   LiDAR Unit <docs/anymal_b300/velodyne>
   Depth Camera <docs/anymal_b300/realsense_D435>
   RTK GPS <docs/anymal_b200/rtk_gps>
   Microphone & Speakers <docs/anymal_b200/microphone_speakers>

.. toctree::
   :name: tocsoft
   :caption: Software & Simulation
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   Getting Started <docs/software/getting_started>
   docs/software/software_overview
   docs/software/installation
   docs/software/simulation
   docs/software/control_framework
   docs/software/navigation_stack
   docs/software/software_developing
   docs/software/catkin_howto
   Troubleshooting <docs/software/software_troubleshooting>

.. toctree::
   :name: tocmodulessoft
   :caption: Software Packages
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/software/model
   docs/software/lowlevel_controller
   docs/software/locomotion_control
   docs/software/free_gait
   docs/software/state_estimation
   docs/software/terrain_mapping
   docs/software/icp_localization_and_mapping
   docs/software/joystick

.. toctree::
   :name: tocappenix
   :caption: Appendix
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/software/repositories
   docs/anymal_b200/components
