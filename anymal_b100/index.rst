ANYmal B100 User Manual
=======================

.. figure:: docs/anymal_b100/images/anymal_bedi.jpg
   :scale: 40 %
   :align: center

   ANYmal Bedi


.. toctree::
   :name: toctop
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/introduction
   docs/anymal_b100/safety
   docs/anymal_b100/product_description
   docs/anymal_b100/specifications

.. toctree::
   :name: tochard
   :caption: Robot
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/anymal_b100/system_overview
   docs/anymal_b100/installation
   docs/anymal_b100/operation
   docs/anymal_b100/network
   docs/anymal_b100/operator_pc
   docs/anymal_b100/onboard_computers
   docs/anymal_b100/rpsm
   docs/anymal_b100/hri_remote_controller
   Main Body <docs/anymal_b100/main_body>
   Legs <docs/anymal_b100/leg>
   Feet <docs/anymal_b100/optofoot>
   Troubleshooting <docs/anymal_b100/troubleshooting>

.. toctree::
   :name: tocmodule
   :caption: Modules
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   IMU  <docs/anymal_b100/imu_xsens_mti100>
   Actuated LIDAR Unit <docs/anymal_b100/hokuyo>
   MultiSense S7 <docs/anymal_b100/multisense_s7>

.. toctree::
   :name: tocsoft
   :caption: Software & Simulation
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   Getting Started <docs/software/getting_started>
   docs/software/software_overview
   docs/software/installation
   docs/software/simulation
   docs/software/control_framework
   docs/software/navigation_stack
   docs/software/software_developing
   docs/software/catkin_howto
   Troubleshooting <docs/software/software_troubleshooting>

.. toctree::
   :name: tocmodulessoft
   :caption: Software Packages
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/software/model
   docs/software/lowlevel_controller
   docs/software/locomotion_control
   docs/software/free_gait
   docs/software/state_estimation
   docs/software/terrain_mapping
   docs/software/icp_localization_and_mapping
   docs/software/joystick


.. toctree::
   :name: tocappenix
   :caption: Appendix
   :glob:
   :hidden:
   :maxdepth: 2
   :numbered:

   docs/software/repositories
   docs/anymal_b100/components
