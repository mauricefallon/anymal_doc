# ANYmal Documenation

This repo holds the documentation of ANYmal.

[![Documentation Status](https://readthedocs.com/projects/anybotics-anymal-doc/badge/?version=stable)](https://anybotics-anymal-doc.readthedocs-hosted.com/en/stable/?badge=stable)

**Author(s):** Christian Gehring, Péter Fankhauser

