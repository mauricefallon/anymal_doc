.. _system_overview_b300:

System Overview
===============

This section provides an overview of the platform.


Robot Power and System Management
---------------------------------

In :numref:`fig_rpsm_overview1_b300` a diagram of the Robot Power and System Management (RPSM) is shown.
A more detailed description can be found in section ":ref:`rpsm_b200`".

.. _fig_rpsm_overview1_b300:

.. figure:: images/rpsm_overview.png
     :width: 18cm
     :alt: rpsm overview
     :align: center

     Diagram of the Robot Power and System Management (RPSM)


Computers and Network
---------------------

*ANYmal* is equipped with three **onboard computers**:

-  **Locomotion PC** (``anymal-NAME-lpc``)
-  **Navigation PC** (``anymal-NAME-npc``)
-  **Application PC** (``anymal-NAME-apc``)

These computers are connected to an **onboard network** over Ethernet and via WiFi to an **Operator PC** (``anymal-NAME-opc``).

:numref:`fig_computers_overview_b300` shows the computers and the networks and the task of each computer.

.. _fig_computers_overview_b300:

.. figure:: images/system_overview.png
     :scale: 50 %

     Overview of computers and networks


The **roscore** runs on the `Locomotion PC` as a system service and is started automatically at bootup. Read section ":ref:`operator_pc`" and ":ref:`onboard_computers`" to get more information on how to set up the computers, and have a look at section ":ref:`network`".


Top Plate Interfaces
--------------------

:numref:`fig_top_plate_interfaces_b300` indicates

- Three `WiFi antennas` for communication with the onboard network
- The `Power` and `E-Stop` button
- The charging socket
- The LiDAR Unit power and data socket
- The network socket for direct access to the onboard network
- The mainboard socket for direct access to the RPSM mainboard.

In addition, optional sockets like LED socket, GPS antenna socket, Microphone socket and XBEE antenna socket can be found depending on the configuration of the robot.

.. _fig_top_plate_interfaces_b300:

.. figure:: images/top_plate_interfaces.png
    :align: center

    Top plate interfaces.


Leg Connectors
--------------

The connectors to the legs are displayed in :numref:`fig_leg_connectors_b300` and show the cables for **12V fan power**, **48V drive power** and **EtherCAT**.

.. _fig_leg_connectors_b300:

.. figure:: images/leg_connectors.png
    :scale: 50 %
    :align: center

    Connectors for **12V fan power**, **48V drive power** and **EtherCAT** (from left).


Antennas
--------

:numref:`fig_top_plate_interfaces_b300` indicates the three Wifi antennas while :numref:`fig_hri_antenna_b300` shows the antenna of the remote controller.


.. _fig_hri_antenna_b300:

.. figure:: images/hri_antenna.jpg
    :width: 10cm
    :align: center

    The antenna of the remote controller.

.. important::
  .. image:: ../images/iso_7010/warning_electricity.png
    :scale: 50 %
    :align: left

  **The battery ground is exposed at the antennas!**
  **Do not touch or make contact with them.**
