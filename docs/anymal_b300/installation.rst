.. _installation_b300:

Installation
============

.. important::

  .. image:: ../images/iso_7010/mandatory_manual.png
    :scale: 50%
    :align: left

  **Carefully read the safety instruction in section** ":ref:`safety_instructions_b200`" **before installing or operating ANYmal.**


First Steps
-----------
#. Unpack your robot and check that you have received all accessories mentioned in the accompanied datasheet of your robot.
#. Connect the battery to the onboard system of *ANYmal* according to the guidelines in section ":ref:`installing_battery_pack_b300`".
#. Charge your robot according to the instructions in section ":ref:`battery_charging_b200`".
#. Read section ":ref:`network_b200`" to understand the network configuration.
#. Follow section ":ref:`operator_pc_setup`".


Using a Safety Harness
----------------------

It is highly recommended, especially during development, to operate *ANYmal* only when attached to a safety harness.
*ANYmal* is equipped with two dedicated eyebolts fixed to the handles as shown in :numref:`fig_hooks_b300`.


.. _fig_hooks_b300:

.. figure:: ../anymal_b100/images/hooks.png
    :width: 10cm
    :align: center

    Hooks for mounting a safety harness.


To attach *ANYmal* to a crane or a rope preferably use carabiners and a crossbeam with a width between 440 and 540mm and vertical ropes with a minimum length of 200mm, as shown on the left in :numref:`fig_safety_harness_b300`.


.. _fig_safety_harness_b300:

.. figure:: images/safety_harness.png
    :width: 10cm
    :align: center

    Left: Correct dimensions of the safety harness. Right: Incorrect safety harness which may damage the handles.

The right image of :numref:`fig_safety_harness_b300` shows an incorrect usage of the safety harness, which induces large transverse forces on the handles. During a fall, these forces can permanently bend the handles or rip them out of the trunk.



.. caution::

  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  - *Hang the robot only at the dedicated eyebolts.*
  - *Make sure that there is only traction and no torsional load on the hooks.*
  - *Always use a crossbeam with a length between 440 and 540mm on the safety harness.*
  - *Do not exceed a maximum load of 40kg on each eyebolt.*


Storing *ANYmal* in the Transportation Box
------------------------------------------

For a safe transportation *ANYmal* comes in a transportation box.

.. important::
    .. image:: ../images/iso_7010/mandatory_general.png
      :width: 2cm
      :align: left

    - Charge the battery to approximately 50% (see section ":ref:`battery_charging_b200`") prior to storing *ANYmal* in the transportation box.
    - Two persons are required to put the robot into the box.


.. _fig_transportation_box_empty_b300:

.. figure:: images/transportation_box_empty.jpg
  :align: center

  Empty transportation box.

To store *ANYmal* in the transportation box:

#. Disconnect all cables from the robot.
#. Place the robot on the belly plate and orient the legs on the side of the main body with the knees pointing upwards as shown in :numref:`fig_transportation_box_filled_b300`.
#. Lift the robot and place it into the box.
#. Put the remote controller and accessories around to the left and right of the robot as shown in :numref:`fig_transportation_box_filled_b300`.
#. Put additional foam on the knees.
#. Close the box and make sure that there is a sticker on it indicating the orientation.


.. _fig_transportation_box_filled_b300:

.. figure:: images/transportation_box_filled.jpg
  :align: center

  ANYmal stored in the transportation box.
