.. _realsense_d435:

Depth Camera
============

The depth camera mounted on *ANYmal* is an Intel RealSense D435.
Download the `product datasheet <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b300/documents/Intel_RealSense_Depth_Cam_D400_Series_Datasheet.pdf>`__ for information regarding specifications.


:numref:`fig_realsense_d435` shows the device installed in the front of *ANYmal*.

.. _fig_realsense_d435:

.. figure:: images/realsense_d435.png
   :width: 10cm
   :alt: realsense
   :align: center

   Intel RealSense D435 mounted on ANYmal.


.. caution::
  .. image:: ../images/iso_7010/warning_electricity.png
    :scale: 50 %
    :align: left

  - **The battery ground is exposed at the chassis of the RealSense!**
  - **It must be isolated from the main body chassis by the plastic mount.**
  - **Do not bypass the isolation by touching the RealSense.**

.. warning::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50 %
    :align: left

  **The RealSense depth camera is not waterproof.**


Overview
--------

The depth camera is mounded on the perception module on the front upper hatch of ANYmal. :numref:`fig_perception_module_b300` shows the components of the perception module:

- The RealSense depth camera, mounted at 0 degree pitch angle
- The microphone (optional, depends on the configuration)
- Ethernet socket 1, connected to port eth3 of the NPC
- Ethernet socket 2, connected to port eth4 of the NPC
- USB 3.0 socket 1, connected to the NPC
- USB 3.0 socket 2, connected to the NPC
- 12V socket


.. _fig_perception_module_b300:

.. figure:: images/perception_module.png
   :scale: 50 %
   :alt: realsense
   :align: center

   Perception module with Intel Realsense D435.

:numref:`fig_perception_module_b300` shows the pin-out of the 12V socket on the perception module.

.. _fig_perception_module_12Vout_b300:

.. figure:: images/perception_module_12Vout.png
   :scale: 50 %
   :alt: realsense
   :align: center

   Pin out of the perception module 12V socket.



Installation
------------

Hardware
~~~~~~~~

#. Mount the Intel RealSense D435 on the intended frame at the front of *ANYmal* using two screws **M3x4** appropriate washers. Tighten the screws with **0.5 Nm**.

#. Adjust the frame's tilt angle to your needs. For adjustment, open the two screws **M5x10** of the adjustment mechanism (:numref:`fig_realsense_adjustment_b300`). There is an indicator printed on the frame where you can read the current angle. After adjustment, tighten the screws **M5x10** with **2 Nm**.

.. _fig_realsense_adjustment_b300:

.. figure:: images/realsense_adjustment.png

   :scale: 50 %
   :alt: realsense
   :align: center

    Angle adjustment mechanism of the RealSense frame.

#. Connect the device to USB socket 1 on the front hatch using a USB 3.0 cable.

Software
~~~~~~~~

The Intel Realsense D435 requires a special driver which has to be installed manually.
`Intel's official repository <https://github.com/IntelRealSense/librealsense>`__ contains a `documentation <https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md>`__ on how to install it.
Read and follow the instructions carefully since the installation requires modifications to the Linux Kernel.

The following setup is confirmed to be working on *ANYmal*:

   - Install Kernel 4.8.0-58 with lowlatency patch

    .. code:: bash

        sudo apt install linux-headers-4.8.0-58-lowlatency linux-image-4.8.0-58-lowlatency

After the installation of the driver, clone the `realsense <https://github.com/intel-ros/realsense/>`__ and
`anymal_navigation <https://bitbucket.org/leggedrobotics/anymal_navigation>`__ repositories into your workspace.
Run

    .. code:: bash

        catkin build realsense2_camera anymal_realsense

Calibration
-----------

#. Clone the `repository <https://github.com/ethz-asl/kinect_tilt_calibration>`__ containing the calibration routine to the NPC's catkin workspace and build it by running

    .. code:: bash

        catkin build kinect_tilt_calibration

#. Run *ANYmal* locomotion and the navigation PCs.

   #. On LPC:

       .. code:: bash

           roslaunch anymal_NAME_lpc lpc.launch

   #. On NPC:

       .. code:: bash

           roslaunch anymal_NAME_npc npc.launch

   #. Place *ANYmal* in the Default configuration on a flat terrain without any obstacles in the field of view of the depth sensor.

#. Run the calibration as follows:

   #. Edit the :code:`kinect_tilt_calibration/launch/example.launch` file:
      Change the topic to :code:`/realsense_D435/points2`, and the frame to :code:`realsense_D435_camera`.

   #. Run the calibration algorithm on the NPC and read the output angle (:code:`pitch deg`) in the console:

       .. code:: bash

           roslaunch kinect_tilt_calibration example.launch

   #. Open :code:`anymal_NAME/anymal_NAME_description/urdf/anymal.urdf.xacro` and set the :code:`mounted_angle_degree` appropriately.

Software
--------

The :code:`anymal_realsense` package in the `anymal_navigation <https://bitbucket.org/leggedrobotics/anymal_navigation>`__ repository provides a launch file which connects to the Realsense.

Troubleshooting
---------------

As the Intel RealSense D435 uses practically the complete bandwidth of USB 3.0, the connection cable should be short and of good quality.
There is a bug that on rare occasions needs a manual reconnection of the realsense when restarting the nodes.
