Product Description
===================

*ANYmal* shown in :numref:`figproduct_description_anymal_b300` is a four-legged industrialized robot platform, capable to move and operate autonomously in challenging terrain while interacting safely with the environment. As a multi-purpose robot platform it is applicable on industrial indoor or outdoor sites for inspection and manipulation tasks, in natural terrain or debris areas for search and rescue tasks, or on stage for animation and entertainment. Its four legs allow the robot to crawl, walk, run, dance, jump, climb, carry – whatever motion the task requires.

.. _figproduct_description_anymal_b300:

.. figure:: images/product_description.png
    :width: 18cm
    :align: center

    Features of ANYmal B300

    
Typical skill features of the *ANYmal* platform include:

- **Outdoor operation:** a ruggedized design allows for operation under harsh conditions
- **Single operator:** with a weight of approximately 33 kg, the machine can be safely handled by a single operator
- **Extreme mobility:** all joints can be rotated +/-180°
- **Fast locomotion:** compliant actuation allows for dynamic gaits up to 1.0 m/s
- **Long endurance:** onboard batteries ensure about 2-4 h operation
- **High autonomy:** LIDAR sensors provide localization and mapping
