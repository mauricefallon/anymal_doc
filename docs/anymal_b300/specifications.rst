Specifications
==============

Technical Data
--------------

The specifications of the platform without external perception modules are listed in :numref:`table_technical_data`.

.. _table_technical_data:

.. table:: Technical data

  +---------------------------------+--------------------------+
  | **Minimal size (lying)**        | 800 x 600 x 400 mm       |
  +---------------------------------+--------------------------+
  | **Operating size (standing)**   | 800 x 600 x 700 mm       |
  +---------------------------------+--------------------------+
  | **Weight**                      | ~33 kg                   |
  +---------------------------------+--------------------------+
  | **Maximum payload** [#]_        | 10 kg                    |
  +---------------------------------+--------------------------+
  | **Speed (max)**                 | 1.0 m/s                  |
  +---------------------------------+--------------------------+
  | **Average power consumption**   | ~300 W                   |
  +---------------------------------+--------------------------+
  | **Operating time**              | 2 hours                  |
  +---------------------------------+--------------------------+
  | **Charging time**               | 6 hours                  |
  +---------------------------------+--------------------------+
  | **Temperature range**           | 0° - 25°C                |
  +---------------------------------+--------------------------+

.. [#] Continuous operation on relatively flat terrain with static walk

The range of motion of the joints of the legs are defined in :numref:`table_rom`. More information about the joint names and definitions can be found in section :ref:`model_state`.

.. _table_rom:

.. table:: Range of motion for left fore leg

  +--------+---------------+-------------------+
  | Joint  | Range         | Condition         |
  +========+===============+===================+
  | HAA    | [-45°, +90°]  |                   |
  +--------+---------------+-------------------+
  | HFE    | [-360°,+360°] | HAA <= 0°         |
  |        |               |                   |
  |        | [-90°,+95°]   | HAA in [0°,+30°]  |
  |        |               |                   |
  |        | [-90°,+85°]   | HAA in [+30°,+45°]|
  |        |               |                   |
  |        | [-90°,+30°]   | HAA in [+45°,+90°]|
  +--------+---------------+-------------------+
  | KFE    | [-360°,+360°] |                   |
  +--------+---------------+-------------------+


.. _load_cases:

Load Cases
----------

*ANYmal* is designed to withstand most of the common falls on flat terrain without damage due to the robust main body design, the compliant actuators and the protective belly plate. However, due to weight limitations, the outer hull is not fully protected. Especially edges and protruding objects on the ground can damage *ANYmal* while falling, for example in rocky environment. Furthermore, no protection can be guaranteed at falling heights larger than 0.5m, for example when falling down stairs or steps. Softer ground like wood or soil on the other hand increases the chance of harmless falling even in rough terrain and at greater fall heights.

.. danger::
    .. image:: ../images/iso_7010/warning_general.png
       :width: 2cm
       :align: left

    **Be aware of fast moving parts (especially the legs) and parts coming off the robot in the case of a crash.**
    **Always comply with the safety instructions in section** ":ref:`safety_instructions_b200`".

The protective elements of *ANYmal* should protect the robot from the following load cases:

#. Collapsing from standing/walking on flat concrete with the belly plate touching the ground, as long as no foot is below the belly plate. This should produce no damages even when occurring on a regular base.
#. Collapsing from standing/walking on a 90degree edge (for example step) with an edge radius >20mm or on a pipe with a diameter of >80mm. This may damage the belly plate, but should leave the main body undamaged.
#. Falling forward/backwards on flat concrete with the hip drives and the LIDAR unit / the camera frame touching the ground. This should produce no damages when occurring occasionally.
#. Falling diagonally on flat concrete with one hip touching the ground. This should produce no damages. It is however recommended to carefully check the affected leg for mechanical damage before continuing operation.
#. Falling sideways on flat concrete. Due to high accelerations, this may damage the electronics of the main body and/or additional equipment mounted on top of the robot. However, the mechanical structure should be able to withstand such a fall without damage.
#. Rolling over on flat concrete after falling. This should produce no damages, since the two handles protect the components on top of *ANYmal*.

The thickness of the protective foam around the body and legs of *ANYmal* should limit the accelerations of the aforementioned load cases below 50g. 

.. important::
    .. image:: ../images/iso_7010/mandatory_general.png
      :width: 2cm
      :align: left

    The protection for all load cases listed above applies only if all hatches of *ANYmal* are closed and all protectors (namely belly plate, handles, LiDAR Unit protectors, sensor protection frames, side hatch foam, hip pads, hip pads, knee pads, and shank pads) are properly attached. If any hatch is open, the structural integrity of the main body cannot be guaranteed during any fall. If protectors containing foam are missing, high accelerations during impact may damage the robot during any fall.
  

.. note::

   If *ANYmal* falls on one of its feet while collapsing from standing/walking, extreme peak forces in the leg structure inevitably occur. To protect the structure, the carbon tube of the foot is designed as a predetermined breaking point to prevent damage on the more expensive parts of the structure.
   
  
Standards
---------

The described system has a **Technology readiness level of 7** according to the *European Commission* [EC2014]_, which means that the system is compliant with a *system prototype demonstration in operational environment*.


.. [EC2014] "Technology readiness levels (TRL)". European Commission, G. Technology readiness levels (TRL), HORIZON 2020 – WORK PROGRAMME 2014-2015 General Annexes, Extract from Part 19 - Commission Decision C(2014)4995.
