.. role:: cpp(code)
   :language: cpp

.. role:: bash(code)
   :language: bash


Introduction
============

Intended Purpose
----------------

The purpose of the present document is to provide specific information to cover particular cases or scenarios that might come in handy during commissioning of your quadruped robot *ANYmal* [Hutter16]_.

.. [Hutter16] Hutter, M., Gehring, C., Jud, D., Lauber, A., Bellicoso, C. D., Tsounis, V., Hwangbo, J., Fankhauser, P., Bloesch, M., Diethelm, R., and Bachmann, S. (2016). “ANYmal - A Highly Mobile and Dynamic Quadrupedal Robot”. IEEE/RSJ Intenational Conference on Intelligent Robots and Systems (IROS).


Target Audience
---------------

This document is intended only for trained and skilled personnel working with the equipment described.
It conveys information on how to understand and fulfill the respective work and duties.
This document is a reference document only and does not replace a proper training.
It does require particular knowledge and expertise specific to the equipment described.


Copyright
---------

Copyright (c) 2017, ANYbotics AG, All rights reserved. All changes are reserved.

The present document – including all parts thereof – is protected by copyright. Any use (including reproduction, translation, microfilming and other means of electronic data processing) beyond the narrow restrictions of the copyright law without the prior approval of ANYbotics AG, is not permitted and subject to persecution under the applicable law.

|     **ANYbotics AG**
|     Leonhardstrasse 21, LEE H303
|     CH-8092 Zurich
|
|     support-anymal@anybotics.com
|     http://support.anybotics.com
|     http://www.anybotics.com
