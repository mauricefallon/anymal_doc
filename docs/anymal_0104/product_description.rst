Product Description
===================

*ANYmal* shown in :numref:`_figproduct_description_anymal_0104` is a four-legged industrialized robot platform, capable to move and operate autonomously in challenging terrain while interacting safely with the environment. As a multi-purpose robot platform it is applicable on industrial indoor or outdoor sites for inspection and manipulation tasks, in natural terrain or debris areas for search and rescue tasks, or on stage for animation and entertainment. Its four legs allow the robot to crawl, walk, run, dance, jump, climb, carry – whatever motion the task requires.

.. _figproduct_description_anymal_0104:

.. figure:: images/product_description.png
    :scale: 100 %

    Features of ANYmal


Different capability levels of the *ANYmal* platform can be provided – be it a minimally equipped platform, capable to walk or run in remote control mode, or a typical autonomous platform that can be equipped for specific applications. Hardware and software interfaces are provided for a number of sensors, actuators or other accessories.

Typical skill features of a standard *ANYmal* platform include:

- **Outdoor operation:** a ruggedized design allows for operation under harsh conditions
- **Single operator:** with a weight approximately 30 kg, the machine can be safely handled by a single operator
- **Extreme mobility:** all joints can be rotated +/-180°
- **Fast locomotion:** compliant actuation allows for dynamic gaits up to 1.0 m/s
- **Long endurance:** onboard batteries ensure about 2-4 h operation
- **High autonomy:** LIDAR sensors provide localization and mapping
