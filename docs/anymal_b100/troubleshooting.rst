Troubleshooting Information for *ANYmal*
========================================

This section contains technical information that might be useful when trying to solve a problem.

Catkin overlay of integration workspace does not work for developer user
------------------------------------------------------------------------

The primary group of the user needs to be **developers**. Check the primary group with: ``id -gn``.


If you use `gnome-terminal` to login to an onboard computer via SSH, please activate `"Run command as login shell"` in your shell.
Otherwise, the file ``~/.profile`` will not be sourced and your catkin workspace is not setup correctly!

.. _timing_issues:

Timing issues
-------------

Check the time offset from a computer to the `Locomotion PC` (e.g. anymal-beth-lpc) which serves as a master:

.. code:: bash

    ntpdate -q anymal-NAME-lpc

If the time offset is too large, `chrony`, which synchronizes the time of your computer with the time of the `Locomotion PC` has an issue.
The state of `chrony` on your computer can be checked with

.. code:: bash

    chronyc tracking

If it is malfunctioning, restart `chrony` on your computer with

.. code:: bash

    sudo service chrony restart

Otherwise, check if `chrony` is set up correctly on your as well as the `Locomotion PC` (see section :ref:`settingup_time_synchronization`)

Actuator overheating
---------------------

If an *ANYdrive* overheats it is automatically is switched off (fatal state, see `docs.leggedrobotics.com/anydrive_doc <http://docs.leggedrobotics.com/anydrive_doc/>`_).
To ensure that the robot does not fall to the side all other drives are set into the standby state.
With all the drives shut off the robot will collapse vertically.

In case of overheating of an overheating actuator follow this procedure:

#. Press the soft e-stop of the robot.
#. If possible hoist the robot up.
#. Do not shut the robot down to keep the air circulating inside the main body.
#. Wait until the drive is cooled off.

.. warning::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **Do not attempt to restart** *ANYmal* **before the drive has cooled off.**
   **Do not use water or ice spray to cool the actuator down.**

Serial Connection crash
-----------------------

In case of an interrupted serial connection, often detected by the outdated rpsm GUI, the easiest fix is to restart the daemon. To do so connect to the `Locomotion PC` and enter the following command in the bash:

.. code:: bash

   sudo service anymal-ros-rpsm restart


Optoforce driver shows multiple errors
--------------------------------------
The thirdparty library from Optoforce has sometimes issues at startup. Try to re-start the launch file:

.. code:: bash

    roslaunch anymal_NAME_lpc optoforce_sensors.launch

Water in the main body
----------------------

If there is water in the main body

#. Shut down the robot and disconnect the battery (see section :ref:`disconnecting_battery_pack`).
#. Remove the water with a towel.
#. Check the hatches and seals for possible leakage.
#. Reconnect the battery.

