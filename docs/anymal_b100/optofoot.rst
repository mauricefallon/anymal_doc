.. _optofeet:

Feet of *ANYmal*
================

The foot of *ANYmal* is shown in :numref:`fig_foot` and contains of a rubber foot with integrated force sensor and a carbon tube with integrated connector socket.

.. _fig_foot:

.. figure:: images/foot_with_text.jpg
    :scale: 50 %
    :align: center

    Rubber foot with force sensor and carbon tube.



.. _feet_force_sensors:

Force Sensor
------------

The feet have an integrated :ref:`Optoforce_OMD_45_FH_2000N` force sensor, which measures a three-dimensional force vector. The sensor provides analog signals, which need to be converted by a DAQ.

.. note:: The grounding of the sensor is wired through the shielding of the analog cable of the Optoforce sensor. Theferore, make sure that the shield is properly connected.

The driver and a ROS wrapper are provided by the software package `optoforce_omd`_.


.. _optoforce_omd: https://bitbucket.org/leggedrobotics/_optoforce_omd

Connecting the foot to the shank
--------------------------------

#. Loosen the **four M3x12mm screws** of the shank clamps to allow easy insertion of the carbon tube of the foot.

    .. figure:: images/shank_clamp_screws.jpg
      :scale: 50 %
      :align: center

#. Grab the edge of the foot connector insert with a bearing puller and remove the foot connector insert from the foot tube.

    .. figure:: images/foot_connector_insert.jpg
      :scale: 50 %
      :align: center

#. Insert the carbon tube of the foot into the shank.
#. The carbon tube of the foot and the aluminum structure of the shank feature grooves to allow for precise positioning. Insert a 2mm steel wire in two opposite grooves the in the foot tube, push the foot downwards until the wire hits the shak and align the wire with the groove in the shank. From the four possible foot tube grooves, select the one which allows the neighbour groove to point towards the robot and the foot bend to point outwards.

    .. figure:: images/foot_aligning.jpg
        :scale: 100 %
        :align: center

#. Slightly fasten the four clamp screws to hold the foot tube in position.
#. Plug in the foot connector and insert the connector insert into the foot tube. Be careful to not move the foot tube with respect to the shank.
#. Fasten the **four M3x12mm clamp screws** with **1.2-1.5Nm**.


Disconnecting the foot from the shank
-------------------------------------

#. Loosen the four screws of the shank clamps to allow removal of the foot connector insert.
#. Grab the edge of the foot connector insert with a bearing puller and remove the connector insert from the foot tube.
#. Reach the foot connector through the openings in the connector insert and unplug it.
#. Remove the foot tube from the shank.
