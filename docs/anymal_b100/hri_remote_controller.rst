
.. _remote_controller:

Remote Controller
=================

.. _hri_joystick_description:

.. figure:: images/hri_joystick_description.png
    :align: center

    Description of the buttons and axes of the HRI's Remote Controller

The robot is equipped with the HRI's `Safe Remote Control System`. More documentation of the device can be found here:

- `User manual <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/HRI_Remote_Controller_CDS-012-03_SRCS_User_Manual.pdf>`__
- `Online wiki <https://hriwiki.atlassian.net/wiki/display/DOC/SRCS+User+Manual>`__


Steering the Robot
------------------

The default functions of the buttons and axes are labelled in :numref:`hri_joystick_description`. Note that the behavior of the buttons and axes depend on the loaded module and can be changed by the user.

Navigating through the Menu
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The buttons on the left side are used to navigate in the displayed menu. The arrow ``>`` shown on the left on the screen visualizes the pointer in the menu. By using the buttons on the left, you can navigate through the menu with the pointer. The top line always shows the active module. If the pointer is at the top line, you can switch between the modules with the buttons ``<`` and ``>``. To select an option, move the menu pointer to the option and press button ``1`` on the right side of the controller.

Emergency Stopping
~~~~~~~~~~~~~~~~~~
The behavior of the **Soft and Hard E-Stop buttons** are described in section ":ref:`operation_emergency_stopping`".

.. caution::
    .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

    **If you drop the remote controller, a hard E-Stop is triggered and the robot will collapse!**


.. caution::
    .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

    **If the communication link between the robot and the remote controller is interrupted, for instance, when the controller is out of sight or farther away than 30 m, or the remote controller turns off, a hard E-Stop is triggered and the robot will collapse!**


Locomotion Control
~~~~~~~~~~~~~~~~~~

When a locomotion controller is active, the joystick axes can be used to send position or velocity commands as labelled in :numref:`hri_joystick_description`. Button ``4`` usually commands the controller to start walking, whereas button ``2`` commands it to go to a stand configuration.

.. note::

    The remote controller has higher priority than the joypad in the GUI of the operator PC. Thus, it can override the commands from the operator PC.


Software
--------

The driver for the HRI's remote controller can be found in package  ``hri_safety_sense`` of the repository `hri-safe-remote-control-system`_. The joystick driver is interfaced through an `ANYJoy module` via the package ``hri_user_interface`` in repository `anymal`_. Read section ":ref:`software_joystick`" for more information about the `ANYJoy framework`.

The relevant software repositories are:


-  `hri-safe-remote-control-system`_: HRI’s Safe Remote Control driver
-  `anymal`_: Package ``hri_user_interface`` to interface ANYJoy
-  `any\_joy`_: ANYJoy - Joystick manager


.. _hri-safe-remote-control-system: https://github.com/leggedrobotics/hri-safe-remote-control-system
.. _any\_joy: https://bitbucket.org/leggedrobotics/any_joy
.. _ANYJoy: http://docs.leggedrobotics.com/any_joy_doc
.. _anymal: https://bitbucket.org/leggedrobotics/anymal
