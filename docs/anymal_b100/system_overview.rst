System Overview
===============

This section provides an overview of the platform.


Robot Power and System Management
---------------------------------

In :numref:`fig_rpsm_overview1` a diagram of the Robot Power and System Management (RPSM) is shown.
A closer description can be found in :numref:`rpsm`.

.. _fig_rpsm_overview1:

.. figure:: images/rpsm_overview.png
     :width: 18cm
     :alt: rpsm overview
     :align: center

     Diagram of the Robot Power and System Management (RPSM)


Computers and Network
---------------------

*ANYmal* is equipped with three **onboard computers**:

-  **Locomotion PC** (``anymal-NAME-lpc``)
-  **Navigation PC** (``anymal-NAME-npc``)
-  **Application PC** (``anymal-NAME-apc``)

These computers are connected to an **onboard network** over Ethernet and via WiFi to an **Operator PC** (``anymal-NAME-opc``).

:numref:`fig_computers_overview` shows the computers and the networks and the task of each computer.

.. _fig_computers_overview:

.. figure:: images/system_overview.png
     :scale: 50 %

     Overview of computers and networks


The **roscore** runs on the `Locomotion PC` as a system service and is started automatically at bootup. Read section :ref:`operator_pc` and :ref:`onboard_computers` to get more information on how to set up the computers, and have a look at section :ref:`network`.


Buttons
-------

:numref:`fig_power_and_estop_buttons` indicates the `Power` and `E-Stop` button on the robot.

.. _fig_power_and_estop_buttons:

.. figure:: images/power_estop_buttons.png
    :width: 10cm
    :align: center

    Power and Hard E-Stop buttons.

.. _connectors:

Connectors
----------

:numref:`fig_top_connectors` indicates the charging socket, onboard Ethernet port and RPSM USB socket.

.. _fig_top_connectors:

.. figure:: images/top_connectors.png
    :width: 18cm
    :align: center

    Charging socket, onboard Ethernet port and RPSM USB socket.


:numref:`fig_sensor_connectors` shows the connectors for additional sensors.

.. _fig_sensor_connectors:

.. figure:: images/sensor_connectors.png
    :width: 18cm
    :align: center

    Connectors for additional sensors.



The connectors to the legs are displayed in :numref:`fig_leg_connectors`.

.. _fig_leg_connectors:

.. figure:: images/leg_connectors.png
    :width: 18cm
    :align: center

    Connectors for the leg.

The connector for the force sensor is shown in :numref:`fig_force_sensor_connector`.

.. _fig_force_sensor_connector:

.. figure:: images/force_sensor_connector.png
    :width: 10cm
    :align: center

    Connector for force sensor in foot.


Antennae
--------

:numref:`fig_wifi_antennae` shows the three 5GHz antennae for wireless communication with the onboard network, whereas :numref:`fig_hri_antenna` shows the antenna of the remote controller.


.. _fig_wifi_antennae:

.. figure:: images/wifi_antennae.png
    :width: 10cm
    :align: center

    The three 5GHz WiFi antennae.

.. _fig_hri_antenna:

.. figure:: images/hri_antenna.png
    :width: 10cm
    :align: center

    The antennae of the remote controller.