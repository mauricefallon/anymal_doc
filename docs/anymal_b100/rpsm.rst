.. _rpsm:

Robot Power and System Management
=================================

:numref:`fig_rpsm_overview` shows the diagram of the `Robot Power and System Management` (RPSM).
All components managed by the RPSM are listed in :numref:`tab_rpsm`.

.. _fig_rpsm_overview:

.. figure:: images/rpsm_overview.png
     :width: 18cm
     :alt: rpsm overview
     :align: center

     Diagram of the Robot Power and System Management (RPSM)

.. _tab_rpsm:

.. table:: Components of the Robot Power and System Management (RPSM)

  ================  ========================
  Component          Section
  ================  ========================
  Battery Pack      :ref:`battery_pack`
  Mainboard         :ref:`mainboard`
  Cooling Fans      :ref:`cooling_fans`
  Actuation Boards  :ref:`actuation_boards`
  RC Receiver       :ref:`remote_controller`
  Router            :ref:`network`
  Locomotion PC     :ref:`onboard_computers`
  Navigation PC     :ref:`onboard_computers`
  Application PC    :ref:`onboard_computers`
  Joint Actuators   :ref:`joint_actuators`
  Force Sensors     :ref:`feet_force_sensors`
  IMU               :ref:`imu`
  LIDAR Unit        :ref:`hokuyo`
  Multisense S7     :ref:`multisense_s7`
  ================  ========================

.. _battery_pack:

Battery Pack
------------

The `battery pack` includes a battery which consists of Lithium ion (Li-ION) cells and a Battery Management System (BMS). The BMS is protecting the battery from operating outside its safe operating area, is monitoring the battery's state, and balances it.

.. important::

  .. image:: ../images/iso_7010/mandatory_manual.png
    :scale: 50%
    :align: left

  * **Carefully read first** :numref:`safety_instructions` **before you handle the battery.**

  |


.. _installing_battery_pack:

Installing the Battery Pack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. caution::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  Do not disconnect the connectors labeled with an `A`.

  |

.. caution::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  If not connected always cover the open power cable connectors (yellow) with a cap or electrical tape.

#. Check that the robot is not powered and not connected to anything.

#. Move the battery pack into the main body from the front of the body and secure it with the screw as shown in :numref:`fig_mount_battery_pack`.

    .. _fig_mount_battery_pack:

    .. figure:: images/mount_battery_pack.png
      :width: 12cm
      :alt: battery pack mount
      :align: center

      Mounting screw for the battery pack

#. Connect the power connectors which are labeled with a `B` as shown in :numref:`fig_battery_pack_connectors`.

    .. _fig_battery_pack_connectors:

    .. figure:: images/battery_pack_connectors.png
        :width: 12cm
        :alt: battery pack mount
        :align: center

        Power connectors of the the battery pack.


#. Connect the I2C communication cable of the BMS with the RPSM mainboard via the I2C-switch on the side next to the battery.


.. _disconnecting_battery_pack:

Disconnecting the Battery Pack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To disconnect the battery pack undo the above mentioned tasks in the following order. Carefully handle the cables leading from the battery away and do not let them touch neither the case nor other parts of the robot.

#. Check that the robot is not powered and not connected to anything.

#. Unplug the I2C connection between the BMS and the RPSM mainboard.

#. Disconnect the power connectors which are labeled with a `B` as shown in :numref:`fig_battery_pack_connectors`. Secure the open connectors with a cap or electrical tape.

#. Remove the screw that fixes the battery to the body shown in :numref:`fig_mount_battery_pack`.

#. Move the battery pack out of the main body through the opening in the front of the body.


.. _mainboard:

Mainboard
---------

The `mainboard` is the central unit of the robot, which monitors and controls all devices of the robot. The `mainboard` is connected to the `Locomotion PC` via serial bus.


.. _actuation_boards:

Actuation Boards
----------------

The `Actuation Board` enables the switching of high side currents to demanding loads, which would be normally connected directly to the onboard battery. This makes the power consumption of actuators switchable and thus the system more flexible to react on environmental circumstances like low battery voltages, erroneous states and mission planned robot states like sleep modes.


.. _cooling_fans:


Cooling Fans
------------

.. _fig_coolingfan:

.. figure:: images/cooling_fan.jpg
    :scale: 20%
    :align: center

    Main cooling fan



There are several cooling fans inside the main body for air circulation and one at the top of the robot as shown in :numref:`fig_coolingfan` for cooling the overall system.

.. caution::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **Do not operate the robot if the cooling fan is not working. The system may overheat and components may get damaged.**


Temperatures inside the robot
-----------------------------

:numref:`tab_temperatures` lists typical and maximum operating temperatures of *ANYmal*. Typical values are only rough references for 25°C ambient temperature without direct sun radiation and slow walking/trotting. Detailed temperatures for the *ANYdrive* can be found in the `docs.leggedrobotics.com/anydrive_doc <ANYdrive documentation>`_ .

.. _tab_temperatures:

.. table:: Temperatures of *ANYmal*

  ==================  =============   ==========
  Location            Typical Temp.   Max. Temp.
  ==================  =============   ==========
  Inside main body    50°C            60°C
  CPU                 60°C            72°C
  Battery             50°C            60°C
  Thigh surface       50°C            60°C
  HFE cooler surface  50°C            60°C
  ==================  =============   ==========


RPSM Software
-------------

On each onboard computer, a ROS node (``rpsm_lpc``, ``rpsm_npc``, etc.) is running in the background as a daemon. The node ``rpsm_lpc`` on the `Locomotion PC` provides information about the `RPSM` because it communicates with the :ref:`mainboard` over a serial bus. The RQT-based GUI ``rqt_rpsm`` visualizes the data and provides actions to interact with the `RPSM`.


The software for interfacing the RPSM is in the follwing repository:

-  `rpsm\_software`_: Software interface for the Robot Power and System Management



.. _rpsm\_software: https://bitbucket.org/leggedrobotics/rpsm_software
