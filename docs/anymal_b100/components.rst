Hardware References
===================

This section provides some information about the installed componentes of *ANYmal* and refers you to further documentation. Have a look at the
datasheet that comes along with your *ANYmal* to figure out which components are installed on your *ANYmal*.


.. toctree::
   :maxdepth: 3

   components


Interfaces
----------

HRI's Safe Remote Control System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `User manual <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/HRI_Remote_Controller_CDS-012-03_SRCS_User_Manual.pdf>`__
- `Online wiki <https://hriwiki.atlassian.net/wiki/display/DOC/SRCS+User+Manual>`__



Computers
---------

Compulab Fit-PC IPC2 (A)
~~~~~~~~~~~~~~~~~~~~~~~~

**Specs:**

* **Processor:** Intel 4th generation (Haswell ULT) i7-4600U (1.4GHz-2.1GHz), dual core 64-bit
* **Memory:** 2x HX316LS9IBK2/16 DDR3L from HyperX
* **Harddisk:** Samsung V-NAND SSD 850 EVO mSATA 500Gb
* **Graphics:** Intel HD4400 Graphics
* **Interfaces:**
    * 2x GbE LAN ports, Intel I218 + I211 GbE controllers, Intel 7260HMW dualband (WLAN 802.11ac + Bluetooth 4.0)
    * 2x USB 2.0
    * 2x USB 3.0
    * 3x RS232 serial communication ports
    * FM-LANE4U2/4 (FM-4LAN) module:
        * 4x GbE LAN 10/100/1000BASE-T compliant with IEEE 802.3/u/ab
        * 2x USB2.0 downstream ports, up to 480Mbps half-duplex
* **Power supply:** 12 – 15VDC input

**Documents:**

* `Hardware specification <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/fit_pc_ipc2_hw_specification.pdf>`__

Compulab Fit-PC IPC2 (B)
~~~~~~~~~~~~~~~~~~~~~~~~

**Specs:**

* **Processor:** Intel 4th generation (Haswell ULT) i7-4600U(1.4GHz-2.1GHz), dual core 64-bit
* **Memory:** 2x HX316LS9IBK2/16 DDR3L from HyperX
* **Harddisk:** Samsung V-NAND SSD 850 EVO mSATA 500Gb
* **Graphics:** Intel HD4400 Graphics
* **Interfaces:**
    * 2x GbE LAN ports, Intel I218 + I211 GbE controllers, Intel 7260HMW dualband (WLAN 802.11ac + Bluetooth 4.0)
    * 2x USB 2.0
    * 2x USB 3.0
    * 3x RS232 serial communication ports
    * FM-USB3 module:
        * 2x USB3.0 downstream ports (USB2.0 supported on separate pins), up to 5Gbps full-duplex
        * 1x mSATA slot allow to connect mSATA SSD storage (on IPC2i5/i7 models only)
* **Power supply:** 12 – 15VDC input

**Documents:**

* `Hardware specification <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/fit_pc_ipc2_hw_specification.pdf>`__

Network Devices
---------------

.. _Netgear_R7000:

Netgear R7000
~~~~~~~~~~~~~

The Netgear R7000 Nighthawk is a WiFi Router.

* `User manual <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/Router_Netgear_R7000_User_Manual.pdf>`__
* `Data sheet <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/Router_Netgear_R7000_Datasheet.pdf>`__

The firmware of Netgear does not support a proper *bridge mode*. Therefore, the installed firmware of the router is *not* the original from Netgear, but **"DD-WRT Kong Mod for NETGEAR R7000"** from `www.myopenrouter.com <https://www.myopenrouter.com>`__.

* `DD-WRT documentation <http://www.dd-wrt.com/wiki>`__

The *DD-WRT* firmware can be installed by updating the firmware with the original Netgear firmware:

* Connect your computer with the onboard network over the Ethernet port on *ANYmal*
* Launch an Internet browser from your computer
* Type http://www.routerlogin.net or http://www.routerlogin.com.
* A login screen displays.
* Enter the router user name and password that you can find on the datasheet that comes along with your *ANYmal*.
* Select *ADVANCED > Administration > Router Update*.
* Load the file with the firmware of *"DD-WRT Kong Mod for NETGEAR R7000"*.

Sensors
-------

Xsens MTi 100
~~~~~~~~~~~~~

*  `User manual <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/IMU_Xsens_MTi_100_User_Manual.pdf>`__

.. _Optoforce_OMD_45_FH_2000N:

Optoforce OMD-45-FH-2000N
~~~~~~~~~~~~~~~~~~~~~~~~~
This sensor measures a three-dimensional force vector.

* `Datasheet <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/Optoforce_OMD-45-FH-2000N-DATASHEET-V2.1.pdf>`__


.. _Hokuyo_UTM30LXEW:

Hokuyo UTM-30LX-EW
~~~~~~~~~~~~~~~~~~

The Hokuyo UTM-30LX-EW is a Scanning Laser Range Finder.

**Specs:**

* *Power Source:* 12VDC ± 10%
* *Interface:* Ethernet 100BASE-TX(Auto-negotiation)
* `Hardware specification <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/LIDAR_Hokuyo_UTM-30LX-EW_spec_en.pdf>`__

Actuators
---------

ANYbotics *ANYdrive* SEA-90-48-A-ER/EF/ET
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* `User manual <http://docs.leggedrobotics.com/anydrive_doc>`__


.. _Robotis_Dynamixel_MX_64:

Robotis Dynamixel MX-64
~~~~~~~~~~~~~~~~~~~~~~~

* `Hardware specification <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/Actuator_Robotis_Dynamixel_MX-64.pdf>`__
