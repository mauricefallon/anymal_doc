.. _installation:

Installation
============

.. important::

  .. image:: ../images/iso_7010/mandatory_manual.png
    :scale: 50%
    :align: left

  **Carefully read the safety instruction in** :numref:`safety_instructions` **before installing or operating ANYmal.**


First Steps
-----------
#. Unpack your robot and check that you have received all accessories mentioned in the accompanied datasheet of your robot.
#. Connect the battery to the onboard system of *ANYmal* according to the guidelines in section :ref:`installing_battery_pack`.
#. Charge your robot according to the instructions in section :ref:`battery_charging`.
#. Read section :ref:`network` to understand the network configuration.
#. Follow section :ref:`operator_pc_setup`.


Using a Safety Harness
----------------------

It is highly recommended, especially during development, to operate *ANYmal* only when attached to a safety harness.
*ANYmal* is equipped with two dedicated eyebolts fixed to the handles as shown in :numref:`fig_hooks`.

To attach *ANYmal* to a crane or a rope preferably use carabiners.

.. _fig_hooks:

.. figure:: images/hooks.png
    :width: 10cm
    :align: center

    Hooks for mounting a safety harness.


.. caution::

  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  - **Hang the robot only at the dedicated eyebolts.**
  - **Make sure that there is only traction and no torsional load on the hooks.**
  - **Do not exceed a maximum load of 40kg on each eyebolt.**


Storing *ANYmal* in the Transportation Box
------------------------------------------

For a safe transportation *ANYmal* comes in a transportation box.
:numref:`fig_transportation` shows the complete robot stored in the transportation box.

.. caution::
    .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

    - Charge the battery to approximately 50% (see :numref:`battery_charging`) prior to storing *ANYmal* in the transportation box.
    - Two persons are required to put the robot into the box.

To store *ANYmal* in the transportation box:

#. Place the HRI Remote controller in the corresponding box.
#. Disconnect all cables and the MultiSense S7 (:numref:`multisense_s7`) from the robot.
#. The cables, charger and other accessories are stored in the grey box.
#. Place the robot on the belly plate and orient the legs on the side of the main body with the knees pointing upwards as shown in :numref:`fig_transportation`.
#. Lift the robot and place it into the box. Slide foam layers in front and on the back between robot and the box (:numref:`fig_transportation`).
#. Place foam between the main body and the thighs as well as the thighs and the box.
#. Put the HRI and accessories box to the left and right of the main body as shown in :numref:`fig_transportation`.
#. Place some foam around the antennas to protect them and if possible fill empty slots with additional foam pads.
#. Close the box and make sure that there is a sticker on it indicating the orientation.



.. _fig_transportation:

.. figure:: images/transportation.png
  :width: 18cm
  :align: center

  ANYmal stored in the transporation box.
