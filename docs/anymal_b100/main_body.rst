
.. _robot_main_body:

Main Body
=========

The main body of *ANYmal* is a composite structure with a cover on top and on each side.
To access the electronics inside which might be necessary to install a sensor the side plates can be removed.

.. caution::
  .. image:: ../images/iso_7010/warning_electricity.png
    :scale: 50 %
    :align: left

  - **Do not open** *ANYmal* **while it is powered.**
  - **Apply ESD protection measures before exposing electronics.**

:numref:`fig_mainbody` shows the outline of the removable side plate.

.. _fig_mainbody:

.. figure:: images/mainbody.png
   :width: 14cm
   :alt: main body
   :align: center

   ANYmal main body.

To open either side of the main body loosen the fifteen screws and remove the plate.

To close either side of the main body put the side plate back into place and tighten the fifteen M5x12 screws with 3-4Nm. Take care that the x-seal is in place and untwisted.

.. caution::

  **Do not scratch the contact surfaces of the side plate seal.**

.. _bellyplate:

Belly Plate and Protective Elements
-----------------------------------

*ANYmal* is equipped with a reinforced composite protection plate on its bottom as shown in :numref:`fig_bellyplate`.
The purpose of this plate is to protect the main body from falls or ground contact in difficult environment, e.g. on stairs. The protection plate itself takes punctual loads from impact and forwads them to the protective foam. The protecive foam between plate and main body then reduces the accelerations acting on the body during impact.

The belly plate is also robust enough to touch the ground during locomotion on a regular base, for example during crawling gaits. Skratches, even deep ones, do not harm the structural integrity of the place. Abrasion on larger surfaces (above 10cm^2) is acceptable if only the outer layer of carbon is affected and the inner layers remain intact. 


Furthermore *ANYmal* is equipped with protective pads on hips, knees and shanks to protect the actuators and the legs.
These pads consist of a glass fiber reinforced outer shell and foam filling and are glued or screwed into place.
See :numref:`legs` for detailed information about the legs and the mounting of the protective pads.

.. caution::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  **To prevend damages, do not operate** *ANYmal* **without the belly plate or protective pads. Falling without these protections may cause massive damage to the main body and main body electronics due to high accelerations.**


.. _fig_bellyplate:

.. figure:: images/bellyplate.png
    :width: 14cm
    :alt: belly plate
    :align: center

    ANYmal belly plate to protect the main body.

During the operation foreign objects and debris might accumulate in the belly plate.
To clean it loosen the eight screws indicated in :numref:`fig_bellyplate`, remove the belly plate and clean out the dirt.

Afterwards mount the plate again using the same screws (**eight M5x12mm screws**) with a tightening torque of **3.5-4Nm**.
