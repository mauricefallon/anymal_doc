.. _operator_pc:

Operator PC
===========

The `Operator PC` can be any personal computer, which can connect to the robot's onboard WiFi. The system and software requirements are described in section ":ref:`software_requirements`". Read the instructions in the next section to set up your machine.

.. _operator_pc_setup:

Setting up an Operator PC
-------------------------

It is highly recommended to setup the system and software on the `Operator PC` as described in section ":ref:`onboard_computer_setup`". The following sections describe further steps to setup your `Operator PC`.

Installing Software
~~~~~~~~~~~~~~~~~~~

Install ROS and the software for *ANYmal* according to section ":ref:`software_installation`" if you haven't followed the instructions on section ":ref:`onboard_computer_setup`".

.. note::

    The `Operator PC` does not necessarily need to run a low-latency patched kernel, since this can be a drawback for graphical user interfaces.

Install the `any_ping_indicator`_ applet to show the ping status of the onboard computers. This helps to quickly check the state of the connection to the onboard computers.

.. _any_ping_indicator: https://github.com/leggedrobotics/any_ping_indicator

Setting up the Network
~~~~~~~~~~~~~~~~~~~~~~

To use hostnames instead of IP addresses, add the following lines to your ``/etc/hosts`` with ``NAME`` being the name of your robot (e.g. *beth*):

.. code:: bash

    192.168.0.151   anymal-NAME-lpc
    192.168.0.152   anymal-NAME-npc
    192.168.0.153   anymal-NAME-apc

.. note::

   Check the datasheet of your *ANYmal* for the correct IP addresses!

Adding the hostnames allows you to login to the onboard PC through SSH as follows:

.. code:: bash

    ssh integration@anymal-beth-lpc

.. _setting_up_ros:

Setting up ROS
~~~~~~~~~~~~~~

The **roscore** runs on the locomotion PC (lpc). In order to run a ROS node on the operator PC that communicates with the `ROS master` on the lpc, the environment variables ``ROS_MASTER_URI`` and ``ROS_IP`` of your shell need to be set appropriately.

Execute the following commands with

-  ``NAME`` being the name of your robot (e.g. *beth*)
-  ``YOURIPADDRESS`` being your current IP address (e.g. 192.168.0.100)

.. code:: bash

    export ROS_MASTER_URI=http://anymal-NAME-lpc:11311
    export ROS_IP=YOURIPADDRESS

For convenience, you can define the following aliases in your ``~/.bashrc``:

.. code:: bash

    alias ros_connect_anymal='export ROS_MASTER_URI=http://anymal-NAME-lpc:11311'
    alias ros_disconnect='export ROS_MASTER_URI=http://localhost:11311'

and connect with the command ``ros_connect_anymal``.

Note that you can also add the export of the ``ROS_URI`` to your ``~/.bashrc`` if you have always the same (static) IP address. If you do not define the ``ROS_URI``, you need to add the hostname of your machine to */etc/hosts* of ALL machines that are connected to the ROS master.


Setting up Time Synchronization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The clock of `Operator PC` needs to be synchronized with the `Locomotion PC`. Follow section ":ref:`settingup_time_synchronization`" to configure chrony for automated time synchronization.
