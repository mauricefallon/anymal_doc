.. _multisense_s7:

Multisense S7
=============

.. _figmultisenses7:

.. figure:: images/MultiSense-S7.png
    :width: 12 cm
    :alt: mulitsense_s7
    :align: center

    MultiSense S7

The Carnegie Robotics' MultiSense S7 shown in :numref:`figmultisenses7` is a high-resolution, high-data-rate, and high-accuracy 3D range sensor.
*ANYmal Bedi* is equipped with the holder shown in :numref:`fig_multisense_mount` to mount the device.


Installation
------------

.. _fig_multisense_mount:

.. figure:: images/s7_mount.png
   :width: 12cm
   :alt: multisense_mount
   :align: center

   Multisense S7 holder.

#. Mount the blue plate to the main body with four M5x12mm screws with a torque of 3Nm. Note that you should not remove this plate.
#. Mount the Multisense S7 holder with two M5x12mm screws with a torque of 3Nm and two M3x10mm with a torque of 1.3Nm as shown in :numref:`fig_bedi_S7_black_holder`.

      .. _fig_bedi_S7_black_holder:

      .. figure:: images/S7_black_holder.png
        :width: 12cm
        :align: center
        :alt: multisense screws

        The srews with the red circles hold the Multisense S7 holder.

#. Mount the Multisense S7 on the holder according to the datasheet of the sensor.
#. Adjust the desired pitch angle using the two screws as shown in :numref:`fig_bedi_S7_angle_adjust`.

    .. _fig_bedi_S7_angle_adjust:

    .. figure:: images/S7_camera_mount.png
      :width: 12cm
      :align: center
      :alt: pitch angle adjustment

      Adjustment of the pitch angle of the sensor.

#. Connect the cable shown in :numref:`fig_multisenses7cable` to the connectors shown in :numref:`fig_bedi_sensor_connectors2`.

    .. _fig_multisenses7cable:

    .. figure:: images/multisens_s7_cable.png
      :width: 12cm
      :align: center
      :alt: multisense cable

      Multisense S7 cable.

    .. _fig_bedi_sensor_connectors2:

    .. figure:: images/sensor_connectors.png
      :width: 12cm
      :align: center
      :alt: bedi sensor connectors

      Connectors on the main body.

.. warning::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  **If some screws are omitted the housing is not waterproof!**

  |

.. warning::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  **Do not lift the robot at the Multisense holder!**

  |


Software
--------

The Multisense S7 can be added to the visualization and simulation for weight and collision models via its own urdf. Look at the ``anymal.urdf.xacro`` file in the **anymal\_NAME\_description\urdf** folder to see how it is included.
A ``mounted_angle_degree`` parameter can be used to adapt the simulated angle to the one indicated on the side of the Multisense holder.
To correct for mounting imperfections, calibration transformations (``<frame>_axis_aligned`` to ``<frame>``) have been introduced.
If the mounting is perfect (or in simulation), the calibration transformations are identity.


Additional Documents
--------------------

- `Cable Pinout <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/StereoCam_Multisense_S7_Interface_Cable_Pin_Definition.pdf>`__
