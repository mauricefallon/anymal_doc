.. _onboard_computers:

Onboard Computers
=================

.. figure:: images/system_overview.png
     :scale: 50 %

     Overview of the onboard computers


The three **onboard computers**:

-  **Locomotion PC** (``anymal-NAME-lpc``)
-  **Navigation PC** (``anymal-NAME-npc``)
-  **Application PC** (``anymal-NAME-apc``)

are running with a low-latency Linux kernel and a standard Ubuntu LTS version. Read section :ref:`onboard_computer_setup` for more details. The subsequent section explains how the users and groups are configured.

Users and Groups
----------------

The onboard computers are set up with a user called **integration**. The integration user runs the system services like *anymal-roscore* and
executes the robot controller and the localization, respectively. The integration user is used to *integrate* the software from several users.
Thus, all Git repositories of the integration user should always be on the master branch. To test new software, an another user account should
be used instead. See next section on how to create a new user.

The integration user's **primary group** is called **developers**. All other users should also have *developers* as their primary group. The
users of the developers group will automatically source the catkin workspace of the integration user instead of the default one of ROS at login of the user (the overlay happens through the *.profile* mechanism). Note that if you use *gnome-terminal* to login to an onboard computer via SSH, please activate *"Run command as login shell"* in your shell. Otherwise, the file ``~/.profile`` will not be sourced and your catkin workspace is not setup correctly!

Add a new User as Developer
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create user ``USERNAME`` with:

.. code:: bash

    sudo adduser --ingroup developers USERNAME

Developer users need to be added to certain secondary groups in order to run applications which require special access rights to system
resources, e.g. ``/dev/ttyUSB*`` files. Add the user to the following groups:

.. code:: bash

    sudo usermod -a -G dialout USERNAME
    sudo usermod -a -G plugdev USERNAME

Remove a User
~~~~~~~~~~~~~

Remove the user ``USERNAME`` with:

.. code:: bash

    deluser --remove-home USERNAME


System Services
---------------
The system services are installed using the script ``anymal_NAME_PCTYPE/install/install.sh``.

The most important services are listed in :numref:`table_system_services`.


.. _table_system_services:

.. table:: System Services

   +---------------------+-------------------------------+-------------------+----------+
   | Service             | Description                   | Computer          | Startup  |
   +=====================+===============================+===================+==========+
   | anymal-roscore      | ROS master                    | LPC               | yes      |
   +---------------------+-------------------------------+-------------------+----------+
   | anymal-ros-rpsm     | RPSM ROS node                 | LPC, NPC, APC     | yes      |
   +---------------------+-------------------------------+-------------------+----------+
   | anymal-ros-logger   | ROS bag                       | LPC, NPC, APC     | no       |
   +---------------------+-------------------------------+-------------------+----------+
   | anymal-ros-hri      | HRI remote controller driver  | LPC               | yes      |
   +---------------------+-------------------------------+-------------------+----------+
   | anymal-ros-optoforce| Optoforce force sensor driver | LPC               | no       |
   +---------------------+-------------------------------+-------------------+----------+


Network interfaces
------------------

The Ethernet network interfaces are configured via the udev rule in ``/etc/udev/70-persistent-net.rules``.

.. note::

   To use predictable network interface names, you need to configure Grub with ``GRUB_CMDLINE_LINUX="net.ifnames=0"``. See section :ref:`grub_setup`.


.. _onboard_computer_setup:

Setting up an Onboard Computer
------------------------------

This section explains how to set up an onboard computer from scratch.

BIOS Settings
~~~~~~~~~~~~~

To use USB 3.0 devices one has to enable the xHCI Mode in the BIOS.

Enter BIOS Setup utility by pressing F2 at boot.
Navigate to *Advanced > South Bridge Configuration > SB USB Config*
and enable the xHCI Mode.

Installing Ubuntu 16.04 LTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Connect screen, keyboard and mouse.
-  Start the computer with an **Ubuntu LiveCD Desktop version**, which supports EFI.
-  Select ``Try Ubuntu without installing`` at bootup.
-  Open **GParted** to check the partitions of the harddisk.

   -  Delete all partitions of sda if necesscary.
   -  Create a new partition table (menu: Device->Create Partition
      Table)
   -  Select type ``msdos``

-  Start the installer of Ubuntu
-  Select ``Install third-party software for graphics and Wi-Fi hardware, Flash, MP3 and other media``
-  Choose ``Something else`` for manual setup of the partitions
-  Create the following partitions (for a 500Gb harddisk):

   -  EFI partition:

      -  *Size:* 100 MB
      -  *Type:* Primary
      -  *Location:* Beginning
      -  *Use as:* EFI System partition

   -  Root partition:

      -  *Size:* 100000 MB
      -  *Type:* Primary
      -  *Location:* Beginning
      -  *Use as:* Ext4 journaling file system
      -  *Mount point:* /

   -  Home partition:

      -  *Size:* 384000 MB
      -  *Type:* Primary
      -  \*Location: Beginning
      -  *Use as:* Ext4 journaling file system
      -  *Mount point:* /home

   -  Swap partition:

      -  *Size:* 16009 MB
      -  *Type:* Primary
      -  *Location:* Beginning
      -  *Use as:* swap area

-  Select time zone (default: Zurich)
-  Select keyboard (default: English (US)/English (US))
-  Setup user:

   -  Your name: Integration
   -  Your computer's name: anymal-NAME-PCTYPE (e.g. anymal-beth-lpc)
   -  Pick a username: ``integration``
   -  Select ``Require my password to log in``

-  Continue to install Ubuntu

Setting up User and Group Rights
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Restart the computer.
-  Login with the user ``integration``.
-  Create the `developers` group, which should be the primary group for all developer users:

   .. code:: bash

       sudo addgroup developers

-  Change the primary group of the *integration* user:

   .. code:: bash

       sudo usermod -g developers integration

-  Remove the *integration* user:

   .. code:: bash

       sudo delgroup integration

-  Log out and log in again.
-  Give all developers sudo rights by running:

   .. code:: bash

       sudo visudo

-  Add the third line (*%developers ...*):

   .. code:: bash

       # Allow members of group sudo to execute any command
       %sudo   ALL=(ALL:ALL) ALL
       %developers  ALL=(ALL:ALL) ALL

-  Add the *integration* user to the following groups:

   .. code:: bash

       sudo usermod -a -G plugdev integration
       sudo usermod -a -G dialout integration
       sudo usermod -a -G video integration
       sudo usermod -a -G audio integration

-  Allow the *integration* user and the users of the *developers* group to modify the realtime priorities:

   .. code:: bash

       sudo gedit /etc/security/limits.conf

-  Add the following lines:

   .. code:: bash

       integration      soft    rtprio           99
       @developers      soft    rtprio           99
       integration      hard    rtprio           99
       @developers      hard    rtprio           99
       integration      -       nice             -20
       @developers      -       nice             -20

Upgrading the System
~~~~~~~~~~~~~~~~~~~~

-  Make sure that the computer is connected to the internet
-  Execute the following commands to upgrade:

   .. code:: bash

       sudo apt update
       sudo apt upgrade


.. _grub_setup:

Installing the Low-Latency Kernel and Setting up Grub
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Install the latest low-latency kernel or choose a specific version:

   .. code:: bash

       sudo apt install linux-headers-lowlatency linux-image-lowlatency

-  Open the Grub config file:

   .. code:: bash

       sudo gedit /etc/default/grub

-  Replace ``GRUB_DEFAULT=0`` with the following line and the installed
   kernel version, to start Ubuntu by default with the lowlatency
   kernel:

   .. code:: bash

       GRUB_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux 4.4.0-77-lowlatency"

-  Replace ``GRUB_CMDLINE_LINUX=""`` with

   .. code:: bash

       GRUB_CMDLINE_LINUX="net.ifnames=0"

-  Update grub

   .. code:: bash

       sudo update-grub

.. note::

    By selecting a specific kernel in GRUB, a user cannot accidentally install a new kernel and change it. You should be careful when changing the kernel because it can influence the hardware drivers.


Enabling "autorepair" to Automatically Correct Disk Errors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The system might getting stuck during bootup due to disk errors. The message **Boot: Errors were found while checking the disk for ... (press   F...)** will appear and a user input is requested. To enable **autorepair**:

-  Open the following file:

   .. code:: bash

       sudo gedit /etc/default/rcS

-  Replace ``FSCKFIX=no`` with ``FSCKFIX=yes``

.. _settingup_time_synchronization:

Setting up Time Synchronization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Chrony synchronizes the time between the different computers.

-  Install chrony:

   .. code:: bash

       sudo apt install chrony

-  Open the config file of chrony:

   .. code:: bash

       sudo gedit /etc/chrony/chrony.conf

-  Replace the config file for the **locomotion PC** (anymal-NAME-lpc)
   with:

   .. code:: bash

       keyfile /etc/chrony/chrony.keys
       commandkey 1
       driftfile /var/lib/chrony/chrony.drift
       log measurements statistics tracking
       logdir /var/log/chrony
       maxupdateskew 100.0
       dumponexit
       dumpdir /var/lib/chrony
       local stratum 8
       allow 192.168.0
       logchange 0.5

-  Replace the config file for the **other PCs** (npc, apc, opc) with
   the following, where NAME is the name of the robot (e.g. beth):

   .. code:: bash

       server anymal-NAME-lpc minpoll 1 maxpoll 2 polltarget 30 maxdelaydevratio 2
       keyfile /etc/chrony/chrony.keys
       commandkey 1
       driftfile /var/lib/chrony/chrony.drift
       log measurements statistics tracking
       logdir /var/log/chrony
       maxupdateskew 100.0
       dumponexit
       dumpdir /var/lib/chrony
       local stratum 10
       allow anymal-NAME-lpc
       logchange 0.5
       initstepslew 5 anymal-NAME-lpc
       makestep 100 10

-  Open the key file:

   .. code:: bash

       sudo gedit /etc/chrony/chrony.keys

-  Add the following line:

   .. code:: bash

       1 anymal

Installing additional software and removing packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Install the following packages:

   .. code:: bash

       sudo apt install openssh-server
       sudo apt install git mercurial
       sudo apt install vim-gnome meld
       sudo apt install libgoogle-glog-dev
       sudo apt install htop iotop powertop iftop strace ltrace screen i2c-tools nmap arp-scan iperf3 aptitude lm-sensors

-  Remove the following packages:

   .. code:: bash

       sudo apt purge modemmanager

   .. note:: The `modemmanager` may unnecessarily disturb hardware drivers and is usually not used.


Setting up the time-based job scheduler
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Open the crontab file:

   .. code:: bash

       crontab -e

.. note:: You need to be logged in with user ``integration``.


-  To automatically check the disk space and remove old log files, add the line at the end:

   .. code:: bash

       */10 * * * * ~/git/anymal_logging/bin/check_disk_space.sh ~/.ros

.. _configuring_network_interfaces:

Configuring network interfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Open the network configuration file:

   .. code:: bash

       sudo gedit /etc/network/interfaces

-  A) Configuration for the **locomotion PC** (anymal-NAME-lpc):

   .. code:: bash

      # interfaces(5) file used by ifup(8) and ifdown(8)
      auto lo
      iface lo inet loopback

      # Onboard network
      auto eth0
      iface eth0 inet static
      network 192.168.0.0
      netmask 255.255.255.0
      address 192.168.0.151
      broadcast 192.168.0.255
      gateway 192.168.0.1
      dns-nameservers 192.168.0.1 8.8.8.8

-  B) Configuration for the **navigation PC** (anymal-NAME-npc):

   .. code:: bash

      # interfaces(5) file used by ifup(8) and ifdown(8)
      auto lo
      iface lo inet loopback

      # Onboard network
      auto eth0
      iface eth0 inet static
      network 192.168.0.0
      netmask 255.255.255.0
      address 192.168.0.152
      broadcast 192.168.0.255
      gateway 192.168.0.1
      dns-nameservers 192.168.0.1 8.8.8.8

      # Front LIDAR
      auto eth2
      iface eth2 inet static
      address 192.168.1.222
      netmask 255.255.255.0
      network 192.168.1.0

-  C) Configuration for the **application PC** (anymal-NAME-apc):

   .. code:: bash

      # interfaces(5) file used by ifup(8) and ifdown(8)
      auto lo
      iface lo inet loopback

      # Onboard network
      auto eth0
      iface eth0 inet static
      network 192.168.0.0
      netmask 255.255.255.0
      address 192.168.0.153
      broadcast 192.168.0.255
      gateway 192.168.0.1
      dns-nameservers 192.168.0.1 8.8.8.8

.. _configuring_hostnames:

Configuring hostnames
~~~~~~~~~~~~~~~~~~~~~~~~~

-  Open the file:

   .. code:: bash

       sudo gedit /etc/hosts

-  A) For the **locomotion PC**, add the following lines with the appropriate names of the machines:

   .. code:: bash

      127.0.0.1 localhost
      127.0.1.1 anymal-NAME-lpc

      # The following lines are desirable for IPv6 capable hosts
      ::1 ip6-localhost ip6-loopback
      fe00::0 ip6-localnet
      ff00::0 ip6-mcastprefix
      ff02::1 ip6-allnodes
      ff02::2 ip6-allrouters

      192.168.0.200 anymal-NAME-opc
      192.168.0.152 anymal-NAME-npc
      192.168.0.153 anymal-NAME-apc

-  B) For the **navigation PC**, add the following lines with the appropriate names of the machines:

   .. code:: bash

      127.0.0.1 localhost
      127.0.1.1 anymal-NAME-npc

      # The following lines are desirable for IPv6 capable hosts
      ::1     ip6-localhost ip6-loopback
      fe00::0 ip6-localnet
      ff00::0 ip6-mcastprefix
      ff02::1 ip6-allnodes
      ff02::2 ip6-allrouters

      192.168.0.200	anymal-NAME-opc
      192.168.0.151	anymal-NAME-lpc
      192.168.0.153	anymal-NAME-apc

-  C) For the **application PC**, add the following lines with the appropriate names of the machines:

   .. code:: bash

      127.0.0.1 localhost
      127.0.1.1 anymal-NAME-apc

      # The following lines are desirable for IPv6 capable hosts
      ::1     ip6-localhost ip6-loopback
      fe00::0 ip6-localnet
      ff00::0 ip6-mcastprefix
      ff02::1 ip6-allnodes
      ff02::2 ip6-allrouters

      192.168.0.200	anymal-NAME-opc
      192.168.0.151	anymal-NAME-lpc
      192.168.0.152	anymal-NAME-npc

Installing ROS
~~~~~~~~~~~~~~~~~~

Follow the guidelines in section :ref:`Installing_ROS` to install ROS.

Installing Catkin Tools
~~~~~~~~~~~~~~~~~~~~~~~

Install the `Catkin Command Line Tools` (`catkin_tools`_) to build and run the software. The tools can be installed with ``apt`` (`Installing catkin\_tools <https://catkin-tools.readthedocs.io/en/latest/installing.html>`__).

.. note:: Use `catkin\_tools`_, i.e. use the command ``catkin build`` instead of ``catkin_make``, to build catkin packages.

.. _catkin\_tools: https://catkin-tools.readthedocs.io

Installing the software with ``apt``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Install the packages for your *ANYmal* (e.g. ANYmal beth) with the following command and replace the following placeholders:

    -  ``USER``: user name
    -  ``PASSWORD``: password
    -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)

    .. code:: bash

        sudo sh -c 'echo "deb [arch=amd64] http://USER:PASSWORD@packages.leggedrobotics.com/anymal-NAME/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/any.list'
        sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 1DFF9FB9

-  Install the package for the specific onboard computer with:

    -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)
    -  ``PCTYPE``: type of the PC (e.g. ``lpc``, ``npc``, ``apc``)

    .. code:: bash

        sudo apt update
        sudo apt install ros-kinetic-anymal-NAME-PCTYPE

-  Re-source the ROS workspace:

    .. code:: bash

        source /opt/ros/kinetic/setup.bash

Setting up Git and SSH
~~~~~~~~~~~~~~~~~~~~~~

To clone the Git repositories from `bitbucket.org <www.bitbucket.org>`__ or `github.com <www.github.com>`__, we recommend to use **SSH**. Since all commits with Git should be asscociated with an author, a separate user account with the Git configurations and SSH keys needs to be
created.

-  Make a folder to store all related Git repositories:

   .. code:: bash

       mkdir -p ~/git
       chmod -R g+swX ~/git

-  Create a new user with username ``USERNAME``:

   .. code:: bash

       sudo adduser --ingroup developers USERNAME

-  Initialize the user's password:

   .. code:: bash

       sudo passwd USERNAME

-  Login with the new user

   .. code:: bash

       sudo su USERNAME

-  Create the git configurations

   .. code:: bash

       git config --global user.name ACCOUNTNAME
       git config --global user.email ACCOUNTEMAIL

-  Create the SSH public key:

   .. code:: bash

         ssh-keygen

   -  Use default directory to store the key.
   -  Enter passphrase.

-  Copy public key from

   .. code:: bash

         cat ~/.ssh/id_rsa.pub

-  The following pages explains you how to setup SSH on your machine:

   -  `How to set up SSH for Bitbucket <https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html>`__
   -  `How to set up SSH for Github <https://help.github.com/articles/generating-an-ssh-key/>`__

Downloading/Cloning the Source Code
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Clone the Git repository for your *ANYmal* in the newly created folder:

   -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)
   -  ``PCTYPE``: type of PC (e.g. ``lpc``)

   .. code:: bash

       cd ~/git
       git clone git@bitbucket.org:leggedrobotics/anymal_NAME.git

   -  Note that you need to be logged in with a user, which has the SSH
      keys.

-  Execute the script in the package for the given computer to
   automatically clone all required repositories:

   -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)
   -  ``PCTYPE``: type of PC (e.g. ``lpc``)

   .. code:: bash

       ./anymal_NAME/anymal_NAME_PCTYPE/bin/clone_deps.sh

-  Logout to use the integration user for the following steps:

   .. code:: bash

       exit

Creating the Catkin Workspace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following is a sequence of commands to set up a catkin workspace
with `catkin_tools`_:

-  Make a new workspace and source space:

   .. code:: bash

       mkdir -p ~/catkin_ws/src

-  Navigate to the workspace root:

   .. code:: bash

       cd ~/catkin_ws

-  Create symlinks to all Git repositories that you want to build from
   source. For instance, create a symlink to your *ANYmal* specific Git
   repository with:

    -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)

   .. code:: bash

       ln -s ~/git/anymal_NAME src/

-  Initialize workspace with a hidden marker file:

   .. code:: bash

       catkin init

-  Configure cmake build type:

   .. code:: bash

       catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release

-  Build the computer specific package:
    -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)
    -  ``PCTYPE``: type of PC (e.g. ``lpc``)

   .. code:: bash

       catkin build anymal_NAME_PCTYPE

-  Load the workspace's environment:

   .. code:: bash

       source ~/catkin_ws/devel/setup.bash

   .. note:: Note that you need to build at least one package, otherwise the `devel` folder won't exist.

Installing system services, udev rules, and profile scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Run the install script to copy the configuration files to */etc*:

   -  ``NAME``: name of your *ANYmal* (e.g. ``beth``)
   -  ``PCTYPE``: type of PC (e.g. ``lpc``)

   .. code:: bash

       cd ~/git
       ./anymal_NAME/anymal_NAME_PCTYPE/install/install.sh

-  Re-login.
