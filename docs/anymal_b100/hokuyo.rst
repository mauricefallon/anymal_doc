.. _hokuyo:

Actuated LIDAR Unit
===================

The actuated LIDAR unit is a sensor module which perceives the environment with an actuated *Scanning Laser Range Finder*.
:numref:`fig_lidar` shows the device that is installed on the back side of *ANYmal*.

.. _fig_lidar:

.. figure:: images/lidar.png
   :width: 10cm
   :alt: lidar
   :align: center

   Actuated LIDAR unit mounted on ANYmal.

It creates a 3D point cloud of the environment by assembling 2D scans from a :ref:`Hokuyo_UTM30LXEW` sensor, which is turned by a :ref:`Robotis_Dynamixel_MX_64` servo motor.
The individual scans are dewarped using the local state estimation from IMU and leg kinematics.


Installation
------------

#. Mount the actuated LIDAR unit with six M5x10mm screws with a torque of 2.5 - 3.5Nm (:numref:`fig_lidar_mount`).

    .. _fig_lidar_mount:

    .. figure:: images/lidar_mount.png
        :width: 10cm
        :alt: lidar mount
        :align: center

        Actuated LIDAR unit screws on ANYmal.

#. Plug the power connector (:numref:`fig_lidar_right`) into the 12V socket of the RPSM main board.

    .. _fig_lidar_right:

    .. figure:: images/lidar_right.png
        :width: 10cm
        :alt: lidar right
        :align: center

        Actuated LIDAR RPSM interface (12V).

#. Plug the USB connector (:numref:`fig_lidar_left`) into an USB2.0 port of the *navigation PC* to control the servo motor.

    .. _fig_lidar_left:

    .. figure:: images/lidar_left.png
        :width: 10cm
        :alt: lidar left
        :align: center

        Actuated LIDAR navigation PC interface (Ethernet and USB2.0).

#. Plug the Ethernet connector (:numref:`fig_lidar_left`) into the *navigation PC* to receive the laser scans.


Software
--------

The `actuated_lidar`_ package provides the software for one or more actuated LIDAR units which are synchronized to assemble a single point cloud.

The file `actuated_lidar/launch/actuated_lidar.launch` serves as an example launch file.

The actuated lidar can be added to the visualization and simulation for weight and collision models via its own urdf located in the **actuated_lidar_description** package.
Look at the ``anymal.urdf.xacro`` file in the **anymal\_NAME\_description/urdf** folder to see how it is included.


Calibration
-----------

The mounting of the actuator and the LIDAR are calibrated separately.
:numref:`fig_lidar_drawing` displays the transformation frames of the LIDAR unit.
To correct for mounting imperfections, calibration transformations (``<frame>_axis_aligned`` to ``<frame>``) have been introduced.
If the mounting is perfect (or in simulation), the calibration transformations are identity.

.. _fig_lidar_drawing:

.. figure:: images/lidar_drawing.png
   :width: 17cm
   :alt: lidar_drawing
   :align: center

   Transformation frames of the actuated LIDAR unit.


Actuator
~~~~~~~~

#. Reset the old calibration: In `anymal_NAME_nav/config/actuated_lidar/actuated_lidar.yaml`, set the ``offset_angle`` to 0.0.
#. Start the actuator as standalone node:

   .. code:: bash

      roslaunch anymal_NAME_npc dynamixel.launch

#. Manually turn the actuated LIDAR into its zero position (see :numref:`fig_lidar`).
#. Read out the actuator joint position

   .. code:: bash

      rostopic echo /actuated_lidar/dynamixel/joint_state

#. Set ``offset_angle`` to the current position.

LIDAR
~~~~~

#. Reset the old calibration: In `anymal_NAME_description/urdf/anymal.urdf.xacro`, set both LIDAR calibration transformations to identity.
#. On the `Locomotion PC`, load the description and launch the TF publishers afterwards:

   .. code:: bash

      roslaunch anymal_NAME_lpc load_description.launch
      roslaunch anymal_NAME_lpc tf_publishers.launch

#. On the `Navigation PC`, launch both the auxiliary TF publisher and the actuated LIDAR module:

   .. code:: bash

      roslaunch actuated_lidar robot_to_map_tf_publisher.launch
      roslaunch anymal_NAME_npc actuated_lidar.launch

#. On the `Operator PC`, run rviz with the prepared configuration and check the scan and the assembled point cloud.

   .. code:: bash

      rosrun rviz rviz -d ~/git/actuated_lidar/actuated_lidar/config/calibration/rviz_configuration.rviz

#. Manually tune the calibration transformation parameters. Note that every time the parameters are changed, step 2. has to be repeated in order to reload them.


Troubleshooting
---------------

TF to assemble point cloud cannot be found on the real robot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You may have a timing issue.
Please refer to section :ref:`timing_issues`.


Actuated LIDAR registers no points in simulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You may have an issue with your graphics card. You can circumvent the problem either by setting

.. code:: bash

  simulate_using_gpu="false"

in your ANYmal description file or by adding the following line to your ``~/.bashrc``:

.. code:: bash

  export LIBGL_ALWAYS_SOFTWARE=1


.. _actuated_lidar: https://bitbucket.org/leggedrobotics/actuated_lidar


Torque limiter of LIDAR actuator triggers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The actuator of the actuated LIDAR unit is equipped with a torque limiter. This limiter disengages the connection between actuator and LIDAR head as soon as the torque becomes too high, for example when the LIDAR head hits the ground. Signs that indicate that the torque is disengaged are:

* The point clud has an angular offset around the rotation axis.
* When turning, the LIDAR head can easily be hold in place by hand while the actuator is still turning.

If this occurs, use the following procedure to engage the torque limiter:

#. Let the actuated LIDAR head turn and hold it in place by hand, while the actuator is still turning. Be sure to use as little force as possible, two fingers should usually be enough to hold the LIDAR head.
#. The actuator then turns until the torque limiter engages when the correct angle between LIDAR head and actuator is reached.
#. When the torque limiter engages, the torque on the LIDAR head increases instantly. When holding the head with as little force as possible, this should make the head slip out of your fingers automatically.
#. If your holding by hand was too strong, the torque limiter disengages again and the procedure repeats after one turn.
