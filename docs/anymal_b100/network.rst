.. _network:

Network
=======

IP Addresses
------------

The IP addresses of the devices on your *ANYmal* can be found in the **datasheet** that comes along with the robot.. The default IP addresses are:

   ===================  ====================  =============
    Device                 Name                IP address
   ===================  ====================  =============
   Locomotion PC        anymal-NAME-lpc       192.168.0.151
   Navigation PC        anymal-NAME-npc       192.168.0.152
   Application PC       anymal-NAME-apc       192.168.0.153
   Onboard router                             192.168.0.154
   Operator PC          anymal-NAME-opc       192.168.0.200
   Front Hokuyo laser                         192.168.2.10
   Rear Hokuyo laser                          192.168.1.10
   ===================  ====================  =============

.. _configurations_of_the_router:

Configurations of the Router Netgear R7000
------------------------------------------

The Netgear R7000 Nighthawk is a WiFi Router which can be operated in:

* **router mode** (creates its own WiFi network) or
* **bridge mode** (connects to another network).

To connect the onboard computers to the internet, the simplest way is to configure the router in *bridge mode* and connect it to a network which has a data link to the web.

The firmware of Netgear does not support a proper *bridge mode*. Therefore, the installed firmware of the router is *not* the original from Netgear, but **"DD-WRT Kong Mod for NETGEAR R7000"** from `www.myopenrouter.com <https://www.myopenrouter.com>`__.

Different configuration files of the router are provided to easily switch the modes of the router.

To change the router configuration settings with the **DD-WRT firmware**:

* Connect your computer with the onboard network over the Ethernet port on *ANYmal* or over WiFi.
* Access the *DD-WRT Web-GUI* using a web browser pointing to the IP address of your router, which you find on the datasheet of your *ANYmal*.

The shipped router configuration settings for *ANYmal* with the **DD-WRT firmware** can be loaded from a file:

* Download the Git repository with the configuration files of your *ANYmal* from https://bitbucket.org/\ (e.g. anymal\_beth).
* Login to the *DD-WRT Web-GUI* as aforementioned.
* Click *Administration > Backup > Backup Browse*.
* Select the configuration file from the Git repository (anymal\_NAME/anymal\_NAME\_router/config/\*.bin).
* Click *Restore*.

To connect the router to another network in bridge mode:

* Login to the *DD-WRT Web-GUI* as aforementioned.
* Click *Wireless > Basic Settins*.
* Change *Wireless Mode* to *Client Bridge*.
* Change *Wireless Network Mode* to the appropriate one (e.g. *AC-Only*).
* Change *Wireless Network Name (SSID)* to the name of the other network (e.g. *anynet*).

To change the router configuration settings with the **Netgear firmware**:

* Connect your computer with the onboard network over the Ethernet port on *ANYmal* or over WiFi.
* Launch an Internet browser from your computer.
* Type http://www.routerlogin.net or http://www.routerlogin.com.
* A login screen displays.
* Enter the router user name and password that you can find on the datasheet that comes along with your *ANYmal*.

.. warning::

  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  Do not setup the router with 2.4 GHz because it will interfere with the HRI's remote controller resulting in unintended emergency stops! Moreover, the 2.4 GHz WiFi setup would require antennae with a different length.

Hints for setting up *ANYmal* to run in a different subnet
----------------------------------------------------------
In order to run *ANYmal* on a different subnet several changes need to be made on the on-board computers and router:

* Configure the *Hosts* files on *LPC*, *NPC* and *APC* to the new subnet (Read section :ref:`configuring_hostnames`).
* Change the *Interfaces* files on *LPC*, *NPC* and *APC* (Read section :ref:`configuring_network_interfaces`).
* Allow *chrony* to operate in the new subnet, i.e., adapt the *chrony.conf* file (Read section :ref:`settingup_time_synchronization`).
* Adapt the *OPC* *.bashrc* file, to set the correct ROS_IP (Read section :ref:`setting_up_ros`).
* Change the on-board router to be an access point in the new network, i.e., change the network SSID and password (Read section :ref:`configurations_of_the_router`).
