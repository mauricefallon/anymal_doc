.. _legs:

Legs of *ANYmal*
================

.. _joint_actuators:

Leg structure
-------------

Leg components
~~~~~~~~~~~~~~

*ANYmal* has four legs, two *left* and two *right*. All four legs contain of mainly the same components, which are listed in :numref:`fig_leg_full`.

.. _fig_leg_full:

.. figure:: images/leg_full.jpg
    :scale: 50 %
    :alt: Leg components
    :align: center

    Components of an *ANYmal* leg. In this picture, a *left* leg is shown.

The difference between left and right leg lies in the way the foot is attached to the shank and can be seen in :numref:`fig_leg_left_right`.

.. _fig_leg_left_right:

.. figure:: images/left_right_leg.jpg
    :scale: 50 %
    :alt: Left and right leg
    :align: center

    Left and right leg of *ANYmal*.


Actuators
~~~~~~~~~

The legs are actuated by the series-elastic actuators *ANYdrive* SEA-90-48-A-ER/EF/ET.
More information about these actuators can be found in the `user manual <https://anybotics-anydrive-doc.readthedocs-hosted.com>`_.

Each leg contains three *ANYdrives*.
They are listed in :numref:`tab_actuators`, starting with the one closest to the main body.

.. _tab_actuators:

.. table:: Actuator Symbols

  ============  =======================
  Abbreviation  Description
  ============  =======================
  HAA  drive    Hip adduction/abduction
  HFE  drive    Hip flexion/extension
  KFE  drive    Knee flexion/extension
  ============  =======================

See as well :ref:`model` for the naming of the joints and the links.

Cabling
~~~~~~~

Each leg of *ANYmal* contains six different cables. They are listed in :numref:`tab_cables`, starting with the one closest to the main body.

.. _tab_cables:

.. table:: Leg Cables

    +-----------------------------------+
    |  Cable                            |
    +===================================+
    | *48V power cable tree*            |
    +-----------------------------------+
    | *foot sensor USB cable*           |
    +-----------------------------------+
    | *main body to HAA EtherCAT cable* |
    +-----------------------------------+
    | *HAA to HFE EtherCAT cable*       |
    +-----------------------------------+
    | *HFE to KFE EtherCAT cable*       |
    +-----------------------------------+
    | *foot sensor analog cable*        |
    +-----------------------------------+

Signal Outage
~~~~~~~~~~~~~

  If an *ANYdrive* looses the EtherCAT signal (e.g. PC shutdown) it switches into the error-state.
  It will remain in that state until the signal is re-established.
  Using *clear* and *enable actuators* will bring the actuators into control-OP state.

  While in the error state the *ANYdrive* controller activates the freeze mode.
  In this mode the actuator will control the velocity to zero and maintain its current position.

  .. important::
      .. image:: ../images/iso_7010/warning_electricity.png
        :scale: 50 %
        :align: left

      **The** *ANYdrive* **is still powered in the error mode.**
      **Do not expose any electronics of the robot.**

      
.. _assembling_leg:

Assembling a Leg
----------------

.. _mounting_haa_drive:

Mount the HAA drive
~~~~~~~~~~~~~~~~~~~


#. Connect the *HAA to HFE EtherCAT cable* to the HAA drive and thread the cable through the hollow shaft.
#. Thread the *48V power cable tree* and the *foot sensor USB cable* through the HAA drive hollow shaft.
#. Attach the HAA drive to the main body, align the back of the drive with the edge of the holder and make sure that the guidance fits (see :numref:`fig_haa_main_body_mounting`). The connector cap has to point outwards with an angle of 10deg (as shown in :numref:`fig_haa_drive_mounting`) with respect to the side of the main body.

    .. _fig_haa_drive_mounting:

    .. figure:: images/haa_drive_mounting.png
        :scale: 50 %
        :alt: HAA drive mounting angle
        :align: center

        HAA drive angle when mounted on the main body.

    .. _fig_haa_main_body_mounting:

    .. figure:: images/haa_main_body_mounting.jpg
        :scale: 50 %
        :alt: HAA main body mounting
        :align: center

        HAA drive attached to the main body. The guidance feature is marked with a red circle.

#. Mount the two HAA drive clamps with **four M5x20mm screws** and tighten the screws with **2.6-3.0Nm**. Use thread locker (for example Loctite 243) to secure the screws. For each clamp, first tighten both screws with minimum torque and make shure that the gap between clamp and main body brace is roughly equal size on both ends of the clamp. Then increse the torque by turning both screws alternately quarter a turn until the maximum torque of **2.6-3.0Nm** is reached for both screws.
#. Connect the *48V power cable* and the *foot sensor USB cable* to the main body and connect the *main body to HAA EtherCAT cable* to main body and HAA drive.

Mount the HFE drive holder
~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Thread the *48V power cable tree*, the *HAA to HFE EtherCAT cable* and the *foot sensor USB cable* through the output shaft flange of the HFE drive holder.
#. Attach the HFE drive holder on the hollow shaft of the HAA drive. The HFE drive holder must point 90deg to the side with respect to the zero position of the output shaft (see :numref:`fig_hfe_drive_holder_mounting`) and the cable opening of the HFE drive holder must point towards the main body.

    .. _fig_hfe_drive_holder_mounting:

    .. figure:: images/hfe_drive_holder_mounting.png
        :scale: 50 %
        :alt: HFE drive holder mounting
        :align: center

        HFE drive holder angle when mounted on the HAA drive.

#. Add ten **M4x12mm screws** and tighten them with **2.1-2.3Nm** to connect the HFE drive holder to the output shaft of the HAA drive. Use thread locker (for example Loctite 243) to secure the screws.

Assembling the HFE drive heat sink
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Eight of the twelve *ANYdrives* on *ANYmal* are equipped with passive cooling elements (sinks) to dissipate heat. When exchanging the HFE Motor the connected heat sink has to be disassembled and re-assembled as well.

.. caution::

  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  **Always apply heat transfer paste on the contact surfaces when mounting the HFE drive heat sink.**


.. _fig_hfe_sink:

.. figure:: images/HFE_sink.png
   :width: 12cm
   :alt: HFE sink
   :align: center

   HFE drive heat sink.

#. Screw the sink onto the back of the HFE drive using **six M2.5x6mm screws** with **0.8-1.0Nm** (small circles in :numref:`fig_hfe_sink`).
#. Slide heat pipes into the bores of the sink (apply heat transfer paste).
#. Place the concave shaped clamp on the thermal interface of the *ANYdrive* and apply some heat transfer paste in the groves for the heat pipes.
#. Make sure the heat pipes are properly aligned in the groves.
#. Using **two M3x10 screws** with **1.2-1.3Nm**, mount the upper part of the to connect the heat pipes (big circle in :numref:`fig_hfe_sink`) to the *ANYdrive*. Use thread locker (for example Loctite 243) to secure the screws.

Mount the HFE drive
~~~~~~~~~~~~~~~~~~~

#. Plug the *HAA to HFE EtherCAT cable* into the HFE drive.
#. Plug the *HFE branch of the 48V power cable* into the HFE drive.
#. Plug the *HFE to KFE EtherCAT cable* into the HFE drive.
#. Attach the HFE drive to the HFE holder:
    #. The back of the HFE drive has to be flush with the back of the HFE holder.
    #. The key in the back of the HFE holder needs to fit in the one positioning feature at the back of the HFE drive which allows the HFE drive connector cap to point upwards and inclined 15� away from the main body, as shown in :numref:`fig_hfe_drive_mounting`.

        .. _fig_hfe_drive_mounting:

        .. figure:: images/hfe_drive_mounting.png
            :scale: 50 %
            :alt: HFE drive mounting
            :align: center

            Mounting angle of the HFE drive.

    #. The *HFE branch of the 48V power cable*, the *HAA to HFE EtherCAT cable* and the *HFE to KFE EtherCAT cable* enter the HFE holder through the upper opening between HFE drive and holder, as shown in :numref:`fig_hfe_drive_cabling`.

        .. _fig_hfe_drive_cabling:

        .. figure:: images/hfe_drive_cabling.jpg
            :scale: 50 %
            :alt: HFE drive cabling
            :align: center

            Cabling on the upper side of the HFE drive.

    #. The *48V power cable tree*, the *foot sensor USB cable* and the *HFE to KFE EtherCAT cable* leave the HFE holder through the lower opening between HFE drive and holder, as shown in :numref:`fig_hfe_drive_cabling_2`.

        .. _fig_hfe_drive_cabling_2:

        .. figure:: images/hfe_drive_cabling_2.jpg
            :scale: 50 %
            :alt: HFE drive cabling bottom
            :align: center

            Cabling on the bottom side of the HFE drive.


#. Mount the two HFE holder clamps with **four M4x12mm screws** and tighten the screws with **2.1-2.3Nm**. Use thread locker (for example Loctite 243) to secure the screws. For each clamp, first tighten both screws with minimum torque and make shure that the gap between clamp and main body brace is roughly equal size on both ends of the clamp. Then increse the torque by turning both screws alternately quarter a turn until the maximum torque of **2.1-2.3Nm** is reached for both screws. The HFE drive pad (see :numref:`fig_shoulder_pads`) is thereby glued to the HFE holder clamp pointing towards the thigh.
#. Thread the *KFE branch of the 48V power cable*, the *HFE to KFE EtherCAT cable* and the *foot sensor USB cable* through the hollow shaft of the HFE drive.


Mount the Thigh
~~~~~~~~~~~~~~~

#. Thread the *KFE branch of the 48V power cable tree*, the *HAA to HFE EtherCAT cable* and the *foot sensor USB cable* through the output shaft flange of the thigh.
#. Attach the thigh on the hollow shaft of the HFE drive. With the zero position on the output shaft of the HFE drive pointing upwards, the thigh has to point downwards.
#. Add **ten Screws M4x12mm** and tighten them with **2.1-2.3Nm** to connect the HAA holder to the drive. Use thread locker (for example Loctite 243) to secure the screws.

Mount the KFE drive
~~~~~~~~~~~~~~~~~~~

#. Apply heat transfer paste on both heatpipe terminals of the thigh (picture).


    .. _fig_heatpipe_terminals:

    .. figure:: images/heatpipe_terminals_thigh.jpg
        :scale: 30 %
        :align: center

        Heatpipe terminals and KFE drive pad.

#. Attach the KFE drive to the thigh:
    #. The back of the KFE drive has to be flush with the back of the inner thigh clamp.
    #. The key in the back of the inner thigh needs to fit in the one positioning feature at the back of the KFE drive which allows the drive connector cap to point upwards inside the thigh.
#. Mount the two thigh clamps with **four M4x12mm** screws and tighten the screws with **2.1-2.3Nm**. Use thread locker (for example Loctite 243) to secure the screws. For each clamp, first tighten both screws with minimum torque and make shure that the gap between clamp and main body brace is roughly equal size on both ends of the clamp. Then increse the torque by turning both screws alternately quarter a turn until the maximum torque of **2.1-2.3Nm** is reached for both screws.
#. Mount the heatpipe terminals with **four M3x10 screws** and tighten them with **1.2-1.3Nm**. Use thread locker (for example Loctite 243) to secure the screws.
#. Plug the *KFE branch of the 48V power cable tree* into the KFE drive.
#. Plug the *HFE to KFE EtherCAT cable* into the KFE drive.
#. Thread the *foot sensor USB cable* through the back opening of the thigh and the hollow shaft of the KFE drive.
#. Close the front thigh cover with **eight M3x6mm screws** and **eight 7mm washers** (see :numref:`fig_thigh_screws`) and tighten the screws with **1.2-1.3Nm**. Use thread locker (for example Loctite 243) to secure the screws.
#. Attach the shoulder pad with **four M3x10mm screws** (see :numref:`fig_thigh_screws`) and tighten them with **1.2-1.3Nm** as shown right in :numref:`fig_shoulder_pads`. Use thread locker (for example Loctite 243) to secure the screws.

    .. caution::
      .. image:: ../images/iso_7010/warning_general.png
        :scale: 50%
        :align: left

      **Never put any load on the legs when the front thigh covers are open. The front thigh cover is an integral part of the thigh structure. Only with the thigh cover attached and all twelve M3 screws properly tightened, the thigh is able to properly bear any loads.**

    .. _fig_thigh_screws:

    .. figure:: images/thigh_screws.jpg
        :scale: 100 %
        :alt: Thigh screws
        :align: center

        Thigh screws.

    .. _fig_shoulder_pads:

    .. figure:: images/shoulder_pads.png
        :width: 10cm
        :alt: shoulder pads
        :align: center

        Shoulder pads.

#. Mount the two thigh clamps with **four M4x12mm** screws and tighten the screws with **2.1-2.3Nm**. Use threadlocker (for example Loctite 243) to secure the screws. For each clamp, first tighten both screws with minimum torque and make shure that the gap between clamp and main body brace is roughly equal size on both ends of the clamp. Then increse the torque by turning both screws alternately quarter a turn until the maximum torque of **2.1-2.3Nm** is reached for both screws. The KFE drive pad (see :numref:`fig_heatpipe_terminals`) is thereby glued to the thigh clamp pointing towards the shank.
#. Mount the heatpipe terminals with **four M3x10 screws** and tighten them with **1.2-1.3Nm**. Use threadlocker (for example Loctite 243) to secure the screws.
#. Plug the *KFE branch of the 48V power cable tree* into the KFE drive.
#. Plug the *HFE to KFE EtherCAT cable* into the KFE drive.
#. Thread the *foot sensor USB cable* through the back opening of the thigh and the hollow shaft of the KFE drive.




#. Attach the knee thigh cover with **two M3x10mm** screws and tighten them with **1.2-1.3Nm**. Use thread locker (for example Loctite 243) to secure the screws.

Mount the shank
~~~~~~~~~~~~~~~

#. Attach the shank on the output shaft of the KFE drive. With the zero position on the output shaft of the KFE drive pointing upwards, the shank has to be positioned horizontally, pointing away from the robot center.
#. Add **five M4x12mm screws** and tighten them with **2.1-2.3Nm** to connect the shank to the drive. Use thread locker (for example Loctite 243) to secure the screws.
#. Add **five M4x12mm screws** and tighten them with **2.1-2.3Nm** to connect the shank to the drive. Use threadlocker (for example Loctite 243) to secure the screws.


Mount the foot
~~~~~~~~~~~~~~

The mounting procedure of the foot is described in :ref:`optofeet`.

Mount the DAQ and shank pad
~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Attach the foot DAQ on the shank.
#. Plug the *foot sensor USB cable* into the DAQ.
#. Plug the *foot sensor anaglog cable* into the foot and into the foot DAQ.
#. Attach the shank pad with **five M3x4mm screws** to the shank and tighten them with **0.4-0.5Nm**. Use thread locker (for example Loctite 243) to secure the screws.

Disssembling a leg
------------------

Disassembling a leg is mainly the reverse process of assembling, as described in .. _assembling_leg:, starting with the foot and ending with the HAA drive. In the following section, the differences between the steps of assembling and disassembling are described.

Unmount the shank, thigh, and HFE drive holder
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For unmounting the shank, thigh, and HFE drive holder aluminum bodies from the output shaft of their drives, the following procedure has to be applied:
#. Remove the **five** (shank) or **ten** (thigh and HFE drive holder) **M4x12 screws**.
#. Insert two M4 screws in the two M4 unmounting threads in the aluminum body (see :numref:`fig_unmounting_thigh` for the thigh) to push the aluminum body out of the output thaft of its drive.

      .. _fig_unmounting_thigh:

      .. figure:: images/unmounting_thigh.jpg
        :scale: 100 %
        :alt: unmounting threads thigh
        :align: center

        M4 Unmounting threads in the thigh.
        

Adding / removing a leg from the main body
------------------------------------------

To add a leg to the main body, connect the HAA drive as described in :ref:`mounting_haa_drive` and connect the  *HAA to HFE EtherCAT cable*, the *48V power cable tree*, and the *foot sensor USB cable*. To remove a leg from the main body, disconnect the three cables and remove the HAA drive.
