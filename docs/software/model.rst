.. _model:

Model of *ANYmal*
=================

A model of ANYmal including kinematic and dynamic quantities is provided by the `quadruped\_model <https://bitbucket.org/leggedrobotics/quadruped_common>`__, which uses the `Rigid Body Dynamics Library (rbdl) <https://bitbucket.org/leggedrobotics/rbdl>`__ in the back end. The structure and parameters of the quadruped model are read from the `URDF <http://wiki.ros.org/urdf>`__ description which is constructed by the `xacro <http://wiki.ros.org/xacro>`__ files in `anymal\_description <https://bitbucket.org/leggedrobotics/anymal>`__. See section :ref:`URDF_model` for more information.

Coordinate Frames
-----------------

-  ``odom`` The state estimator estimates the pose of the robot with respect to this odometry frame. This frame is also called ``world`` in the controller.
-  ``map`` The localizer determines the pose of the robot with respect to this frame.
-  ``map_ga`` This frame is the map frame which is aligned with gravity.
-  ``base`` This is the coordinate frame that is attached to the torso of *ANYmal*.
-  ``base_inertia`` The inertial parameters are expressed in this frame which overlaps with the base frame. This hack was required to turn off some warning in Gazebo.
-  ``imu_link`` The data of the inertial measurement units (IMU) is expressed in this frame.
-  ``*_HIP`` This frame is located at the drive shaft of the ``HAA``-drive where the ``HFE``-drive is connected.
-  ``*_THIGH`` This frame marks the rotating side of the ``HFE``-drive at the connection of the thigh.
-  ``*_SHANK`` This frame marks the rotating side of the ``KFE``-drive at the connection of the shank.
-  ``*_ADAPTER`` This frame indicates the top of the shin and differs by a rotation of the z-axis from the ``*_SHANK``-frame.
-  ``*_MOUNT`` This frame indicates the end of the shank, where the contact sensor is mounted.
-  ``*_FOOT`` This is the end-effector frame, the tip of the foot, used for control.
-  ``footprint``  This frame is located at the center of the stance feet and is aligned with the midpoint of the front and hind feet. Therefore it is jumping.
-  ``feet_center`` This frame is located at the center of all the feet and is aligned  with the midpoint of the front and hind feet.
-  ``control`` This is the frame in which high-level velocities are defined. It is located at the odom frame, and rotated such that it is aligned with the heading direction of the torso and the orientation of the estimated ground plane.

ROS Transforms (TFs)
~~~~~~~~~~~~~~~~~~~~

The `TFs <http://wiki.ros.org/tf>`__ in ROS, which describe the state of the robot are broadcasted by the `quadruped\_tf\_publisher <https://bitbucket.org/leggedrobotics/quadruped_common>`__.

**TF Model in Rviz**

.. figure:: images/ANYmal_tf.png
   :alt: ANYmal tf

   ANYmal tf

**All TF Frames**

.. figure:: images/ANYmal_tf_tree.png
   :alt: ANYmal tf tree

   ANYmal tf tree

**TF Tree as a Graph** (click on the image to download the PDF)

|ANYmal tf frames|

.. _model_state:

State
-----

The state of the robot in the `quadruped\_model <https://bitbucket.org/leggedrobotics/quadruped_common>`__ consists of the **generalized coordinates (q)** and **velocities (u)**, which describe the state of the torso (main body) and the joints of the legs:

|ANYmal tf| More information about the symbols and the parameterization of the orientation is given in the `cheatsheet of kindr <http://docs.leggedrobotics.com/kindr/cheatsheet_latest.pdf>`__.

The **pose and velocities of the torso are estimated** by the state estimator with respect to the ``odom``/``world``/``inertial`` frame. Note that without LIDAR-based localization, the position and yaw angle of the torso are unobservable and hence drift!

The names and order of the **legs** are as follows:

-  ``LF`` left fore
-  ``RF`` right fore
-  ``LH`` left hind
-  ``RH`` right hind

The names and order of the **joints of a leg** are:

-  ``HAA`` hip adduction/abduction
-  ``HFE`` hip flexion/extension
-  ``KFE`` knee flexion/extension

The names and order of the **joints in the state** are:

-  ``LF_HAA``
-  ``LF_HFE``
-  ``LF_KFE``
-  ``RF_HAA``
-  ``RF_HFE``
-  ``RF_KFE``
-  ``LH_HAA``
-  ``LH_HFE``
-  ``LH_KFE``
-  ``RH_HAA``
-  ``RH_HFE``
-  ``RH_KFE``

**Zero-configuration, Inverse Kinematics and calibration**

When the joint angles are all zero, the hip, thigh, and shank frames have a null relative displacement along the heading direction of the base, i.e. along the x axis of the base frame. The adapter, mount and foot frame, however, do exhibit a displacement due to the fact the different feet (with different offsets for the frames) can be mounted.

To allow flexibility when changing the contact sensor,the analytical inverse kinematics take into account the relative pose between the ``MOUNT`` and the ``FOOT`` coordinate frames. For this reason, when calibrating the joint angles we must know the relative between the shank frame and the thigh frame. More precisely, when viewing the coordinates frames on the x-z plane, we must know the angle between (a) the line which connects the mount and the adapter frame, and (b) the line which connects the thigh and the shank frames.

.. figure:: images/ANYmal_joint_calibration.png
   :alt: ANYmal joint calibration

   ANYmal joint calibration

.. _URDF_model:

URDF Model
----------

The **URDF model** is generated by the `xacro <http://wiki.ros.org/xacro>`__ files which are in `anymal\_description <https://bitbucket.org/leggedrobotics/anymal>`__ and ``anymal_NAME_description``.

**anymal\_description** The common description is located in package ``anymal_description/urdf`` and is divided into folders for different rigid links. Each folder contains:

-  **anymal\_LINK\_parameter.urdf.xacro:** xacro:property definitions of the default link parameters.
-  **anymal\_LINK\_macro.urdf.xacro:** xacro:macro definition for the default link.
-  **anymal\_LINK.dae:** collada meshes for visualization.

These are all included in the main ``anymal.urdf.xacro`` file which is transformed and loaded onto the ROS parameter server when calling the ``launch/load.launch`` file.

**anymal\_NAME\_description** While the folder structure follows the default description package, the custom ``anymal.urdf.xacro`` file additionally includes the main ``anymal_description/urdf/anymal.urdf.xacro`` mentioned above.

The local folders with their **parameters**, **macros** and **meshes** can overwrite the default ones in ``anymal_description/urdf/anymal.urdf.xacro``.

**Example:** To add custom feet to the robot you can place your files in the ``urdf/foot/`` folder and adapt either the ``<xacro:include [...] anymal_foot_macro.urdf.xacro"/>`` or only the ``<xacro:include [...] anymal_foot_parameter.urdf.xacro"/>``.

**Attachments:** If the robot comes with additional modules like an actuated lidar or a Multisense S7 their urdfs are included at the bottom of the custom ``anymal_NAME_description/urdf/anymal.urdf.xacro``.
Runtime roslaunch arguments are defined in the default launch files for convenience to let the user decide which additional module to load in the model of *ANYmal*.
Note that depending on the different attachments the simulated mass will change and robot's the center of mass will shift.


Access to the State and Model
-----------------------------

Access by the Controller
~~~~~~~~~~~~~~~~~~~~~~~~

The *quadruped model* can be accessed by a robot controller `roco <https://bitbucket.org/leggedrobotics/roco>`__, for instance, in the ``advance()`` method as follows:

.. code:: cpp

    this->getState().getQuadrupedModelPtr()

Access in ROS
~~~~~~~~~~~~~

The state of the robot is published in a ``QuadrupedState.msg`` (`quadruped\_msgs <https://bitbucket.org/leggedrobotics/quadruped_common>`__) by the state estimator or the simulator under the topic ``/state_estimator/quadruped_state`` at control update rate and ``/state_estimator/quadruped_state_throttle`` with a reduced rate.

To initialize ``QuadrupedState.msg`` use the helper methods in `anymal\_description <https://bitbucket.org/leggedrobotics/anymal>`__

.. |ANYmal tf frames| image:: images/ANYmal_tf_frames.png
   :target: https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/software/documents/ANYmal_tf_frames.pdf
.. |ANYmal tf| image:: images/ANYmal_state.png
