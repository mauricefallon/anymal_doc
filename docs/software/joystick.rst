
.. _software_joystick:

Joystick
========

The **ANYJoy** framework in repository `any\_joy`_ provides tools to interface and manage multiple joypads to control a single robot at the same time. The following features are provided:

- Priority assignment when using multiple different joystick devices
- Dependency free plugins to listen at joystick commands.
- Multi-threaded features that allow high controller frequencies

More information can be found here: `http://docs.leggedrobotics.com/any_joy_doc <http://docs.leggedrobotics.com/any_joy_doc>`_. Read page `JoyManager`.

Joypad in GUI
-------------

The package ``rqt_joypad`` in repository `any\_joy`_  provides an RQT-based GUI plugin to simulate a joypad as shown in :numref:`fig_rqt_joypad`.


.. _fig_rqt_joypad:

.. figure:: images/rqt_joypad.png
     :scale: 100 %
     
     GUI plugin `rqt_joypad`
    

.. note::

    By double clicking on the circle with the mouse, the joystick axis is set back to the middle.
    
    
.. _any\_joy: https://bitbucket.org/leggedrobotics/any_joy