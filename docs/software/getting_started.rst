Getting Started
===============

This document provides an overview of the *ANYmal* software framework. If you require more detailed information about individual software packages, have a look at the *Readme* file of the software repository. A list of all repositories can be found in the appendix ":ref:`software_repositories`". Some software packages also provide a `Doxygen documentation`_, which is linked in the list of repositories.

An overview of meta-packages can be found in section ":ref:`software_overview`". The control framework ranging from system drivers up to the high-level controller is introduced in section ":ref:`software_locomotion_control`", and the navigation system including localization and mapping is described in ":ref:`navigation_stack`".

Get Support
-----------

Need help with the software? Try this:

-  Contact the maintainer of the software package (see the ``package.xml`` for the contact).
-  Write an e-mail to support-anymal@anybotics.com.


Section ":ref:`software_developing_guidelines`" provides some general guidelines on how to develop and improve the software, while section ":ref:`catkin_howto`" gives some instructions for the build system. Finally, section ":ref:`troubleshooting`" helps with the most common software errors and mistakes.

.. _software_requirements:

Requirements
------------

The *ANYmal* software framework is based on `ROS <http://www.ros.org>`__ and available as debian packages or source code. 
The following system and thirdparty libraries are used:

-  `Ubuntu`_, version 16.04 (LTS)
-  `GCC`_, version 5.4.0 or newer
-  `Boost`_, version 1.58.0
-  `Qt`_, version 5.5
-  `Eigen`_, version 3.3~beta1-2 (Official package of Ubuntu 16.04: libeigen3-dev)
-  `ROS`_, version Kinetic
-  `Gazebo`_, version 7.0.0

.. _Doxygen documentation: http://www.doxygen.org/
.. _docs.leggedrobotics.com: http://docs.leggedrobotics.com/
.. _Ubuntu: https://www.ubuntu.com
.. _GCC: http://gcc.gnu.org/
.. _Boost: http://www.boost.org/
.. _Qt: https://www.qt.io/
.. _Eigen: https://eigen.tuxfamily.org
.. _ROS: http://ros.org
.. _Gazebo: http://gazebosim.org
.. _catkin: http://wiki.ros.org/catkin
.. _Installation: _software_installation
.. _Software Packages: software_packages

Follow the instructions in the sections ":ref:`software_installation`" and ":ref:`software_simulation`" to set it up after reading section ":ref:`software_overview`".