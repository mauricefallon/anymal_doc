
.. _catkin_howto:

Catkin Howto
============

How to cleanly specify catkin package dependencies
--------------------------------------------------

Having defined the dependencies of a package is not only important for deploying your code on other computers, but not doing so can lead to
messy linker and include errors. For example if one of your package misses the definition of a system dependency such as Eigen, every other
package that depends on yours will fail to compile because it cannot find the header files (unless you hack it by calling ``find_package(Eigen3)`` in *every* package). The following description assumes that you are using the ``package.xml`` format version 2 by specifying ``<package format="2">`` at the top of the file.

Dependencies on Other Catkin Packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dependencies on other catkin packages have generally to be listed in three places:

-  In the ``package.xml`` file as ``<depend>PkgName</depend>``. See `this link <http://docs.ros.org/jade/api/catkin/html/howto/format2/catkin_library_dependencies.html>`__
   for a specification when you can use other dependency types such as ``build_depend``, ``build_export_depend`` or ``exec_depend``.
-  In the ``CMakeLists.txt`` file in ``find_package(catkin REQUIRED COMPONENTS PkgName ...)`` (if it is  required for building your package).
-  In the ``CMakeLists.txt`` file in ``catkin_package(... CATKIN_DEPENDS PkgName ...)`` (if it is required for executing your package)

System Dependencies
~~~~~~~~~~~~~~~~~~~

System dependencies (such as boost, eigen, ...) have to be specified in the ``package.xml`` with their rosdep key, which is usually the name of the library written in lower case. If you are unsure what the rosdep key is, you can try it out with ``rosdep resolve <dependency_key>`` or search `this <https://raw.githubusercontent.com/ros/rosdistro/master/rosdep/base.yaml>`__, `this <https://raw.githubusercontent.com/ros/rosdistro/master/rosdep/python.yaml>`__ or `this <https://raw.githubusercontent.com/ros/rosdistro/master/rosdep/ruby.yaml>`__ list. Commonly used keys are (everyone, please complete this list):

-  ``boost`` for the boost libraries
-  ``eigen`` for eigen library
-  ``libblas-dev`` for blas library

System dependencies can generally be specified as ``build_depend``. **But** if your package exports a header that includes a header of its dependency, then other packages that ``build_depend`` on yours will need that dependency too. To make that work correctly, declare the dependency as ``build_export_depend`` (see `this
link <http://docs.ros.org/jade/api/catkin/html/howto/format2/system_library_dependencies.html>`__ for details).

In the ``CMakeLists.txt`` file the dependencies have to be specified in ``catkin_package(DEPENDS PkgName)``, *after* they have been found with
``find_package(PkgName)`` or, if no cmake file is provided but a pkg-config file (which is usually the case if it was installed over ``apt``), with

.. code::

    find_package(PkgConfig REQUIRED)
    pkg_check_modules(PkgName REQUIRED)

Both of these methods should define ``<PkgName>_INCLUDE_DIRS`` and ``<PkgName>_LIBRARIES`` variables, whose content is then copied by the ``catkin_package(...)`` macro (see `this documentation <http://docs.ros.org/groovy/api/catkin/html/dev_guide/generated_cmake_api.html#catkin-package>`__). However, some of the system dependencies do not follow the above naming convention (e.g. Eigen, see "How ``find_package(..)`` (should) work" below), which breaks the ``catkin_package(DEPENDS ...)`` feature. If this is the case, the include directories and libraries of this dependencies have to be exported as if they were includes and libraries
of the package itself, e.g. ``catkin_package(INCLUDE_DIRS include ${EIGEN3_INCLUDE_DIR})``.

How ``find_package(..)`` (should) work
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``find_package(<PkgName>)`` cmake command in CMakeLists.txt file tries to find include and library directories of a given package. If it was successful, catkin expects the following two variables to be defined: ``<PkgName>_INCLUDE_DIRS`` and ``<PkgName>_LIBRARIES`` (if the package contains any libraries). Note that those variables are case sensitive, so trying to find a package named ``PkgName`` is not the same as trying to find ``pkgname``. Under the hood, cmake executes a
Find.cmake script, which is generally provided by the developer of the package, not by the developers of cmake. Unfortunately, not all developers stick to the naming convention of the above variables. E.g. Eigen provides a FindEigen3.cmake file, but the output variable is named ``EIGEN3_INCLUDE_DIR`` (note the missing 's' after DIR and the upper case of EIGEN). This is the reason why Eigen cannot be specified as a normal system dependency of a catkin package.

Catkin Install Space
--------------------

The install space, which is in the ``ìnstall/`` folder of your catkin workspace, can make deployment of your compiled software much easier: You just need to copy the install folder to the new computer, source it and the software from this workspace is ready to be executed (of course you still need to install dependencies on packages/libraries which are not inside this workspace). What will be copied to the install space is specified at the end of the packages' ``CMakeLists.txt``.

-  The install space should at least have all libraries and executables of your package. So every target, which was added with ``add_library(myLibrary ...)`` or with
   ``add_executable(myExecutable ...)`` should be listed in the following statement:

   .. code:: cmake

       install(TARGETS myLibrary myExecutable
         ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
         LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
         RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
       )

-  If you want to be able to write new code on the target machine which
   include header files of your package, you should also install the
   header files:

   .. code:: cmake

       install(DIRECTORY include/${PROJECT_NAME}/
         DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
         PATTERN ".svn" EXCLUDE
       )

-  If your package provides rosparam files, launchfiles and/or scripts (which should, according to the ros package standard, be located in
   ``param/``, ``launch/`` and ``scripts/`` subdirectories of your package), add the following:

   .. code:: cmake

       install(DIRECTORY
         param
         launch
         scripts
         DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
       )

-  If your package uses roscpp\_nodewrap (or is a nodelet), do not forget to export the ``nodelet_plugins.xml`` file as well:

   .. code:: cmake

       install(FILES
         nodelet_plugins.xml
         DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
       )

You must tell catkin to create the install space with the command ``catkin config --install``. Disabling the install space can be done with the ``--no-install`` flag.

Catkin Unit Tests
-----------------

Catkin introduces a cmake flag ``CATKIN_ENABLE_TESTING`` which defaults to ``ON``. Catkin/cmake will then try to find all dependencies, including test dependencies (``<test_depend>mypackage</test_depend>`` in ``package.xml``).

However it should still be possible to build the code without the having the test dependencies. Therefore the flag ``CATKIN_ENABLE_TESTING`` can
be set to ``OFF``.

The unit test part of the ``CMakelists.txt`` should look similar to the following.

   .. code:: cmake

    #############
    ## Testing ##
    #############
    if(CATKIN_ENABLE_TESTING)

      find_package(my_test_dependency REQUIRED)

      include_directories(
        tests/include
        ${my_test_dependency_INCLUDE_DIRS}
      )

      catkin_add_gtest(test_${PROJECT_NAME}
        tests/src/test_main.cpp
      )

      if(TARGET test_${PROJECT_NAME})
        target_link_libraries(test_${PROJECT_NAME}
          ${catkin_LIBRARIES}
          ${PROJECT_NAME}
          ${my_test_dependency_LIBRARIES}
        )
      endif()
    endif()

Every test dependency has to be found, included and linked seperately.

In this example the ``package.xml`` contains the line ``<test_depend>my_test_dependency</test_depend>``.
