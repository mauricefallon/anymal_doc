.. _state_estimation:

State Estimation
================

Overview
--------

 .. image:: images/state_estimation.png
     :scale: 100 %
     :align: left

Model-based controllers typically need the full state of the robot, including the pose and velocity of the torso of the robot.
The **state estimator** estimates these states by fusing joint information and kinematics with measurements from an inertial measurement unit (IMU). The state needs to be updated at the same rate the controller is running, which is 400Hz or higher.

The state estimator requires as minimal **input** the following measurements:

- positions and torques of the joints, and
- linear acceleration and angular velocity of the main body of the robot.

Optionally, the state estimator can process an external 6D pose measurement, which comes, for instance, from an :ref:`ICP_based_localization`.
Contact forces are another optional measurement to the state estimator, which in this case also recalibrates the force sensors in coordination with :ref:`software_locomotion_control`.

The state estimator **outputs** its results in a `quadruped state`, the most important of which are the following:

- The estimated **pose and twist of the main body** of the quadruped with respect to the :code:`odom` frame
- The **covariance of the pose and the twist** of the main body of the quadruped with respect to the :code:`odom` frame
- The **contact forces** expressed in :code:`odom` frame, either estimated or provided by calibrated measurements
- The **contact state** of the feet
- The **pose of the external pose measurement frame** with respect to the :code:`odom` frame
- The overall **confidence** in the estimated state

To codify the confidence level adressed in the last item, the `quadruped state` includes a **state** field. The following is a list of possible states and their meaning:

- **Sensor error:** Some measurement data must be wrong or timeouts occurred, this also includes complete loss of contact
- **Estimator error:** There is an issue within the estimator
- **Ok:** The estimated state can be trusted
- **Sensor pose warning:** The external pose measurements are enabled but no messages are received
- **Error unknown:** The estimator has not initialized yet

.. note::

    The state estimator does not directly broadcast any transformations via `TF`_. Instead, the `quadruped\_tf\_publisher`_ listens to the ``quadruped_msgs::QuadrupedState`` and transforms the poses into a `TF`_ message. Have a look at the overview in section ":ref:`navigation_stack`".


Usage
-----

In order to initialize, the estimator first needs to detect simultaneous contact of all feet. Note that the estimator waits two seconds after full contact before it changes the status of the estimated state to **Ok**. This ensures that the filter has converged and a controller can safely be run.

If all contacts are lost for for more then 0.1 seconds or any other error is detected from either sensors or the estimator itself, the estimator will change its state to one of the error states listed above. To return to an operational state, the filter then needs to be **reset** manually.


.. note::

.. caution::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **Do not reset the state estimator while a controller is running.**
  |
  |

   
Filters
-------

The ANYmal state estimator can be run using two different filters performing the actual sensor fusion. They are either based the Lightweight Filtering (LWF) or Two-State Information Filter (TSIF) framework. Both employ an estimation scheme that makes use of the fact that the point of contact with the ground is typically stationary with respect to some inertial coordinate frame [Bloesch12]_. To this end, the foot positions calculated from forward kinematics are treated as temporary landmarks in the :code:`odom` frame whenever the foot is in contact. The contact states necessary for this strategy are determined by either thresholding the contact force or the torque measured in the knee flexion/extension joint of the corresponding leg. If no direct contact force measurements are provided it is possible to estimate them using measured joint states.



.. note::

    Without any other exteroceptive sensors, the position and yaw angle (heading) of the robot with respect to the inertial frame are not directly observable, and thus drift over time.
  
.. note::
  
    The Two-State Information Filter is considered as the state-of-the-art filter.

 
The Lightweight Filter (LWF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Based on `lightweight\_filtering`_ and `lightweight\_filtering\_models`_, the ANYmal LWF implements the kinematics and IMU fusion strategy as an Extended Kalman Filter (EKF). The velocity error at the contact points is directly used as innovation term within the Kalman filter update step, and IMU measurements are used for state prediction together with a simple Mahalonobis based outlier detection scheme for slipping point contacts. This leads to a very accurate and robust estimation of the main body velocity and orientation.

.. note::
  
    Unlike the TSIF version, the ANYmal LWF does not process external pose measurements and will during operation default to the **Sensor pose warning** state whether the measurements are provided or not.

.. [Bloesch12] M. Bloesch, M. Hutter, M. Hoepflinger, S. Leutenegger, C. Gehring, C. D. Remy, and R. Siegwart. State Estimation for Legged Robots - Consistent Fusion of Leg Kinematics and IMU. In Robotics Science and Systems (RSS), pages 17-24, 2012.


The Two-State Information Filter (TSIF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The TSIF framework of the `two\_state\_information\_filter`_ package builds on the LWF and improves it in terms of flexibility. Instead of a standard EKF, the TSIF has at its core a more general recursive estimation algorithm as described in [Bloesch13]_. The ANYmal TSIF reimplements the familiar kinematics and IMU strategy in this framework and adds features such as a decoupled filter for the external pose measurements and outlier rejection for all measurements based on a Huber loss function.


.. [Bloesch13] M. Bloesch, M. Burri, H. Sommer, R. Siegwart and M. Hutter. The Two-State Implicit Filter - Recursive Estimation for Mobile Robots. IEEE Robotics and Automation Letters, volume 3, issue 1, pages 573-580, 2017.


Software
--------
A general interface for estimators using joint encoder and inertial measurements can be found in the `any\_state\_estimator`_ package, while the `anymal\_state\_estimator`_ and `anymal\_state\_estimator\_basic`_ packages provide an implementation for the estimator used on ANYmal.

The configuration files containg all the tuning parameters for the filter algorithms are located in the package `anymal\_b\_locomotion`_  (or a robot-specific package overriding the generic configuration).


.. _TF: https://wiki.ros.org/tf
.. _anymal\_b\_locomotion: https://bitbucket.org/leggedrobotics/anymal_b
.. _quadruped\_tf\_publisher: https://bitbucket.org/leggedrobotics/quadruped_common
.. _anymal\_state\_estimator: https://bitbucket.org/leggedrobotics/anymal_state_estimator
.. _any\_state\_estimator: https://bitbucket.org/leggedrobotics/any_common
.. _anymal\_state\_estimator_basic: https://bitbucket.org/leggedrobotics/anymal
.. _lightweight\_filtering: https://bitbucket.org/leggedrobotics/lightweight_filtering
.. _lightweight\_filtering\_models: https://bitbucket.org/leggedrobotics/lightweight_filtering_models
.. _two\_state\_information\_filter: https://github.com/leggedrobotics/two_state_information_filter
