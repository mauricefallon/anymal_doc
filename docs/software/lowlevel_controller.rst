.. _lowlevel_controller:

Low-Level Controller
====================

Overview
--------
The low-level controller communicates with the actuators (ANYdrives) and synchronizes the control sytem (see section ":ref:`time_sync_control_system`"). A state machine supervises the state of each actuator.

Its relevant data flow can be described as follows:

- Acquire readings from the ANYdrives and forward them to the high-level controller.
- Receive the actuator commands from the high-level controller and send them to the ANYdrives.

Conditional State Machine
-------------------------
The low-level controller features a conditional state machine (CSM) which is implemented as follows:

The robot has a state, as well as all of its essential sub-systems (ANYdrives).
Each robot state has a set of conditions for the state of the sub-systems.
If all of its conditions are fulfilled, the CSM allows to switch to a certain state.
As soon as the conditions are not fulfilled anymore, the CSM switches to the *Idle* state, which has no conditions and therefore can be entered under any circumstances.

Actions are special type of robot states, which are only temporary.
When an action is finished, the CSM tries to transition to the previous state.

The following table lists *ANYmal's* states:

.. _tab_anymal_states:

.. table:: States of ANYmal

    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Robot States                                                                | Required sub-system states conditions      |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Name        | Type   | Description                                          | IMU         | Feet        | ANYdrives      |
    +=============+========+======================================================+=============+=============+================+
    | Idle        | State  | Default state. Fallback state, if conditions         | none        | none        | none           |
    |             |        | of active state are not fulfilled anymore.           |             |             |                |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Operational | State  | The robot is fully operational. Commands from        | Device      | All devices | All devices    |
    |             |        | the high-level are sent to the ANYdrives.            | is          | are         | are in state   |
    |             |        |                                                      | operational | operational | *ControlOp*.   |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Fatal       | State  | If one or multiple drives enter the *Fatal*          | none        | none        | none           |
    |             |        | state (the output can rotate freely), the robot      |             |             |                |
    |             |        | enters the *Fatal* state. In there, all other        |             |             |                |
    |             |        | *ANYdrives* are commanded to enter the  *Standby*    |             |             |                |
    |             |        | state which makes them rotate freely as well.        |             |             |                |
    |             |        | This makes sure the robot collapses equally with     |             |             |                |
    |             |        | all joints which is more safe than a potential       |             |             |                |
    |             |        | fall to the side.                                    |             |             |                |
    |             |        |                                                      |             |             |                |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | ZeroJoint-  | State  | All ANYdrives are controlled to 0 joint torque.      | none        | none        | All devices    |
    | Torque      |        | This can be useful to check their gear/joint         |             |             | are in state   |
    |             |        | encoder offset calibrations.                         |             |             | *ControlOp*.   |
    |             |        |                                                      |             |             |                |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Actuators-  | Action | Send a clear errors command to all ANYdrives         | none        | none        | At least one   |
    | ClearErrors |        | which are in the *Error* state and in order to       |             |             | device is in   |
    |             |        | enter the *MotorOp* state.                           |             |             | state *Error*. |
    |             |        |                                                      |             |             |                |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Actuators-  | Action | Send a warm reset command to all ANYdrives which     | none        | none        | At least one   |
    | Warm-       |        | are in the *Fatal* state and in order to enter       |             |             | device is in   |
    | Reset       |        | the *WarmStart* state.                               |             |             | state *Fatal*. |
    |             |        |                                                      |             |             |                |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Actuators-  | Action | Disable all ANYdrives by commanding them to          | none        | none        | none           |
    | Disable     |        | enter the *Standby* state.                           |             |             |                |
    |             |        |                                                      |             |             |                |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | Actuators-  | Action | Enable all ANYdrives by commanding them to           | none        | none        | None of the    |
    | Enable      |        | enter the *ControlOp* state.                         |             |             | devices  is    |
    |             |        |                                                      |             |             | missing or     |
    |             |        |                                                      |             |             | in state       |
    |             |        |                                                      |             |             | *Error* or     |
    |             |        |                                                      |             |             | *Fatal*.       |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | GoDefault   | Action | Command all ANYdrives to enter the *default*         | none        | none        | All devices    |
    |             |        | joint position configuration.                        |             |             | are in state   |
    |             |        |                                                      |             |             | *ControlOp*.   |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | GoDock      | Action | Command all ANYdrives to enter the *dock* joint      | none        | none        | All devices    |
    |             |        | position configuration.                              |             |             | are in state   |
    |             |        |                                                      |             |             | *ControlOp*.   |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | GoRest      | Action | Command all ANYdrives to enter the *rest* joint      | none        | none        | All devices    |
    |             |        | position configuration.                              |             |             | are in state   |
    |             |        |                                                      |             |             | *ControlOp*.   |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
    | GoZero      | Action | Command all ANYdrives to enter the *zero* joint      | none        | none        | All devices    |
    |             |        | position configuration.                              |             |             | are in state   |
    |             |        |                                                      |             |             | *ControlOp*.   |
    +-------------+--------+------------------------------------------------------+-------------+-------------+----------------+
