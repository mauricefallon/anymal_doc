.. _software_repositories:

Software Repositories
=====================

The source code of the packages are available on

-  `Bitbucket - leggedrobotics`_,
-  `Github - leggedrobotics`_,
-  `Github - anybotics`_, and
-  `Github - ethz-asl`_.

Configurations
--------------

-  `anymal`_: Common code, msg/srv definitions, URDF etc. for ANYmal
-  `anymal\_b`_: Generic configurations for *ANYmal*
-  `anymal\_b\_drivers`_: Common launch files for drivers used on all robots.
-  `anymal\_bedi`_: Configurations for *ANYmal Bedi*
-  `anymal\_beth`_: Configurations for *ANYmal Beth*
-  `anymal\_boxy`_: Configurations for *ANYmal Boxy*
-  `anymal\_bonnie`_: Configurations for *ANYmal Bonnie*
-  `anymal\_navigation`_: Common config and launch files for navigation stack.

Model and Simulation
--------------------

-  `any\_gazebo`_: Generic Gazebo related packages
-  `quadruped\_common`_: Quadruped model, common code, msg/srv
   definitions, etc.
-  `romo`_: Generic robot model API (`API Docu <https://docs.leggedrobotics.com/romo_doc/>`_)
-  `rbdl`_: Fork of Rigid Body Dynamics Library used by
   ``romo_rbdl`` (clone with mercurial)
-  `series\_elastic\_actuator`_: Library for series-elastic
   actuators

Locomotion Control
------------------

-  `anymal\_ctrl\_free\_gait`_: Free Gait controller & adapter for
   *ANYmal* and basic Free Gait actions
-  `anymal\_ctrl\_staticwalk`_: Static walk controller
-  `anymal\_ctrl\_test`_: Test controller
-  `anymal\_ctrl\_trot`_: Trot controller
-  `anymal\_highlevel\_controller`_: Controller framework for *ANYmal*
-  `anymal\_lowlevel\_controller`_: Low-level controller for *ANYmal*
-  `free\_gait`_: Software library for the versatile control of
   legged robots
-  `loco`_: Locomotion control library (`API Docu <https://docs.leggedrobotics.com/loco_doc/>`_)
-  `roco`_: Robot Controller API (`API Docu <https://docs.leggedrobotics.com/rocoma_doc/>`_)
-  `rocoma`_: Robot controller manager library (`API Docu <https://docs.leggedrobotics.com/rocoma_doc/>`_)
-  `whole\_body\_control`_: Whole body control with hierarchical optimization framework

Estimation, Localization and Mapping
------------------------------------

-  `anymal\_state\_estimator`_: State estimator for *ANYmal*
-  `grid\_map`_: Universal grid map library for mobile robotic
   mapping
-  `elevation\_mapping`_: Terrain mapping library
-  `icp\_tools`_: ROS tools for ICP localization and mapping
-  `lightweight\_filtering`_: Library which provides basic
   functionalities for implementing EKF and UKF filters
-  `lightweight\_filtering\_models`_: Models for the
   lightweight\_filtering library
-  `two\_state\_information\_filter`_: Two State Estimation Filter library

Generic Tools and Libraries
---------------------------

-  `any\_common`_: Common tools
-  `any\_joy`_: Joystick manager (`API Docu <https://docs.leggedrobotics.com/any_joy_doc/>`_)
-  `any_ping_indicator`_: An Ubuntu indicator applet to show the ping status
-  `conditional\_state\_machine`_: Conditional State Machine
-  `cosmo`_: Communication Over Shared Memory On-demand library (`API Docu <https://docs.leggedrobotics.com/cosmo_doc/>`_)
-  `curves`_: Curve library (Splines)
-  `helper\_scripts`_: Collection of bash scripts
-  `kindr`_: Kinematics and Dynamics for Robotics (`API Docu <https://docs.leggedrobotics.com/kindr/>`_)
-  `kindr\_ros`_: ROS extension for kindr
-  `message\_logger`_: Message logger
-  `numopt`_: Numerical optimization toolbox
-  `parameter\_handler`_: Parameter handler (`API Docu <https://docs.leggedrobotics.com/parameter_handler_doc/>`_)
-  `robot\_utils`_: C++ utilities
-  `rqt\_multiplot`_: RQT-based plotting tool
-  `serial`_: Serial Communication Library
-  `signal\_logger`_: Signal logger (`API Docu <https://docs.leggedrobotics.com/signal_logger_doc/>`_)
-  `tcan`_: Threaded Communication and Networking library
-  `user\_interface`_: Generic user interface tools

Drivers
-------

-  `actuated_lidar`_: Manages the actuated LIDAR
-  `anydrive_sdk`_: Interface library for ANYdrive (`user manual <https://anybotics-anydrive-doc.readthedocs-hosted.com>`_, `API Docu <https://docs.leggedrobotics.com/anydrive_sdk_doc/>`_)
-  `dynamixel`_: Description of Dynamixel, ROS wrapper for libdynamixel unit
-  `ethz\_piksi\_ros`_: ROS drivers and tools for the Piksi Real Time Kinematic (RTK) GPS device
-  `hri-safe-remote-control-system`_: HRI's Safe Remote Control
-  `libdynamixel`_: C++-based library for interfacing Dynamixel servo motors
-  `librealsense`_: Driver for Intel RealSense devices
-  `libxsensmt`_: Xsens MT SDK software (driver)
-  `optoforce_omd`_: Driver and ROS wrapper for the OMD Optoforce sensor
-  `realsense`_: ROS Wrapper for Intel RealSense evices
-  `rpsm\_software`_: Robot Power and System Management
-  `xsensmt`_: C++ and ROS interfaces to libxsensmt

Thirdparty Software
-------------------

-  `eigen`_: Library for linear algebra: matrices, vectors,
   numerical solvers, and related algorithms
-  `gtest`_: Google's C++ test framework
-  `libnabo`_: A fast K Nearest Neighbour library for
   low-dimensional spaces
-  `libpointmatcher`_: A library implementing the Iterative Closest
   Point (ICP) algorithm for aligning point clouds
-  `pointmatcher-ros`_: ROS wrapper for libpointmatcher
-  `tinyxml`_: C++ XML parser



.. _Bitbucket - leggedrobotics: https://bitbucket.org/leggedrobotics/
.. _Github - leggedrobotics: https://github.com/orgs/leggedrobotics
.. _Github - anybotics: https://github.com/orgs/anybotics
.. _Github - ethz-asl: https://github.com/orgs/ethz-asl/
.. _anymal: https://bitbucket.org/leggedrobotics/anymal
.. _anymal\_b: https://bitbucket.org/leggedrobotics/anymal_b
.. _anymal\_b\_drivers: https://bitbucket.org/leggedrobotics/anymal_b_drivers
.. _anymal\_navigation: https://bitbucket.org/leggedrobotics/anymal_navigation
.. _anymal\_bedi: https://bitbucket.org/leggedrobotics/anymal_bedi
.. _anymal\_beth: https://bitbucket.org/leggedrobotics/anymal_beth
.. _anymal\_boxy: https://bitbucket.org/leggedrobotics/anymal_boxy
.. _anymal\_bonnie: https://bitbucket.org/leggedrobotics/anymal_bonnie
.. _any\_gazebo: https://bitbucket.org/leggedrobotics/any_gazebo
.. _any_ping_indicator: https://github.com/leggedrobotics/any_ping_indicator
.. _quadruped\_common: https://bitbucket.org/leggedrobotics/quadruped_common
.. _romo: https://bitbucket.org/leggedrobotics/romo
.. _rbdl: https://bitbucket.org/leggedrobotics/rbdl
.. _series\_elastic\_actuator: https://bitbucket.org/leggedrobotics/series_elastic_actuator
.. _anymal\_ctrl\_free\_gait: https://bitbucket.org/leggedrobotics/anymal_ctrl_free_gait
.. _anymal\_ctrl\_staticwalk: https://bitbucket.org/leggedrobotics/anymal_ctrl_staticwalk
.. _anymal\_ctrl\_trot: https://bitbucket.org/leggedrobotics/anymal_ctrl_trot
.. _anymal\_highlevel\_controller: https://bitbucket.org/leggedrobotics/anymal_highlevel_controller
.. _anymal\_lowlevel\_controller: https://bitbucket.org/leggedrobotics/anymal_lowlevel_controller
.. _free\_gait: https://github.com/leggedrobotics/free_gait
.. _loco: https://bitbucket.org/leggedrobotics/loco
.. _roco: https://bitbucket.org/leggedrobotics/roco
.. _rocoma: https://bitbucket.org/leggedrobotics/rocoma
.. _anymal\_state\_estimator: https://bitbucket.org/leggedrobotics/anymal_state_estimator
.. _grid\_map: https://github.com/ethz-asl/grid_map
.. _elevation\_mapping: https://github.com/ethz-asl/elevation_mapping
.. _icp\_tools: https://bitbucket.org/leggedrobotics/icp_tools
.. _lightweight\_filtering: https://bitbucket.org/leggedrobotics/lightweight_filtering
.. _lightweight\_filtering\_models: https://bitbucket.org/leggedrobotics/lightweight_filtering_models
.. _any\_common: https://bitbucket.org/leggedrobotics/any_common
.. _any\_joy: https://bitbucket.org/leggedrobotics/any_joy
.. _conditional\_state\_machine: https://bitbucket.org/leggedrobotics/conditional_state_machine
.. _curves: https://github.com/ethz-asl/curves
.. _helper\_scripts: https://bitbucket.org/leggedrobotics/helper_scripts
.. _kindr: https://github.com/ethz-asl/kindr
.. _kindr\_ros: https://github.com/ethz-asl/kindr_ros
.. _message\_logger: https://bitbucket.org/leggedrobotics/message_logger
.. _numopt: https://bitbucket.org/leggedrobotics/numopt
.. _parameter\_handler: https://bitbucket.org/leggedrobotics/parameter_handler
.. _robot\_utils: https://bitbucket.org/leggedrobotics/robot_utils
.. _rqt\_multiplot: https://github.com/ethz-asl/rqt_multiplot_plugin
.. _serial: https://github.com/leggedrobotics/serial
.. _signal\_logger: https://bitbucket.org/leggedrobotics/signal_logger
.. _tcan: https://bitbucket.org/leggedrobotics/tcan
.. _user\_interface: https://bitbucket.org/leggedrobotics/user_interface
.. _actuated_lidar: https://bitbucket.org/leggedrobotics/actuated_lidar
.. _optoforce_omd: https://bitbucket.org/leggedrobotics/optoforce_omd
.. _anydrive\_sdk: https://bitbucket.org/leggedrobotics/anydrive_sdk
.. _dynamixel: https://bitbucket.org/leggedrobotics/dynamixel
.. _hri-safe-remote-control-system: https://github.com/leggedrobotics/hri-safe-remote-control-system
.. _libdynamixel: https://bitbucket.org/leggedrobotics/libdynamixel
.. _libxsensmt: https://bitbucket.org/leggedrobotics/libxsensmt
.. _rpsm\_software: https://bitbucket.org/leggedrobotics/rpsm_software
.. _xsensmt: https://bitbucket.org/leggedrobotics/xsensmt
.. _eigen: eigen.tuxfamily.org
.. _gtest: https://github.com/google/googletest
.. _libnabo: https://github.com/ethz-asl/libnabo
.. _libpointmatcher: https://github.com/ethz-asl/libpointmatcher
.. _pointmatcher-ros: https://github.com/ethz-asl/pointmatcher-ros
.. _tinyxml: https://sourceforge.net/projects/tinyxml/
.. _cosmo: https://bitbucket.org/leggedrobotics/cosmo
.. _ethz\_piksi\_ros: https://github.com/ethz-asl/ethz_piksi_ros
.. _two\_state\_information\_filter: https://github.com/leggedrobotics/two_state_information_filter
.. _librealsense: https://github.com/leggedrobotics/librealsense
.. _realsense: https://github.com/leggedrobotics/realsense
.. _whole\_body\_control: https://bitbucket.org/leggedrobotics/whole_body_control
.. _anymal\_ctrl\_test: https://bitbucket.org/leggedrobotics/anymal_ctrl_test
