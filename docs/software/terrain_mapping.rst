
.. _software_terrain_mapping:

Terrain Mapping
===============

Robot-Centric Elevation Mapping
-------------------------------

.. figure:: images/elevation_mapping.png
    :scale: 80 %

    Elevation map showing valid footholds in blue and invalid footholds in red.

The software package `elevation\_mapping`_ is designed for (local) navigation tasks with robots which are equipped with a pose estimation (e.g. IMU & odometry) and a distance sensor (e.g. kinect, laser range sensor, stereo camera). The provided elevation map is limited around the robot and reflects the pose uncertainty that is aggregated through the motion of the robot (robot-centric mapping). This method is developed to explicitly handle drift of the robot pose estimation.

The `elevation\_mapping`_ package builds up on the `grid\_map`_ package. This package provides a library to manage universal two-dimensional grid maps with multiple data layers. With the help of `grid\_map`_, one can visualize, interpret, and transform elevation maps for further processing.

For more information about Elevation Mapping and Grid Map, refer to the `Elevation Mapping Readme <https://github.com/ethz-asl/elevation_mapping/blob/master/README.md>`_ and the `Grid Map Readme <https://github.com/ethz-asl/grid_map/blob/master/README.md>`_ and the publications [Fankhauser2014ElevationMapping]_ and [Fankhauser2016GridMap]_.

.. _elevation\_mapping: https://github.com/ethz-asl/elevation_mapping
.. _grid\_map: https://github.com/ethz-asl/grid_map

.. [Fankhauser2014ElevationMapping] P. Fankhauser, M. Bloesch, C. Gehring, M. Hutter, and R. Siegwart, "Robot-Centric Elevation Mapping with Uncertainty Estimates", in International Conference on Climbing and Walking Robots (CLAWAR), 2014. (`PDF <https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/154610/eth-8740-01.pdf>`_)
.. [Fankhauser2016GridMap] P. Fankhauser and M. Hutter, "A Universal Grid Map Library: Implementation and Use Case for Rough Terrain Navigation", in Robot Operating System (ROS) – The Complete Reference (Volume 1), A. Koubaa (Ed.), Springer, 2016. (`PDF <http://www.researchgate.net/publication/284415855>`_)
