
.. _software_developing_guidelines:

Developing Guidelines
=====================

Coding Practices & Workflow
---------------------------

In order to build quality software at scale we use the following tactics (parts taken from `here <https://blog.bitbucket.org/2014/12/01/bitbucket-building-high-quality-software-with-speed-and-scale/?atl_medium=ACE&atl_camp=ACE-486&atl_camptype=blog&atl_source=BB>`__).

Code Documentation & Consistency
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Write code that is self-documenting and as readable as possible. This is crucial for effective collaborative work on the same codebase and helps new starters get up to speed with the code base quickly. Structure your code to be flexible, so it is easy to change and re-use later.

As for documentation, we aim for multiple stages towards a ‘final’ product: First, each repository should be equipped with a markdown readme file that describes the content of itself and also links to the automated doxygen documentation. The Readme should then be expanded as described in `this template <https://github.com/ethz-asl/ros_best_practices/tree/master/ros_package_template>`__. In a next step a ``[repoName]_doc`` folder should be included that contains additional doxygen pages. Those could contain use-cases, how-to guides or highlevel connections to other packages.

We try to achieve consistent code by agreeing on a set of guidelines. Read about our `Programming Guidelines <https://leggedrobotics.github.io/styleguide/cppguide.html>`__ (especially the `Google C++ Guide <https://google.github.io/styleguide/cppguide.html>`__) and `Best Practices for ROS <https://github.com/ethz-asl/ros_best_practices/wiki>`__.

We use the C++11 standard.

Branching Model
~~~~~~~~~~~~~~~

We follow a branching model using Git that enables the use of pull requests for code reviews. Having a second pair of eyes looking at every
code change helps spread the knowledge within the team and spot potential problems early.

**Learn about Git in these** `tutorials <https://www.atlassian.com/git/tutorials/>`__. In particular, follow the methods described in

-  `Feature Branch Workflow <https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow>`__,
-  `Making a Pull Request <https://www.atlassian.com/git/tutorials/making-a-pull-request>`__,
-  `Use the Issue Tracker <https://confluence.atlassian.com/display/BITBUCKET/Use+the+issue+tracker>`__.

The Git repositories have the following default branches:

-  ``master`` - latest code, which is tested on a build server
-  ``release`` - stable code, which is tested on a build server and available in binary form of debian packages

Comprehensive Tests
~~~~~~~~~~~~~~~~~~~

Create unit tests, system-level integration tests and performance tests
to verify the functionality.

**Catkin helps you to setup unit tests with** `gtest <https://code.google.com/p/googletest/>`__:

-  `Setup unit tests with Catkin <http://wiki.ros.org/catkin/CMakeLists.txt#Unit_Tests>`__  (`example <https://github.com/ethz-asl/grid_map/tree/master/grid_map_lib/test>`__),
-  `Running unit tests <http://docs.ros.org/hydro/api/catkin/html/howto/format1/run_tests.html>`__,
-  `Integration tests with `rostest <http://wiki.ros.org/rostest>`__  (`example <https://bitbucket.org/leggedrobotics/anymal/src/2a4e77415adaa5e8f63b984056e28df3d4ce2153/anymal_sim/test/?at=master>`__).

Software needs to be also tested regularly on the real robot! We perform weekly shakeouts with the robot to measure our progress.

Separation of Algorithms and ROS-Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We use ROS for interprocess communication. It is a good practice to separate your algorithms from their ROS interface. This will ensure your algorithms can be used outside of ROS environments. The ROS interface can be added in a separate catkin package, e.g. by deriving from the classes in your core code. More information can be found in the `ROS best practices guidelines <https://github.com/ethz-asl/ros_best_practices/wiki#using-third-party-libraries>`__.

Examples on how to split core algorithm and ROS interface:

-  `grid\_map and grid\_map\_ros <https://github.com/ethz-asl/grid_map>`__
-  `loco and loco\_ros <https://bitbucket.org/leggedrobotics/loco>`__

Soft Real-Time Programming with Linux
-------------------------------------

In linux, processes and threads are handled in the same way. Thus, when talking about a thread or process, the same applies to the other as
well.

Linux has implemented different schedulers. By default, processes use a scheduler (``SCHED_OTHER``) which is based on ``nice values`` to select the process to run, but has priority 0. The nice values range from -20 to 20, where -20 is the highest priority and 0 the default. For more time-critical processes, it is recommended to use the ``SCHED_FIFO`` scheduler, which allows to set higher priorities (``rtprio``, normally 1 to 99, but this is implementation dependent, see `here <http://man7.org/linux/man-pages/man2/sched_get_priority_min.2.html>`__
and `here <http://man7.org/linux/man-pages/man2/sched_get_priority_max.2.html>`__). Processes with higher priorities (higher number) can preempt processes with lower priorities. The `any\_node <https://bitbucket.org/leggedrobotics/any_common/>`__
and `rtcontrol <https://bitbucket.org/leggedrobotics/rtcontrol/overview>`__ packages allow to set these priorities.

See `man sched <http://man7.org/linux/man-pages/man7/sched.7.html>`__ for details.

Enabling Higher Process Priorities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Normal users cannot set priorities higher than 0 by default.
To allow your linux user account to do so, you need to append the following entries to the ``/etc/security/limits.conf`` file (replacing ``<username>``):

.. code:: bash

    <username>       -       rtprio          99
    <username>       -       nice            -20

To allow your entire group to set higher priorities, append (replacing ``<groupname>``):

.. code:: bash

    @<groupname>     -       rtprio          99
    @<groupname>     -       nice            -20

Priority List
~~~~~~~~~~~~~

The following list is a guideline of what priorities a thread should be
given, depending on the type of task it is doing:

-  **90-99:** hardware I/O processes (very short and fast operations)
-  **80-89:** reserved
-  **70-79:** timing related processes (e.g. lowlevel controller, which triggers control updates)
-  **60-69:** reserved
-  **50-59:** processes closing control loop (e.g. locomotion controller)
-  **40-49:** reserved
-  **30-39:** other controller related processes (e.g. path planners)
-  **10-29:** reserved
-  **0- 9:** GUI processes
