.. _navigation_stack:

Navigation Stack
================
:numref:`fig_navigation_stack` shows a block diagram of the most important ROS nodes (rectangles) and topics (rounded blocks) on the navigation, locomotion and operator PC necessary for navigation.  

.. _fig_navigation_stack:

.. figure:: images/navigation_stack.png
    :width: 100%
    :alt: Navigation stack
    :align: center

    This block diagram shows the most important nodes (rectangles) and topics (rounded blocks) running on the operator, navigation and locomotion PC. 
    
    
State Estimator
---------------
The **State estimator** estimates the pose of the robot's main body in the *odom* frame. Note that the position and and yaw angle of the main body can drift over time since these quantities are not observable with measurements provided to the state estimator. The pose of the main body is provided as *TF* via the **Quadruped TF Publisher** and published on the topic ``state_estimator/pose_in_odom`` together with a covariance, which indicates the certainty of the estimate. See section ":ref:`state_estimation`" for more details.
    
Drivers
-------
Both the **LiDAR Unit** driver and the **Depth Camera** driver publish the latest 3D scan of the environment as a point cloud. The **Point Cloud Filter** filters the point cloud from the LiDAR by removing outliers and selecting only points in a given box.

ICP-based Localization & Mapping
--------------------------------
The ":ref:`software_icp_localization_and_mapping`" node maps the environment and localizes the robot using the *Iterative Closest Point* method. The localization requires an initial guess of the robot's pose in *map* frame. This initial guess can be set manually with the interactive marker in the 3D visualization of the environment. Otherwise, the localization takes automatically the pose provided by the state estimator as a guess to match the latest scan of the sensor with the reference map.


Terrain Mapping
---------------
For foothold planning, the local terrain of the robot is mapped using the measurements of the Depth Camera. The point cloud is fused together with the information of the state estimator to provide a robot-centric elevation map.


3D Visualization
----------------
The reference map of the ICP-based mapping tool and the terrain map can be visualized in 3D.