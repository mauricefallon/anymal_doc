
.. _software_icp_localization_and_mapping:

ICP-based Localization & Mapping
================================

.. _figpointcloud:

.. figure:: images/scan_and_point_cloud_map.png
    :scale: 80 %
    :align: center

    Reference (gray dots) and single scan (colored) point clouds

If the robot is equipped with an exteroceptive sensor, which generates 3D point clouds of the environment, a robust  **iterative closest point (ICP)** algorithm [Pomerleau13]_ can be used to localize the robot with respect to a reference map. The ICP algorithm searches for neighbor points between the two 3D point clouds of the single scan of the sensor and the map (see :numref:`figpointcloud`) and tries to minimize the sum of all their distances.

.. [Pomerleau13] F. Pomerleau. Applied registration for robotics. PhD thesis, ETH, 2013.

.. _ICP_based_localization:

ICP-based Localization
-----------------------

The software package ``icp_localizer`` provides the algorithm to localize the robot.

As input it needs

-  the reference point cloud (map, in the :code:`map` frame),
-  the single scan point cloud (in the :code:`torso` frame),
-  an initial guess for the 6D pose of the robot's torso (in the :code:`map` frame),

and it outputs

-  the 6D pose of the robot's torso (in the :code:`map` frame) and
-  the matched single scan point cloud (in the :code:`map` frame).

The initial guess has to be given only once, the following executions take *tf* or the last resulting pose as the initial guess, depending on the configuration.

ICP-based Mapping
-----------------

The software package ``icp_mapper`` creates or extends a map using the matched single scan point cloud.
The map can be saved to and and loaded from a ``.ply`` file.

As input it needs

-  the matched single scan point cloud (in the :code:`map` frame),

and it outputs

-  the reference point cloud (map, in the :code:`map` frame).

Usage
-----------------

The ICP-based localizer and mapper can be controlled through the RQT-based GUI ``rqt_icp`` as shown in :numref:`figrqticp`.

.. _figrqticp:

.. figure::  images/rqt_icp.png
    :scale: 100 %
    :align: center

    ICP GUI

-  Launch the ``icp_localizer`` and the ``icp_mapper`` ROS nodes by starting the navigation PC or navigation simulation launch script.
-  For simultaneous mapping and localization, follow these instructions:

   -  Optional: Load an existing map via the GUI or by setting the appropriate launch file argument.
   -  In order to start simultaneous mapping and localization, enable *localization*, *publish pose* and *mapping* in the GUI.
   -  Move your robot around to fill your map.
   -  When you are satisfied with your map, disable *mapping*.
   -  The new resp. extended map can be saved to file via the GUI.

-  For localization only, follow these instructions:

   -  Load an existing map via the GUI or by setting the appropriate launch file argument.
   -  In order to start localization, enable *localization*.
   -  Give ICP an initial guess by dragging the robot's interactive marker in RViz to the appropriate pose in the map. Right click and select *Set localization initial guess*. Enable *publish pose* in the GUI.
   -  You can see the resulting pose from ICP in RViz by enabling the according topic. If it is not correct, repeat the last step.

.. note::
  The ICP algorithm only converges to a local minimum.
  In order to converge to the correct solution, ICP requires that the errors of the initial guess of the robot’s pose are less than 1.5m in position and 30° in orientation.






