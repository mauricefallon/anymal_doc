
.. _troubleshooting:

Troubleshooting Information for Software
========================================

This section contains technical information that might be useful when you are trying to solve a problem.

Building Issues
---------------

The compiler only finds the header files of the overlayed package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the compiler only finds the header files of the overlayed package instead of the overlaying package, the order of the include directories in the ``CMakeLists.txt`` of your package might be wrong. Make sure the ``include`` folder of the package is always listed at the first place.

The **wrong way** is:

.. code::

    include_directories(
      ${EIGEN3_INCLUDE_DIR}
      ${catkin_INCLUDE_DIRS}
      include
    )

The **correct way** is:

.. code::

    include_directories(
      include
      ${EIGEN3_INCLUDE_DIR}
      ${catkin_INCLUDE_DIRS}
    )


Runtime Issues
--------------

Operation not permitted
~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    [ WARN] Failed to set thread priority for worker [updateWorker]: Operation not permitted

Your user does not have the rights to change the thread priority. Read section :ref:`software_developing_guidelines` to get more information about this topic.

`rocoma` does not find my controller
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get all plugins that are currently registered as rocoma plugins can be listed with the following command:

.. code:: bash

    rospack plugins --attrib=plugin rocoma_plugin

.. note:: Source the workspace before checking!

The catkin package of your controller needs to be **directly** depending on the package ``rocoma_plugin``. Check your ``package.xml``.


Gazebo crashes at startup
~~~~~~~~~~~~~~~~~~~~~~~~~

Gazebo 7.x sometimes crashes at startup due to a known bug in Gazebo itself. Restart the simulation.


Software hangs at startup
~~~~~~~~~~~~~~~~~~~~~~~~~

If a COSMO node crashes while it locked its shared memory, it might not be able to restart properly, since it waits indefinitely for the mutex to be released again.
To resolve this, one must clean COSMO's shared memory manually by deleting ``/dev/shm/COSMO_SHM``.


Symbol is undefined
~~~~~~~~~~~~~~~~~~~

If during runtime a ``symbol is undefined`` error happens, you can check the shared library dependencies with ``ldd``:

.. code:: bash

    ldd ~/catkin_ws/devel/lib/<package_name>/<executable_name>


Shared library is not found
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you get the following error

.. code:: bash

    </path/to/executable>: error while loading shared libraries: <library_name>.so: cannot open shared object file: No such file or directory

Try updating your shared library cache:

.. code:: bash

    sudo ldconfig /path/to/library/folder

For catkin libraries, this results in

.. code:: bash

    sudo ldconfig /opt/ros/kinetic/lib
