Free Gait
=========

.. figure:: images/free_gait_examples.jpg
    :width: 11cm
    :alt: Free Gait motion definition overview
    :align: center

    Free Gait application examples for footstep planning (stair climbing), motion planning (dynamic maneuvers), motion scripting (stand up / lie down), and tele-operation (crawling in a tunnel).

*Free Gait* is a software framework for the versatile, robust, and task-oriented control of legged robots. The Free Gait interface defines a *whole-body abstraction layer* to accomodate a variety of task-space control commands such as end effector, joint, and base motions. The deﬁned motion tasks are tracked with a feedback whole-body controller to ensure accurate and robust motion execution even under slip and external disturbances. The application of this framework includes intuitive tele-operation of the robot, efficient scripting of behaviors, and fully autonomous operation with motion and footstep planners.

.. figure:: images/free_gait_motion_definition_overview.jpg
    :width: 11cm
    :alt: Free Gait motion definition overview
    :align: center

    Free Gait motions are based on a combination of (possibly multiple) leg motions and a base motion per command (step). The base motion defines indirectly the motion of the support legs. Both leg and base motions can be defined a reference frame suitable to the task.

The Free Gait software for *ANYmal* is structured as follows:

-  `free\_gait`_: This repository holds the robot-independent core software of Free Gait. This includes the core algorithms, C++, Python, YAML, and ROS interfaces, RViz and RQT plugins, the action loader, and more.
-  `anymal\_ctrl\_free\_gait`_: This repository holds the Free Gait software for *ANYmal*. This includes the Free Gait ANYmal adapter, the `loco <https://bitbucket.org/leggedrobotics/loco>`_ controller to execute Free Gait actions, and several packages with Free Gait actions.

For more information about Free Gait, refer to the `Readme <https://github.com/leggedrobotics/free_gait/blob/master/README.md>`_ and the publication [Fankhauser2016FreeGait]_.

.. [Fankhauser2016FreeGait] P. Fankhauser, C. Bellicoso, C. Gehring, R. Dube, A. Gawel, and M. Hutter, "Free Gait – An Architecture for the Versatile Control of Legged Robots", in IEEE-RAS International Conference on Humanoid Robots (Humanoids), 2016. (`PDF <https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/121336/eth-49788-01.pdf>`_)
.. _`free\_gait`: https://github.com/leggedrobotics/free_gait
.. _`anymal\_ctrl\_free\_gait`: https://bitbucket.org/leggedrobotics/anymal_ctrl_free_gait
