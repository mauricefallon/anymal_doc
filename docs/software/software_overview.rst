.. _software_overview:

Software Overview
=================

A list of all repositories can be found in the appendix ":ref:`software_repositories`". The following section explains the meta-packages used for configuration and launch files to start the software.

Meta Packages
-------------

Since your specific *ANYmal* can differ from the standard platform, for instance, it is equipped with additional sensors, its model description and control parameters may need to be adapted accordingly. For this reason, there exists a dedicated git repository (e.g. ``anymal_beth``) for each *ANYmal* with its unique name, which contains all configurations.

The meta-packages and their dependencies for building and launching the software are described in :numref:`fig_anymal_repo_structure`.

.. _fig_anymal_repo_structure:

.. figure:: images/ANYmal_software_meta_packages.png
    :align: center

    The structure of the ANYmal specific repositories.
    
The packages in red in :numref:`fig_anymal_repo_structure` are stored in the dedicated repository of your ANYmal (``anymal_NAME``). 
The packages on the right side are used to build and launch the software in simulation and on the robot.
