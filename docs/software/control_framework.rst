.. _control_framework:

Locomotion Stack
================
:numref:`fig_control_framework` shows a block diagram of the most important ROS nodes (rectangles) and topics (rounded blocks) on the locomotion and operator PC necessary for locomotion control.

.. _fig_control_framework:

.. figure:: images/control_framework.png
    :width: 100%
    :alt: Control Framework
    :align: center

    This block diagram shows the most important nodes (rectangles) and topics (rounded blocks) running on the operator and locomotion PC. The solid lines indicate the data transmission over shared memory whereas the dashed lines display the ROS messaging.



Drivers
-------
The **Robot Power and System Management** provides information about the overall system like battery state and main body temperature. The :ref:`lowlevel_controller` publishes the readings of the leg actuators, whereas the **Inertial Measurement Unit (IMU)** provides inertial measurements for the :ref:`state_estimation`. The **HRI Remote Control** nodes publish the commands from the remote controller and manages the display of the joystick.


State Estimator
---------------
The **State Estimator** listens to the measurements from the :ref:`Low-Level Controller`_ and the IMU.  The estimator fuses the sensor information and  estimates the state of the main body of the robot. The output is the full robot state as described in the Section :ref:`state_estimation`. The results are available as ROS topics under the namespace ``/state_estimator/``.

High-Level Controller
---------------------
The **High-level Controller** executes and manages different walking controllers. Custom robot controllers can be loaded, started and switched. The output of the active controller is sent to the `Low-Level Controller`_, which does some additional safety checking before transmitting the commands to the actuators.

Joy Manager & Twist Mux
-----------------------
Multiple joysticks can be used simultaneously to operate the robot. The **Joy Manager** prioritizes the different joysticks and generates desired velocity and pose commands for the high-level controller. The velocity commands are further routed through the  **Twist Mux** before they reach the locomotion controller. The *Twist Mux* allows to switch between manual control of the robot by joystick and autonomous control via the navigation software.


TF Publishers
-------------
For visualization and navigation puposes, **TF Publishers** of the `ROS tf stack <http://wiki.ros.org/tf>`__ generate TF transforms of the state of the robot. To minimize the data transmission over the network, the state of the robot is published using minimal coordinates at a throttled rate and is available under the topic ``/tf_minimal``.

.. note::

  The topic ``/tf`` should not be used on the Operator PC to avoid high-bandwidth data transmission over the network.

GUI & 3D Visualization
----------------------
The low-level and high-level controller as well as the state estimator can be controlled over a graphical user interface ( `RQt <http://wiki.ros.org/rqt>`__). The state of the robot can be visualized via  `RViz <http://wiki.ros.org/rviz>`__.

.. _time_sync_control_system:

Data Transmission and Time Synchronization
-------------------------------------------
The data transmission between the processes on the locomotion PC is done over shared memory using the `COSMO`_ library instead of using the ROS publisher/subscriber mechanism. `COSMO`_ provides an interface to write data directly to shared memory and thus avoids any serialization of the data, which is necessary for the ROS communication. Note that the ROS communication is not reliable enough for update frequencies that are larger than 400Hz.

The update threads of the low-level controller, state estimator, and high-level controller are synchronized via shared memory using the utilities of the `COSMO`_ library as shown in :numref:`fig_timing`. The low-level controller sends a *sync* message to trigger the update method of the state estimator. As soon as the state estimator has computed the actual state, a  *sync* message is sent to the high-level controller, which computes the next actuator commands. The high-level controller can also be updated in parallel to the state estimator, however, this will increase the overall delay of the control system.

.. _fig_timing:

.. figure:: images/locomotion_control_timing.png
    :width: 100%
    :alt: Time synchronization
    :align: center

    Time synchronization of the low-level controller, state estimator, and high-level controller



.. _`COSMO`: https://docs.leggedrobotics.com/cosmo_doc/
