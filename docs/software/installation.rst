
.. _software_installation:

Software Installation
=====================

The *ANYmal* software framework is based on `ROS Kinetic <http://www.ros.org>`__ and tested under `Ubuntu 16.04 <https://www.ubuntu.com>`__.

`ROS <_installation_ros>`__ is required to build and run the *ANYmal* software framework.
The *ANYmal* software framework can then be installed either as `debian packages <_installation_anymal_debians>`__ or by `building from source <_installation_anymal_source>`__.

.. note:: It is recommended to install the *ANYmal* software framework as debian packages and then using the `catkin overlay mechanism <http://wiki.ros.org/catkin/Tutorials/workspace_overlaying>`__ for the packages you want to modify. This makes sure all required dependencies are installed automatically and reduces the amount of repositories you need to clone.

.. _installation_ros:

Install ROS
-----------

-  Install our ROS mirror repository:

    .. code:: bash

        sudo sh -c 'echo "deb [arch=amd64] http://packages.leggedrobotics.com/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/any_ros.list'
        sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 1DFF9FB9
        sudo apt update

.. note:: You can install ROS from the official repository as described in the `ROS wiki <http://wiki.ros.org/kinetic/Installation/Ubuntu>`__, however, interface changes may break our software. We therefore strongly recommend you to install ROS from our tested mirror.

-  It is recommended to install the full desktop version of ROS, the hector gazebo plugins and the multiplot plotting tool:

    .. code:: bash

        sudo apt install ros-kinetic-desktop-full
        sudo apt install ros-kinetic-hector-gazebo-plugins
        sudo apt install ros-kinetic-rqt-multiplot

-  Initialize rosdep:

    .. code:: bash

        sudo rosdep init
        rosdep update

-  It is convenient if the ROS environment variables are automatically added to your bash session every time a new shell is launched:

    .. code:: bash

        echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc


.. _installation_anymal_debians:

Install Debian Packages
-----------------------

The installation steps for the *ANYmal* software framework partially depend on whether you want to install the software for the standard or a specific *ANYmal*:

Standard *ANYmal*
~~~~~~~~~~~~~~~~~

-  Install the standard *ANYmal* repository:

    .. code:: bash

        sudo sh -c 'echo "deb [arch=amd64] http://anymal-sim:HvQbNJd349p1QvgQ1Rf9@packages.leggedrobotics.com/anymal-sim/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/anymal-sim.list'
        sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 1DFF9FB9
        sudo apt update

You can either install the basic simulation or the navigation simulation:

-  Install the basic simulation:

    .. code:: bash

        sudo apt install ros-kinetic-anymal-b-sim

-  Install the navigation simulation:

    .. code:: bash

        sudo apt install ros-kinetic-anymal-b-navigation-sim

Specific *ANYmal*
~~~~~~~~~~~~~~~~~

-  Install the specific *ANYmal* repository (e.g. *ANYmal beth*) replacing the following placeholders:

    -  ``USER``: User name
    -  ``PASSWORD``: Password
    -  ``NAME``: Name of your *ANYmal* (e.g. ``beth``)

    .. code:: bash

        sudo sh -c 'echo "deb [arch=amd64] http://USER:PASSWORD@packages.leggedrobotics.com/anymal-NAME/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/any.list'
        sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 1DFF9FB9
        sudo apt update

You can either install the basic simulation or the navigation simulation:

-  Install the basic simulation:

    -  ``NAME``: Name of your *ANYmal* (e.g. ``beth``)

    .. code:: bash

        sudo apt install ros-kinetic-anymal-NAME-sim

-  Install the navigation simulation:

    -  ``NAME``: Name of your *ANYmal* (e.g. ``beth``)

    .. code:: bash

        sudo apt install ros-kinetic-anymal-NAME-navigation-sim

.. _installation_anymal_source:

Build from Source
-----------------

Prerequisites
~~~~~~~~~~~~~

In order to clone and build the *ANYmal* source code, you need to install the following libraries:

.. code:: bash

    sudo apt install git mercurial libblas-dev xutils-dev gfortran libf2c2-dev libgoogle-glog-dev

The `Catkin Command Line Tools <https://catkin-tools.readthedocs.io>`__ provide useful features for the development of catkin packages.
Follow the `instructions <https://catkin-tools.readthedocs.io/en/latest/installing.html>`__ to install them.
If you installed our ROS mirror, make sure you do not add the official ROS mirror during the installation.

Set up *SSH*
~~~~~~~~~~~~

To clone the repositories from `Bitbucket <www.bitbucket.org>`__ or `Github <www.github.com>`__, we recommend to use *SSH*. The following pages explain how to set up *SSH* on your machine:

-  `How to set up SSH for Bitbucket <https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html>`__
-  `How to set up SSH for Github <https://help.github.com/articles/generating-an-ssh-key/>`__

Create folders
~~~~~~~~~~~~~~

Create folders to prepare your workspace:

.. code:: bash

    mkdir -p ~/git ~/catkin_ws/src

.. note:: We recommend the practice of cloning all repositories into a dedicated ``git`` folder and only symbolicaly linking them into the catkin workspace ``catkin_ws/src`` if needed. This has the advantage, that repositories do not need to be cloned again if additional workspaces are set up, they only need to be linked another time.

Set up the catkin workspace
~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Initialize the catkin workspace:

   .. code:: bash

       source /opt/ros/kinetic/setup.bash
       cd ~/catkin_ws
       catkin init

   .. note:: You need to install `Catkin Command Line Tools <https://catkin-tools.readthedocs.io>`__ to execute the aforementioned command.

-  Set the workspace's cmake build type to ``Release`` (the default is ``Debug``):

   .. code:: bash

       catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release

.. note:: The new catkin workspace extends the workspaces which were sourced at the time ``catkin init`` is executed. You can check the workspace configuration with ``catkin config``.

Clone the source code
~~~~~~~~~~~~~~~~~~~~~

If you installed the *ANYmal* software framework using debian packages, you only need to clone and build the repositories which you want to modify. If this is not the case, all repositories containing dependencies have to be cloned and built from source.

A list of our meta-packages can be found in the :ref:`software_overview`.

-  To clone a single repository and link it to your workspace, run the following commands:

    -  ``REPOSITORY_NAME``: Name of the repository you want to add to your workspace, e.g. ``anymal_b``.

   .. code:: bash

       cd ~/git
       git clone git@bitbucket.org:leggedrobotics/REPOSITORY_NAME.git
       ln -s ~/git/REPOSITORY_NAME ~/catkin_ws/src

-  To clone all repositories required by one of our meta-packages, you can use the contained ``clone_deps.sh`` script which clones all repositories containing dependencies:

    -  ``REPOSITORY_NAME``: Name of the repository containing ``METAPACKAGE_NAME``, e.g. ``anymal_b``
    -  ``METAPACKAGE_NAME``: Name of the meta-package you want to build, e.g. ``anymal_b_sim``

   .. code:: bash

       cd ~/git
       git clone git@bitbucket.org:leggedrobotics/REPOSITORY_NAME.git
       ./REPOSITORY_NAME/METAPACKAGE_NAME/bin/clone_deps.sh
       ln -s ~/git/* ~/catkin_ws/src

.. note:: Some software is only available as debian packages but the ``clone_deps.sh`` script will try to clone its source code. If a package dependency is missing when building please add the apt key as described in :ref:`installation_anymal_debians` and then install it with:

  .. code:: bash

    sudo apt install ros-kinetic-PACKAGE-NAME

  For users that build the generic simulation from source this needs to be done for both ``rqt-rpsm`` and ``rqt-anymal-lowlevel-controller``.


.. note:: It is recommended to work with the ``release`` branches of the software repositories. They contain tested and stable code that is also available in binary form in the debian packages. The ``master`` branches of the software repositories contain the latest development code, which might be unstable. Both type of branches are built and tested on our build server.

.. note:: To simplify working with multiple repositories, have a look at our `helper scripts <https://bitbucket.org/leggedrobotics/helper_scripts>`__. Common procedures, such as updating repositories or checking out branches can be processed for the entire ``git`` folder. Another helpful tool is `smartgit <http://www.syntevo.com/smartgit/>`__.

Build the source code
~~~~~~~~~~~~~~~~~~~~~

To build a catkin package, e.g. ``anymal_b_sim``, run:

   .. code:: bash

       catkin build anymal_b_sim
