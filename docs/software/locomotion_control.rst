
.. _software_locomotion_control:

Locomotion Control
==================

Overview
--------

The **high-level controller** (`anymal\_highlevel\_controller`_) executes the controllers and manages them through the **Robot Controller Manager** (`rocoma`_), which uses the **Robot Controller Interface** (`roco`_). Read the extensive `documentation of rocoma`_.

The *Robot Controller Manager* requires all controllers to be templated on the state and command of *ANYmal*, which are stored in package `anymal\_roco`_. The manager provides a plugin mechanism (``rocoma_plugin``) which loads all controllers at startup. Once you have built your own controller plugin, you need to tell the *high-level controller* (`anymal\_highlevel\_controller`_) to load it. Therefore you need to add your controller to the list in ``anymal_NAME/PACKAGE/config/locomotion_controllers/controllers.yaml`` (PACKAGE being either your simulation, anymal_NAME_sim, or the locomotion PC, anymal_NAME_lpc).

As a reference, have a look at the following controllers:

-  `anymal\_ctrl\_trot:`_ This controller walks blindly (with no exteroceptive sensing) using a walking trot gait. Its main input is a body velocity command. It can reach faster velocities than a static gait, while being able to handle some roughness and slopes in simulation.
-  `anymal\_ctrl\_staticwalk:`_ This controller walks blindly using a statically stable walking gait. Its main input is a body velocity command. It is more robust against disturbances and rough terrain than the trot, thanks to its static stability.

-  `anymal\_ctrl\_free\_gait:`_ This controller utilizes the `free gait interface`_ to perform versatile motions. It supports desired body postures and leg trajectories as inputs. Moreover, it can be used together with exteroceptive sensing to handle more complex terrains.

A script to automate the generation of controller packages is provided in `roco`_. The usage is similar to the usual ``catkin_create_pkg`` command.

Example: The following will generate a package named ``my_controller`` with dependency on the ``anymal_roco`` package in the src directory of your catkin workspace. The class containing the controller implementaion is named ``MyController`` with state class :cpp:`anymal_roco::RocoState` and command class :cpp:`anymal_roco::RocoCommand`. It is an emergency controller with ROS features enabled.

.. code:: bash

    ./catkin_create_roco_pkg my_controller anymal_roco -p ~/catkin_ws/src -c "MyController" --state_package "anymal_roco" --is_ros --is_emergency -a "Your Name" -e "your@email.com"





Considerations regarding ROS
----------------------------

We recommend you to consider the following points:

-  Be careful with using ROS in a controller because ROS is not meant to be real-time.
-  Make sure that all ROS publishers, service clients, etc. are properly shut down in the cleanup method of the controller. -  Try to avoid publishing ROS messages in the :cpp:`advance()` method of a controller. Use a separate worker instead.
-  Do not use `TF`_ in the controller.

Tuning and Debugging
--------------------

To tune and debug a controller, use the following tools:

`message\_logger`_
~~~~~~~~~~~~~~~~~~

This packages provides an interface to log text messages. If your library is built within ROS, it uses the `ROS logging`_ mechanism. Otherwise it outputs the messages to :cpp:`std::cout`.

`signal\_logger`_
~~~~~~~~~~~~~~~~~

If you want to investigate the values of your variables within your controller at each control update, then the signal logger is the right tool. You can simply add your variables to the signal logger and after each call of the :cpp:`advance()` method of the controller, the values of the variables are automatically collected and stored. The signal logger is able to store the logging data in a binary file that can be read in `Matlab`_ or it can store it in a `ROS bag`_ file. Moreover it can publish the data through ROS in order that the signals can be inspected in real-time. We recommend to use our C++ plotting tool `rqt\_multiplot`_ to plot the signals instead of using the python-based `rqt\_plot`_ tool of ROS. Check out the `documentation of the signal logger`_.

The `Matlab`_ script ``quadruped_logging/signal_logger/main.m`` plots the most relevant data from the binary file, whereas the configuration files in ``quadruped_logging/multiplots/`` help you to to visualize the data with `rqt\_multiplot`_.

`parameter\_handler`_
~~~~~~~~~~~~~~~~~~~~~

To quickly change parameters or gains of your controller while the controller is running, you should use the parameter handler. A GUI (``rqt_parameters``) gives you the possibility to change the values in an easy way. Check out the `documentation of the parameter handler`_.


High-Level Commands
-------------------

To steer the robot, high-level velocity or pose commands can be sent to the controller through ROS. The high-level controller (`anymal\_highlevel\_controller`_) listens to the following topics:

-  ``/commands/pose`` pose commands
-  ``/commands/twist`` velocities commands
-  ``/commands/joy`` joystick commands

The `joy\_manager`_ automatically translates the joystick commands to pose and twist messages. It scales the joystick axes with the minimal and maximal values published to the following topics:

-  ``/commands/pose_min`` min. pose
-  ``/commands/pose_min`` max. pose
-  ``/commands/twist_min`` min. velocities
-  ``/commands/twist_max`` max. velocities



.. _anymal\_highlevel\_controller: https://bitbucket.org/leggedrobotics/anymal_highlevel_controller
.. _roco: https://bitbucket.org/leggedrobotics/roco
.. _rocoma: https://bitbucket.org/leggedrobotics/rocoma
.. _documentation of rocoma: http://docs.leggedrobotics.com/rocoma_doc
.. _anymal\_roco: https://bitbucket.org/leggedrobotics/anymal
.. _`anymal\_ctrl\_staticwalk:`: https://bitbucket.org/leggedrobotics/anymal_ctrl_staticwalk
.. _loco: https://bitbucket.org/leggedrobotics/loco
.. _`anymal\_ctrl\_trot:`: https://bitbucket.org/leggedrobotics/anymal_ctrl_trot
.. _`anymal\_ctrl\_free\_gait:`: https://bitbucket.org/leggedrobotics/anymal_ctrl_free_gait
.. _free gait interface: https://github.com/leggedrobotics/free_gait
.. _TF: http://wiki.ros.org/tf
.. _message\_logger: https://bitbucket.org/leggedrobotics/message_logger
.. _ROS logging: http://wiki.ros.org/roscpp/Overview/Logging
.. _signal\_logger: https://bitbucket.org/leggedrobotics/signal_logger
.. _Matlab: https://www.mathworks.com/products/matlab.html
.. _ROS bag: http://wiki.ros.org/rosbag
.. _rqt\_multiplot: https://github.com/ethz-asl/rqt_multiplot_plugin
.. _rqt\_plot: http://wiki.ros.org/rqt_plot
.. _documentation of the signal logger: http://docs.leggedrobotics.com/signal_logger_doc/
.. _parameter\_handler: https://bitbucket.org/leggedrobotics/parameter_handler
.. _documentation of the parameter handler: http://docs.leggedrobotics.com/parameter_handler_doc/
.. _joy\_manager: https://bitbucket.org/leggedrobotics/any_joy
