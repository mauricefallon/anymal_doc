
.. _software_simulation:

Simulation
==========

Overview
~~~~~~~~

The simulation of *ANYmal* is based on `Gazebo <http://gazebosim.org/>`__.

There are two kind of simulation environments, the basic simulation (:numref:`fig_basic_simulation`) for testing locomotion controllers  and the navigation simulation (:numref:`fig_navigation_simulation`), which adds perception sensors, mapping and localization. Both simulation environments exist for the standard *ANYmal* as well as a specific *ANYmal* (e.g. *ANYmal beth*).

.. _fig_basic_simulation:

.. figure:: images/basic_simulation.png

    The basic simulation environment for testing locomotion controllers.

.. _fig_navigation_simulation:

.. figure:: images/navigation_simulation.png

    The navigation simulation environment for testing perception, mapping and localization.

Preparations to Run the Simulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Source the global ROS workspace:

    .. code:: bash

        source /opt/ros/kinetic/setup.bash

-  If you have built code from source, also source the local catkin workspace:

    .. code:: bash

        source ~/catkin_ws/devel/setup.bash

.. note:: You need to build at least one package, otherwise the `devel` folder does not exist.

-  In case you just installed or built new rqt plugins, run rqt once to make it discover them:

    .. code:: bash

        rqt --force-discover

Run the Simulation
~~~~~~~~~~~~~~~~~~

To start the simulation using the standard *ANYmal*, run:

.. code:: bash

    roslaunch anymal_b_sim sim.launch

This command launches the following software:

-  The headless `Gazebo <http://gazebosim.org/>`__ (physics simulation engine, including sensors and actuators),
-  the :ref:`software_locomotion_control` software stack (robot controller enabling locomotion),
-  the :ref:`software_joystick` manager (handling multiple joysticks with different priorities),
-  the `TF <http://wiki.ros.org/tf>`__-publisher (generating transformations from the quadruped state, e.g. for rviz),
-  the GUI (including `rviz <http://wiki.ros.org/rviz>`__ and `rqt-based <http://wiki.ros.org/rqt>`__ plugins),

.. note:: Gazebo downloads some models from the internet when you start it for the first time. This might take a few seconds.

To start the navigation simulation using the standard *ANYmal*, run:

.. code:: bash

    roslaunch anymal_b_navigation_sim sim.launch

This launches the navigation simulation environment which launches the following software additionally to the standard simulation:

-  the :ref:`software_terrain_mapping` software (navigation simulation only), and
-  the :ref:`software_icp_localization_and_mapping` tools (navigation simulation only).


By replacing ``anymal_b`` with ``anymal_NAME``, the user can start a specific *ANYmal* simulation.


Run the Barebone Simulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To start the barebone *ANYmal* Gazebo simulation, run:

.. code:: bash

    roslaunch anymal_gazebo anymal_gazebo.launch

.. note:: This command does not start a graphical user interface. Also, it loads the standard model of *ANYmal* and not a specific one.

To learn more about the Gazebo plugin refer to the package `readme <https://bitbucket.org/leggedrobotics/anymal/src/master/anymal_gazebo>`__.
