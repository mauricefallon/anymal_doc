.. _rpsm_b200:

Robot Power and System Management
=================================

:numref:`fig_rpsm_overview_b200` shows the diagram of the `Robot Power and System Management` (RPSM).
All components managed by the RPSM are listed in :numref:`tab_rpsm_b200`.

.. _fig_rpsm_overview_b200:

.. figure:: images/rpsm_overview.png
     :width: 18cm
     :alt: rpsm overview
     :align: center

     Diagram of the Robot Power and System Management (RPSM)

.. _tab_rpsm_b200:

.. table:: Components of the Robot Power and System Management (RPSM)

  ================  ========================
  Component          Section
  ================  ========================
  Battery Pack      :ref:`battery_pack_b200`
  Mainboard         :ref:`mainboard_b200`
  Cooling Fans      :ref:`cooling_fans_b200`
  Actuation Boards  :ref:`actuation_boards_b200`
  RC Receiver       :ref:`remote_controller`
  Router            :ref:`network_b200`
  Locomotion PC     :ref:`onboard_computers_b200`
  Navigation PC     :ref:`onboard_computers_b200`
  Application PC    :ref:`onboard_computers_b200`
  Joint Actuators   :ref:`joint_actuators`
  IMU               :ref:`imu`
  LIDAR Unit        :ref:`velodyne_b200`
  Audio             :ref:`microphone_speaker`
  RTK GPS           :ref:`rtk_gps`
  ================  ========================


.. _mainboard_b200:

Mainboard
---------

The `mainboard` is the central unit of the robot, which monitors and controls all devices of the robot. The `mainboard` is connected to the `Locomotion PC` via serial bus.


.. _actuation_boards_b200:

Actuation Boards
----------------

The `Actuation Board` enables the switching of high side currents to demanding loads, which would be normally connected directly to the onboard battery. This makes the power consumption of actuators switchable and thus the system more flexible to react on environmental circumstances like low battery voltages, erroneous states and mission planned robot states like sleep modes.


.. _cooling_fans_b200:


Cooling Fans
------------

.. _fig_coolingfan_b200:

.. figure:: images/cooling_fan.jpg
    :scale: 20%
    :align: center

    Main cooling fan



There are several cooling fans inside the main body for air circulation and one at the top of the robot as shown in :numref:`fig_coolingfan_b200` for cooling the overall system.

.. caution::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **Do not operate the robot if the cooling fan is not working. The system may overheat and components may get damaged.**


Temperatures inside the robot
-----------------------------

:numref:`tab_temperatures_b200` lists typical and maximum operating temperatures of *ANYmal*.
Typical values are only rough references for 25°C ambient temperature without direct sun radiation and slow walking/trotting.
Detailed temperatures for the *ANYdrive* can be found in the user manual of the *ANYdrive*.

.. _tab_temperatures_b200:

.. table:: Temperatures of *ANYmal*

  ==================  =============   ==========
  Location            Typical Temp.   Max. Temp.
  ==================  =============   ==========
  Inside main body    50°C            60°C
  CPU                 60°C            72°C
  Battery             50°C            60°C
  Thigh surface       50°C            60°C
  HFE cooler surface  50°C            60°C
  ==================  =============   ==========


RPSM Software
-------------

On each onboard computer, a ROS node (``rpsm_lpc``, ``rpsm_npc``, etc.) is running in the background as a daemon. The node ``rpsm_lpc`` on the `Locomotion PC` provides information about the `RPSM` because it communicates with the :ref:`mainboard_b200` over a serial bus. The RQT-based GUI ``rqt_rpsm`` visualizes the data and provides actions to interact with the `RPSM`.


The software for interfacing the RPSM is in the follwing repository:

-  `rpsm\_software`_: Software interface for the Robot Power and System Management



.. _rpsm\_software: https://bitbucket.org/leggedrobotics/rpsm_software
