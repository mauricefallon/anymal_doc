.. _velodyne_b200:

LiDAR Unit
==========

The LiDAR Unit consists of a Velodyne Puck LITE, which is able to acquire 3D point clouds at a high rate.
The `specification sheet <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b200/documents/velodyne_vlp16_spec_sheet.pdf>`__ and
the `user manual and programming guideline <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b200/documents/velodyne_vlp16_user_manual_and_programming_guide.pdf>`__ contain information about its usage.
More documentation can be found on the `Velodyne Puck LITE website <http://velodynelidar.com/vlp-16-lite.html>`__.

:numref:`fig_velodyne` shows the device installed on the top hatch of *ANYmal*.

.. _fig_velodyne:

.. figure:: images/velodyne.png
   :width: 15cm
   :alt: velodyne
   :align: center

   Velodyne Puck LITE mounted on ANYmal.

.. warning::
 .. image:: ../images/iso_7010/warning_general.png
   :scale: 50 %
   :align: left

 **The battery ground is exposed at the chassis of the Velodyne Puck LITE!**
 **Do not touch or make contact with its surface.**


Installation
------------

#. Mount the Velodyne Puck LITE on the top hatch of *ANYmal* using four M3 x 8 mm screws and appropriate washers.

#. Plug in the 12 V power cable in the appropriate socket (labelled 12 V) in the middle of *ANYmal*'s top plate.

#. Plug in the Ethernet cable in the appropriate socket (labelled NPC) in the middle of *ANYmal*'s top plate to connect it to the navigation PC.

#. To check if the Velodyne Puck LITE is running and connected, log into the navigation PC and check the output of the following command (The device's IP is listed in the datasheet that comes long with your *ANYmal*.):

    .. code:: bash

        ping <VELODYNE_IP>

#. You can configure the Velodyne Puck LITE over the device's web-interface by logging into the navigation PC using the X-Forwarding option of `SSH <https://wiki.ubuntuusers.de/SSH/>`__ and starting a web-browser.

    .. code:: bash

        ssh integration@anymal-NAME-npc -X
        firefox <VELODYNE_IP>

Calibration
-----------

In order to calibrate the pose of the Velodyne Puck LITE on your *ANYmal*, you can modify the calibration transformation in :code:`anymal_NAME_description/urdf/anymal.xacro.urdf`.
If the device's mounting is perfectly matching the CAD, the calibration transformation is identity.

Software
--------

The :code:`anymal_velodyne` package in the `anymal_navigation <https://bitbucket.org/leggedrobotics/anymal_navigation>`__ repository provides a launch file which connects to the Velodyne Puck LITE.
It uses the `velodyne_pointcloud <http://wiki.ros.org/velodyne_pointcloud>`__ package from ROS.
