Operation
=========

.. important::

  .. image:: ../images/iso_7010/mandatory_manual.png
    :scale: 50%
    :align: left

  **Before installing or operating ANYmal carefully read section** ":ref:`safety_instructions_b200`" **.**

  |

Basic Operating Instructions
----------------------------

Turning on *ANYmal*
~~~~~~~~~~~~~~~~~~~

#. **Activate ANYmal:** To wake up *ANYmal*, press the `PWR button` and hold it. *ANYmal* will beep and the button will illuminate and start pulsing with an interval of one second. Hold the button **5 seconds** until the light starts flashing faster.
#. **Activate the HRI's Joystick:** Press the power button on the HRI's joystick for two seconds until it turns on.

Turning off *ANYmal*
~~~~~~~~~~~~~~~~~~~~

#. **Turning off ANYmal:**
    #. Make sure that the main body of *ANYmal* is lying on the ground! *ANYmal* will power off its legs.
    #. *The Soft Way:* Press the `PWR button` for about **5 seconds**. The light will start flashing in an interval of one second. Release it as soon as the light starts flashing faster. The system will start a safe shutdown. Complete power off is reached when the fans stop turning after roughly 45 seconds.
    #. *The Hard Way:* Press the `PWR button` for about **10 seconds** and release it when it starts flashing faster. Ignore the fast flashing of the light at 5 seconds. The system will immediately power off.
#. **Turning off the HRI's remote controller:** Press the power button on the HRI's remote controller.

.. _battery_charging_b200:

Battery Charging
~~~~~~~~~~~~~~~~

The battery can be re-charged while *ANYmal* is operating. To avoid sparks on the connector, turn off the charger before plugging in *ANYmal*. For best results, use only the charger that comes with *ANYmal*.

.. warning::

  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  - **The battery should always be charged at temperatures above freezing.**
  - **The battery management system will not charge the battery due to safety reasons if the temperature is higher than 45°C.**

To detect if the charger is running correctly listen for the fan running inside and look at the indicator light on the side of the charger. During the charging procedure the LED glows red. If the robot is fully charged it turns green.

When it is planned to pause operation of the robot for a longer period of time the optimal way of storing *ANYmal* is by having the battery charged to 50%. This also applies in case of its transportation.

.. _operation_emergency_stopping_b200:

Emergency Stopping
~~~~~~~~~~~~~~~~~~

*ANYmal* has two **E-Stop buttons** for emergency stopping:

- **Vehicle E-Stop:** red button on the top of *ANYmal*.
- **Controller E-Stop:** red button on the HRI's remote controller.

This `E-Stop buttons` trigger a **Hard Emergency Stop**, which immediately powers off the actuators of legs by interrupting the main power line to the legs. Devices like computers and network remain powered.


The remote control and the operator GUI have an E-Stop button, which triggers a **Soft Emergency Stop**, which switches the active high-level controller to an `emergency stop controller`. By default, the high-level controller switches to the controller `anymal_ctrl_emcy_freeze <https://bitbucket.org/leggedrobotics/anymal_highlevel_controller>`__, which freezes all joint actuators. That means that the motor speed is regulated to zero and the robot does not collapse immediately, but can eventually fall to the side.

.. warning::

  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50%
    :align: left

  Every controller can use a different custom-made emergency stop controller, which can be more suited for the given controller, but can also lead to unexpected behavior for the operator.

.. important::

   In doubt, always press the **Hard E-Stop button** on the remote control!

Starting the Software
---------------------

.. note:: Read first section ":ref:`network_b200`" on how to connect to the onboard network of the robot and section ":ref:`operator_pc`" on how to set up the `Operator PC`.

.. _operation_locomotion_pc:

Locomotion PC
~~~~~~~~~~~~~

The following procedure will start the locomotion controller:

#. Login to the `Locomotion PC`:

   .. code:: bash

       ssh integration@anymal-NAME-lpc -X

#. Make sure that the actuators of the robot and the remote controller is powered on, and the E-Stop buttons are released.
#. To run the LPC stack run

   .. code:: bash

     roslaunch anymal_NAME_lpc lpc.launch



Navigation PC
~~~~~~~~~~~~~

#. Make sure that the locomotion PC and, thus, the roscore are running.
#. Login to the `Navigation PC`:

   .. code:: bash

       ssh integration@anymal-NAME-npc -X


#. To start the actuated LIDAR, the localization and mapping, run

   .. code:: bash

       roslaunch anymal_NAME_npc npc.launch

Operator PC
~~~~~~~~~~~

#. Make sure that the `Locomotion PC` and, thus, the roscore are running.
#. Make sure that you have correctly set up ROS  on the `Operator PC` according to section ":ref:`operator_pc_setup`".
#. To launch the graphical user interface, run

  .. code:: bash

       roslaunch anymal_NAME_opc opc.launch

Running the Robot
-------------------

Running Controllers
~~~~~~~~~~~~~~~~~~~

.. caution::
    .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

    **Before running a controller, make sure that ANYmal is touching the ground with the feet, its legs are in the default joint position configuration, and that the state estimator is operational. Failing to do so may lead in erratic behavior of ANYmal!**

.. caution::
    .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

    **The remote controller turns off after some minutes if the driver on the locomotion PC is not running. If the remote controller turns off, the actuators will turn off and the robot will eventually collapse.**


.. _fig_ll_commands:

.. figure:: images/ll_commands.png

     Three low-level controller commands: go_zero, go_default and go_rest.


To start a controller (other than the `freeze` controller) choose one of the following methods:

**Starting from default joint positions:**

Use this method if you have a way to hang up the robot.

#. To put *ANYmal's* legs into the *default* joint position configuration, use the `Go Default` command in the user interface on the Operator PC or on the remote control. The robot will change its configuration to the one depicted in the middle of :numref:`fig_ll_commands`.

#. Put down *ANYmal* such that it stands on *all four feet*.

#. Confirm that the state estimtor is *operational*. If necessary, use the `reset origin`/`reset here` command in the user interface on the Operator PC or on the remote control.

#. Start the desired controller user interface on the Operator PC or from the remote control.

**Starting from rest joint positions:**

Use this method if you would like to start the robot from the ground.

#. To put *ANYmal's* legs into the *rest* joint position configuration, use the `Go Rest` command in the user interface on the Operator PC or on the remote control. The robot will change its configuration to the one depicted on the right of :numref:`fig_ll_commands`.

#. Put down *ANYmal* such that it lies on the *protective belly plate*.

#. Confirm that the state estimtor is *pre-operational*. If necessary, use the `reset origin`/`reset here` command in the user interface on the Operator PC or on the remote control.

#. Start the `free_gait_impedance_ros` controller from the user interface on the Operator PC or from the remote control.

#. Select the `stand_up` action to have *ANYmal* perform a standup maneuver.

#. Start the desired controller user interface on the Operator PC or from the remote control.

.. note::

    The only controller that can be started from another joint position than default is **free_gait_impendance_ros**. This controller can be started from a rest configuration on the protective belly plate and from any configuration with at least three legs touching the ground. When starting the **free_gait_impendance_ros** controller, it keeps the current pose of the base and the legs of *ANYmal* until an action is triggered. When starting from a non-standard configuration, it's your responsibility to send the appropriate commands that can safely transition from the current configuration to the desired motion!

Steering the Robot
~~~~~~~~~~~~~~~~~~~

Read section ":ref:`remote_controller`" to learn how to steer the robot with the remote controller.
