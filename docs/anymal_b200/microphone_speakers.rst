.. _microphone_speaker:

Microphone & Speaker
=====================

ANYmal has integrated both a microphone and a speaker to allow for audio communication.


Microphone
----------

The microphone is of the type **AXIS T8351 Microphone 3.5mm**. As it is waterproof, it can be mounted outside of the main body, as a part of the perception module. Plug it in the audio input jack on the top plate to connect it to the LPC.

.. _fig_microphone:

.. figure:: images/microphone.png
   :width: 14cm
   :alt: microphone
   :align: center

   AXIS T8351 Microphone 3.5mm.

Speaker
-------
The speaker is a modified version of the type **LC Prime® Resonance Vibration Speaker Music Player**. It is mounted on the inside of the main body side plate and connected to the LPC audio output jack.


Software
--------
To configure the two devices (e.g. change volume etc.) use tools that come with the operating system such as alsamixer on the LPC via ssh.
