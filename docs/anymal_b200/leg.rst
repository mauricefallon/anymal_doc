.. _legs_b200:

Legs of *ANYmal*
================

.. _joint_actuators_b200:

*ANYmal* has four legs, *left front*, *right front*, *left hind*, and *right hind*, as shown in :numref:`fig_leg_labelling_b300`. The front of the robot is the side where the depth camera is attached to the main body.

.. _fig_leg_labelling_b300:

.. figure:: images/leg_labeling.jpg
    :scale: 50 %
    :alt: Leg labelling
    :align: center

    Labelling and placement of the legs of ANYmal.

Leg structure
-------------

Leg components
~~~~~~~~~~~~~~

The *left front* and the *right hind* legs are identical in terms of component and assembly. Also, the *right front* and the *left hind* legs are identical in terms of components. :numref:`fig_leg_full_b200` highlights the main components of an ANYmal leg, at the example of a *left front*/*right hind* leg.

.. _fig_leg_full_b200:

.. figure:: images/leg_full.png
    :scale: 50 %
    :alt: Leg components
    :align: center

    Components of an *ANYmal* leg. In this picture, a *left front*/*right hind* leg is shown.

The difference between *left front*/*right hind* and *right front*/*left hind* leg lies in the way the foot is attached
to the shank and can be seen in :numref:`fig_left_right_leg_b200`.

.. _fig_left_right_leg_b200:

.. figure:: images/left_right_leg.jpg
    :scale: 50 %
    :alt: Left front/right hind and right front/left hind leg
    :align: center

    *Left front*/*right hind* and *right front*/*left hind* leg of *ANYmal*.


Actuators
~~~~~~~~~

The legs are actuated by the series-elastic actuators *ANYdrive*
SEA-90-48-A-ER/EF/ET.
More information about these actuators can be found in the
`user manual <https://anybotics-anydrive-doc.readthedocs-hosted.com>`_.

Each leg contains three *ANYdrives*.
They are listed in :numref:`tab_actuators`, starting with the one closest to the
main body.

.. _tab_actuators:

.. table:: Actuator Symbols

  ============  =======================
  Abbreviation  Description
  ============  =======================
  HAA  drive    Hip adduction/abduction
  HFE  drive    Hip flexion/extension
  KFE  drive    Knee flexion/extension
  ============  =======================

See as well :ref:`model` for the naming of the joints and the links.

Cabling
~~~~~~~

Each leg of *ANYmal* contains five different cables. They are listed in
:numref:`tab_cables_b200`, starting with the one closest to the main body.

.. _tab_cables_b200:

.. table:: Leg Cables

    +-----------------------------------+
    |  Cable                            |
    +===================================+
    | *12V fan power cable tree*        |
    +-----------------------------------+
    | *48V power cable tree*            |
    +-----------------------------------+
    | *main body to HAA EtherCAT cable* |
    +-----------------------------------+
    | *HAA to HFE EtherCAT cable*       |
    +-----------------------------------+
    | *HFE to KFE EtherCAT cable*       |
    +-----------------------------------+

Signal Outage
~~~~~~~~~~~~~

If an *ANYdrive* loses the EtherCAT signal (e.g. PC shutdown) it switches
into the error-state.
It will remain in that state until the signal is re-established.
Using *clear* and *enable actuators* will bring the actuators into control-OP
state.

While in the error state the *ANYdrive* controller activates the freeze mode.
In this mode the actuator will control the velocity to zero and maintain its
current position.

.. important::
    .. image:: ../images/iso_7010/warning_electricity.png
      :scale: 50 %
      :align: left

    **The** *ANYdrive* **is still powered in the error mode.**
    **Do not expose any electronics of the robot.**


.. _assembling_leg_b200:

Assembling a Leg
----------------

.. _mounting_haa_drive_b200:

Mount the whole assembled leg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*General notes*

- The leg attachment works best when ANYmal is lying on the side.

- The hip braces have a tooth-like geometric feature which allows for rotational
  positioning of the HAA ANYdrives. The HAA anydrives feature several grooves in
  which the tooth of the hip brace can engage as shown in
  :numref:`fig_haa_mounting_2_b200`. This allows the HAA to be automatically fixed with
  the right mechanical calibration with respect to the body.

  .. _fig_haa_mounting_2_b200:

  .. figure:: images/haa_mounting_2.png
      :scale: 100 %
      :alt: HAA drive groove and hip
      :align: center

      HAA drive groove and hip.

*Attachment steps for all four legs*

- Slide the rear drive clamp over the back of the drive
- Thread the front drive clamp over the front of the drive as shown in
  :numref:`fig_haa_mounting_3_4_5_b200`.

  .. _fig_haa_mounting_3_4_5_b200:

  .. figure:: images/haa_mounting_3_4_5.png
      :scale: 100 %
      :alt: Mounting the clamps
      :align: center

      Mounting the clamps.

- Route the leg cables between trunk and hip braces as shown in
  :numref:`fig_haa_mounting_6_b200`.

  .. _fig_haa_mounting_6_b200:

  .. figure:: images/haa_mounting_6.png
      :scale: 100 %
      :alt: HAA cabling
      :align: center

      HAA cabling.

- Turn the HAA drive about 9° inwards until you feel the the tooth of the hip brace
  engaging into one groove of the HAA drive.
- Wiggle the drive slightly clockwise/counter-clockwise to ensure that the groove is
  really centered on the tooth as shown in :numref:`fig_haa_mounting_8_b200` and
  :numref:`fig_haa_mounting_7_b200`.

  .. _fig_haa_mounting_8_b200:

  .. figure:: images/haa_mounting_8.png
      :scale: 100 %
      :alt: Correct position of HAA drive in front view.
      :align: center

      Correct position of HAA drive in front view.


  .. _fig_haa_mounting_7_b200:

  .. figure:: images/haa_mounting_7.png
      :scale: 100 %
      :alt: Correct position of HAA drive
      :align: center

      Correct position of HAA drive.

- Apply loctite to a galvanized steel screw M4x16, 12.9 and fix the rear clamp on the
  rear brace using the **bottom** screw hole.
- Turn the screw in until you feel the resistance rising from clamp and brace touching
  each other as shown in :numref:`fig_haa_mounting_10_bottom_b200`.
- Then loosen the screw again for one full turn. The rear clamp should be loose now
  and a gap of 0.7mm should occur between clamp and brace.

  .. _fig_haa_mounting_10_bottom_b200:

  .. figure:: images/haa_mounting_10_bottom.png
      :scale: 100 %
      :alt: Mounting the rear bottom screw
      :align: center

      Mounting the rear bottom screw.


- Apply loctite to a galvanized steel screw M4x16, 12.9 and fix the rear clamp on the
  rear brace using the **top** screw hole as shown in :numref:`fig_haa_mounting_10_top_b200`.
- Tighten this crew on the top screw hole to 2.2Nm. After tightening, a small gap
  between clamp and brace should remain.
- Tighten the screw on the bottom screw hole also
  to 2.2Nm. After tightening, a small gap between clamp and brace should remain.

  .. _fig_haa_mounting_10_top_b200:

  .. figure:: images/haa_mounting_10_top.png
      :scale: 100 %
      :alt: Mounting the rear top screw
      :align: center

      Mounting the rear top screw.


- Repeat these last six steps for front clamp.

*Attachment steps for right front/left hind legs*

- Plug in Fan power (Lemo 0T 6-pole) and leg power (Lemo 2T 4-pole) to the trunk.
- Plug in drive power (Lemo 0T 2-pole) to the drive.
- Plug in leg EtherCAT to the center socket of the drive.
- Plug in trunk EtherCAT to the EC1 socket of the drive.
- Plug in EtherCAT to the trunk. It should now look like :numref:`fig_haa_mounting_11_b200`.

*Attachment steps for left front/right hind legs:*

- Plug in Fan power (Lemo 0T 6-pole) and leg power (Lemo 2T 4-pole) to the trunk.
- Plug in trunk EtherCAT to the EC1 socket of the drive.
- Plug in EtherCAT to the trunk.
- Plug in drive power (Lemo 0T 2-pole) to the drive.
- Plug in leg EtherCAT to the center socket of the drive.


  .. _fig_haa_mounting_11_b200:

  .. figure:: images/haa_mounting_11.png
      :scale: 100 %
      :alt: Cabling on a right leg
      :align: center

      Cabling on a right front/left hind leg.


Mount the HFE drive holder
~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Thread the *48V power cable tree* and the *HAA to HFE EtherCAT cable* through
   the output shaft flange of the HFE drive holder.
#. Attach the HFE drive holder on the hollow shaft of the HAA drive. The HFE
   drive holder must point 90° to the side with respect to the output shaft positioning feature (see :numref:`fig_HFE_drive_holder_assembly_b300`).

    .. _fig_HFE_drive_holder_assembly_b300:

    .. figure:: images/HFE_drive_holder_assembly.jpg
        :alt: HFE drive holder mounting
        :align: center

        HFE drive holder angle when mounted on the HAA drive.

#. Add ten **M4x12mm screws** and tighten them with **2.1-2.3Nm** to connect the
   HFE drive holder to the output shaft of the HAA drive. Use thread locker (for
   example Loctite 243) to secure the screws.

Mount the HFE drive
~~~~~~~~~~~~~~~~~~~

#. Plug the *HAA to HFE EtherCAT cable* into the HFE drive.
#. Plug the *HFE branch of the 48V power cable* into the HFE drive.
#. Plug the *HFE to KFE EtherCAT cable* into the HFE drive.
#. Attach the HFE drive to the HFE holder:

   #. The back of the HFE drive has to be flush with the back of the HFE holder.
   #. The key in the back of the HFE holder needs to fit in the one positioning
      feature at the back of the HFE drive which allows the HFE drive connector
      cap to point upwards and inclined 15 degree away from the main body, as
      shown in :numref:`fig_hfe_drive_mounting_b200`.

        .. _fig_hfe_drive_mounting_b200:

        .. figure:: images/hfe_drive_mounting.png
            :scale: 50 %
            :alt: HFE drive mounting
            :align: center

            Mounting angle of the HFE drive.

   #. The *HFE branch of the 48V power cable*, the *HAA to HFE EtherCAT cable*
      and the *HFE to KFE EtherCAT cable* enter the HFE holder through the upper
      opening between HFE drive and holder, as shown in
      :numref:`fig_hfe_drive_cabling_b200`.

      .. _fig_hfe_drive_cabling_b200:

      .. figure:: images/hfe_drive_cabling.jpg
          :scale: 50 %
          :alt: HFE drive cabling
          :align: center

          Cabling on the upper side of the HFE drive.

   #. The *48V power cable tree* and the *HFE to KFE EtherCAT cable* leave the
      HFE holder through the lower opening between HFE drive and holder, as
      shown in :numref:`fig_hfe_drive_cabling_2_b200`.

      .. _fig_hfe_drive_cabling_2_b200:

      .. figure:: images/hfe_drive_cabling_2.jpg
          :scale: 50 %
          :alt: HFE drive cabling bottom
          :align: center

          Cabling on the bottom side of the HFE drive.


#. Mount the two HFE holder clamps with **four M4x16mm screws** and tighten the
   screws with **2.1-2.3Nm**. Use thread locker (for example Loctite 243) to
   secure the screws. For each clamp, first tighten both screws with minimum
   torque and make shure that the gap between clamp and main body brace is
   roughly equal size on both ends of the clamp. Then increse the torque by
   turning both screws alternately quarter a turn until the maximum torque of
   **2.1-2.3Nm** is reached for both screws. The HFE drive pad (see
   :numref:`fig_shoulder_pads_b200`) is thereby glued to the HFE holder clamp
   pointing towards the thigh.
#. Thread the *KFE branch of the 48V power cable* and the *HFE to KFE EtherCAT
   cable* through the hollow shaft of the HFE
   drive.


Mount the Thigh
~~~~~~~~~~~~~~~

#. Thread the *KFE branch of the 48V power cable tree* and the *HAA to HFE
   EtherCAT cable* through the output shaft flange of the thigh.
#. Attach the thigh on the hollow shaft of the HFE drive. With the zero position
   on the output shaft of the HFE drive pointing upwards, the thigh has to point
   downwards.
#. Add **ten Screws M4x12mm** and tighten them with **2.1-2.3Nm** to connect the
   HAA holder to the drive. Use thread locker (for example Loctite 243) to
   secure the screws.

Mount the KFE drive
~~~~~~~~~~~~~~~~~~~

#. Attach the KFE drive to the thigh:

   #. The back of the KFE drive has to be flush with the back of the inner
      thigh clamp as shown in :numref:`fig_KFE_assembly_b300`
   #. The key in the back of the inner thigh needs to fit in the one
      positioning feature at the back of the KFE drive which allows the drive
      connector cap to point upwards inside the thigh.

      .. _fig_KFE_assembly_b300:

      .. figure:: images/KFE_assembly.jpg
          :alt: KFE assembly
          :align: center

          Positioning of the KFE drive in the thigh.


#. Mount the two thigh clamps with **four M4x16mm** screws and tighten the
   screws with **2.1-2.3Nm**. Use thread locker (for example Loctite 243) to
   secure the screws. For each clamp, first tighten both screws with minimum
   torque and make shure that the gap between clamp and main body brace is
   roughly equal size on both ends of the clamp. Then increse the torque by
   turning both screws alternately quarter a turn until the maximum torque of
   **2.1-2.3Nm** is reached for both screws.
#. Plug the *KFE branch of the 48V power cable tree* into the KFE drive.
#. Plug the *HFE to KFE EtherCAT cable* into the KFE drive outer port.
#. Plug the blind cap to seal the center port.
#. Close the front thigh cover with **eight M3x6mm screws** and
   **eight 7mm washers** (see :numref:`fig_thigh_screws_b200`) and tighten the
   screws with **1.2-1.3Nm**. Use thread locker (for example Loctite 243) to
   secure the screws.
#. Attach the shoulder pad with **four M3x10mm screws** (see
   :numref:`fig_thigh_screws_b200`) and tighten them with **1.2-1.3Nm** as shown
   right in :numref:`fig_shoulder_pads_b200`. Use thread locker (for example
   Loctite 243) to secure the screws.
#. Attach the knee thigh cover with **two M3x10mm** screws and tighten them with
   **1.2-1.3Nm**. Use thread locker (for example Loctite 243) to secure the
   screws.

   .. caution::
     .. image:: ../images/iso_7010/warning_general.png
       :scale: 50%
       :align: left

   **Never put any load on the legs when the front thigh covers are open. The front thigh cover is an integral part of the thigh structure. Only with the thigh cover attached and all twelve M3 screws properly tightened, the thigh is able to properly bear any loads.**

   .. _fig_thigh_screws_b200:

   .. figure:: images/thigh_screws.png
       :alt: Thigh screws
       :align: center

       Thigh screws.

   .. _fig_shoulder_pads_b200:

   .. figure:: images/shoulder_pads.jpg
       :width: 10cm
       :alt: shoulder pads
       :align: center

       Shoulder pads.


Mount the shank
~~~~~~~~~~~~~~~

#. Attach the shank on the output shaft of the KFE drive. With the zero position
   on the output shaft of the KFE drive pointing upwards, the shank has to be
   positioned horizontally, pointing away from the robot center.
#. Add **five M4x12mm screws** and tighten them with **2.1-2.3Nm** to connect
   the shank to the drive. Use thread locker (for example Loctite 243) to secure
   the screws.

Mount the foot
~~~~~~~~~~~~~~

The mounting procedure of the foot is described in :ref:`feet_b200`.

Calibrate the ANYdrives
~~~~~~~~~~~~~~~~~~~~~~~
The ANYdrives' absolute zero position offset need to be calibrated for the legs.

#. Connect the robot to the safety harness and let it hang in the air or put the robot with the main body on the ground.
#. Power up the robot and the actuators.
#. Launch the software on LPC. See section ":ref:`operation_locomotion_pc`" for instructions.
#. Stop the software as soons as the ANYdrives have been initialized and configured.
#. Download the ANYdrive Studio (anydrive_studio.AppImage) from support.anybotics.com to the locomotion PC.
#. Start the ANYdrive studio and load the setup.yaml from anymal_b_common/config/anydrive/setup.
#. Calibrate the joint position homing of the ANYdrives. The HAA drives have an offset of 9° = 0.1571 rad, the HFE an offset of 15° = 0.2618 rad, and the KFE have no offset. Set the values of :numref:`tab_anydrive_offsets` with the ANYdrive studio relative to the *factory calibration*.


.. important::

   Note that the LPC software configures temporarily the turning direction of the drives. :numref:`tab_anydrive_offsets` is only correct if the turning direction has been configured correctly. If you re-boot a drive, the turning direction could be wrong. In that case, restart the LPC software.

.. _tab_anydrive_offsets:

.. table:: Homing offsets of ANYdrives


 +-------------+------------------------------------------------+
 | Joint       | Relative Gear and Joint Encoder Homing [rad]   |
 +=============+================================================+
 | LF_HAA      | +0,1571                                        |
 +-------------+------------------------------------------------+
 | LF_HFE      | +0,2618                                        |
 +-------------+------------------------------------------------+
 | LF_KFE      | 0,0                                            |
 +-------------+------------------------------------------------+
 | RF_HAA      | -0,1571                                        |
 +-------------+------------------------------------------------+
 | RF_HFE      | -0,2618                                        |
 +-------------+------------------------------------------------+
 | RF_KFE      | 0,0                                            |
 +-------------+------------------------------------------------+
 | LH_HAA      | -0,1571                                        |
 +-------------+------------------------------------------------+
 | LH_HFE      | -0,2618                                        |
 +-------------+------------------------------------------------+
 | LH_KFE      | 0,0                                            |
 +-------------+------------------------------------------------+
 | RH_HAA      | +0,1571                                        |
 +-------------+------------------------------------------------+
 | RH_HFE      | +0,2618                                        |
 +-------------+------------------------------------------------+
 | RH_KFE      | 0,0                                            |
 +-------------+------------------------------------------------+



Disassembling a leg
-------------------

Disassembling a leg is mainly the reverse process of assembling, as described in
:ref:`assembling_leg_b200`, starting with the foot and ending with the HAA
drive. In the following section, the differences between the steps of assembling
and disassembling are described.

Demount the shank, thigh, and HFE drive holder
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For demounting the shank, thigh, and HFE drive holder aluminum bodies from the
output shaft of their drives, the following procedure has to be applied:

#. Remove the **five** (shank) or **ten** (thigh and HFE drive holder)
   **M4x12 screws**.
#. Insert two M4 screws made of plastic to protect the ANYdrive from abrasion in the two M4 demounting threads in the aluminum body
   (see :numref:`fig_unmounting_thigh_b200` for the thigh) to push the aluminum
   body out of the output thaft of its drive.

   .. _fig_unmounting_thigh_b200:

   .. figure:: images/unmounting_thigh.png
       :alt: unmounting threads thigh
       :align: center

   M4 Demounting threads in the thigh.


Adding / removing a leg from the main body
------------------------------------------

To add a leg to the main body, connect the HAA drive as described in
:ref:`mounting_haa_drive_b200` and connect the  *HAA to HFE EtherCAT cable* and
the *48V power cable tree*. To remove a leg from the main body, disconnect the
three cables and remove the HAA drive.
