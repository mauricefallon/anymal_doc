.. _feet_b200:

Feet of *ANYmal*
================

The foot of *ANYmal* is shown in :numref:`fig_foot_b200` and contains a rubber base connected to a carbon tube.

.. _fig_foot_b200:

.. figure:: images/foot.png
    :align: center

    Rubber foot with carbon tube.


Connecting the foot to the shank
--------------------------------
.. figure:: images/shank_clamp_screws.png
    :align: center
    :alt: shank clamp screws

    The clamps that attach the foot tube to the shank.


#. Thread the two shank clamps on the foot carbon tube.
#. Apply a thin layer of carbon assembly compount (for example Muc-Off Carbon Gripper) on the foot tube contact surfaces at the shank.

    .. figure:: images/foot_tube_contact_surfaces.png
        :align: center
        :alt: foot tube contact surface

        The contact surface underneath the foot tube.

#. Position the carbon tube on the shank. Add **four M3x12mm screws** to connect the clamps with the shank.
#. Make sure that the foot end stop touches the shank and the groove in the foot end stop shows away from the shank.
#. Slightly fasten the four clamp screws to hold the foot tube in position.
#. Fasten the **four M3x12mm clamp screws** with **0.9-1.1Nm**.

    .. warning::
        .. image:: ../images/iso_7010/warning_general.png
            :scale: 50%
            :align: left

      **Exceeding 1.3Nm on the shank clamp screws may crush the foot carbon tube.**

#. Attach the knee protector with **four M3x12mm screws** with **0.9-1.1Nm**.

  .. figure:: images/foot_kfe_protector.png
      :align: center
      :alt: knee protector

      The outer protector of the KFE drive.


Disconnecting the foot from the shank
-------------------------------------

#. Remove the outer knee protector by removing its four screws.
#. Loosen the four screws of the shank clamps.
#. Remove the foot from the shank.
#. Remove the shank clamps from the foot carbon tube.
