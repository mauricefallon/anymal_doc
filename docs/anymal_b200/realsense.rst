.. _realsense_b200:

Depth Camera
============

The depth camera mounted on *ANYmal* is an Intel Realsense ZR300.
Download the `product datasheet <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b200/documents/intel_realsense_zr300_product_datasheet.pdf>`__ for information regarding specifications.


:numref:`fig_realsense` shows the device installed in the front of *ANYmal*.

.. _fig_realsense:

.. figure:: images/realsense.jpg
   :width: 10cm
   :alt: realsense
   :align: center

   Intel Realsense ZR300 mounted on ANYmal.


.. caution::
  .. image:: ../images/iso_7010/warning_electricity.png
    :scale: 50 %
    :align: left

  - **The battery ground is exposed at the chassis of the realsense!**
  - **It must be isolated from the main body chassis by the mount.**
  - **Do not bypass the isolation by touching the Realsense.**

.. warning::
  .. image:: ../images/iso_7010/warning_general.png
    :scale: 50 %
    :align: left

  **The Realsense depth camera is not waterproof.**

  |


Installation
------------

Hardware
~~~~~~~~

#. Mount the Intel Realsense ZR300 on the intended frame at the front of *ANYmal* using two M2.5 x 10 mm screws and appropriate washers.

#. Adjust the frame's tilt angle to your needs.
   There is an indicator printed on the frame where you can read the current angle.

#. Connect the device to the left lower USB socket on the front hatch using a USB 3.0 cable.

Software
~~~~~~~~

The Intel Realsense ZR300 requires a special driver which has to be installed manually.
`Intel's official repository <https://github.com/IntelRealSense/librealsense>`__ contains a `documentation <https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md>`__ on how to install it.
Read and follow the instructions carefully since the installation requires modifications to the Linux Kernel.

The following setup is confirmed to be working on *ANYmal*:

   - Install Kernel 4.8.0-58 with lowlatency patch

    .. code:: bash

        sudo apt install linux-headers-4.8.0-58-lowlatency linux-image-4.8.0-58-lowlatency

   - Use the :code:`legacy` branch of librealsense

After the installation of the driver, clone the `realsense <https://bitbucket.org/leggedrobotics/realsense>`__ and
`anymal_navigation <https://bitbucket.org/leggedrobotics/anymal_navigation>`__ repositories into your workspace.
Run

    .. code:: bash

        catkin build anymal_realsense

Calibration
-----------

#. Clone the `repository <https://github.com/ethz-asl/kinect_tilt_calibration>`__ containing the calibration routine to the NPC's catkin workspace and build it by running

    .. code:: bash

        catkin build kinect_tilt_calibration

#. Run *ANYmal* locomotion and the navigation PCs.

   #. On LPC:

       .. code:: bash

           roslaunch anymal_NAME_lpc lpc.launch

   #. On NPC:

       .. code:: bash

           roslaunch anymal_NAME_npc npc.launch

   #. Place *ANYmal* in the Default configuration on a flat terrain without any obstacles in the field of view of the depth sensor.

#. Run the calibration as follows:

   #. Edit the :code:`kinect_tilt_calibration/launch/example.launch` file:
      Change the topic to :code:`/realsense_zr300/points2`, and the frame to :code:`realsense_zr300_camera`.

   #. Run the calibration algorithm on the NPC and read the output angle (:code:`pitch deg`) in the console:

       .. code:: bash

           roslaunch kinect_tilt_calibration example.launch

   #. Open :code:`anymal_NAME/anymal_NAME_description/urdf/anymal.urdf.xacro` and set the :code:`mounted_angle_degree` appropriately.

Software
--------

The :code:`anymal_realsense` package in the `anymal_navigation <https://bitbucket.org/leggedrobotics/anymal_navigation>`__ repository provides a launch file which connects to the Realsense.

Troubleshooting
---------------

If the installation was successful but the communication fails when launching the driver, try using a shorter USB 3.0 cable.
As the Intel Realsense ZR300 uses practically the complete bandwidth of USB 3.0, the connection cable should be short and of good quality.
