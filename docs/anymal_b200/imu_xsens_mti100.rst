
.. _imu_b200:

Inertial Measurement Unit (IMU)
===============================

The robot is equipped with a **Xsens MTi 100**, which is rigidly attached to the stiff frame of the main body.
It is highlighted in orange in :numref:`figXsensMTi_b200`.

.. _figXsensMTi_b200:

.. figure:: images/imu.jpg
    :width: 16 cm
    :alt: IMU
    :align: center

    Xsens MTi 100 in ANYmal main body.

The sensor measures

- linear accelerations, and
- angular velocities

at 400 Hz. Note that the IMU can also estimate the orientation, but at a lower rate and the firmware needs to be reconfigured with the tool of Xsens running on Windows.
More information can be found in the `user manual <https://bitbucket.org/leggedrobotics/anymal_doc/raw/master/docs/anymal_b100/documents/IMU_Xsens_MTi_100_User_Manual.pdf>`__.
