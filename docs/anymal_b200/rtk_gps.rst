.. _rtk_gps:

Real Time Kinematic GPS
=======================

ANYmal has the capability to receive RTK GPS signals.
The antenna can be mounted to the top plate of the main body. It has to be connected via the top hatch connectors.


Antenna mounting
----------------

Base station
------------

Software repositories
---------------------
There exists an extensive wiki for the software used both on the NPC and on the base station:
`rtk\_gps\_wiki`_


Piksi Multi
-----------
`piksi\_multi`_



.. _piksi\_multi: https://support.swiftnav.com/customer/en/portal/topics/961591-piksi-multi/articles
.. _rtk\_gps\_wiki: https://github.com/ethz-asl/ethz_piksi_ros/wiki
