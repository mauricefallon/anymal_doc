.. _safety_instructions_b200:

Safety
======


Important Safety Instructions
-----------------------------

These instructions are intended for qualified and trained technical personnel.
Prior commencing with any activities,

- this manual must be read carefully and understood, and
- the instructions given therein must be followed.


In addition to the instructions in the next sections, any regulations applicable in the country and/or at the site of implementation with regard to health and safety/accident prevention and/or environmental protection must be observed!


General
~~~~~~~

.. danger::
   .. image:: ../images/iso_7010/warning_fire.png
      :scale: 50%
      :align: left

   **In case of fire,**

   |

   -  let the fire department fight fires,
   -  use a **fire extinguisher with water and F-500 EA** to fight **Li-ION battery fires**, and 
   -  use a **Class B fire extinguisher**, preferrably a Carbon Dioxide, for **burning electronics**.

 
.. danger::
   .. image:: ../images/iso_7010/warning_hand.png
      :scale: 50%
      :align: left

   -  **Always keep a safety distance between the robot and any person around.**
   -  **Keep hands clear while the robot is moving to avoid injury.**

.. danger::
    .. image:: ../images/iso_7010/warning_general.png
       :scale: 50%
       :align: left

    **Be aware of fast moving parts (especially the legs) and parts coming off the robot in the case of a crash.**
   
.. caution::
   .. image:: ../images/iso_7010/warning_hand.png
      :scale: 50%
      :align: left

   -  **Do not touch the cooling fan.**
   -  **Do not touch the heat sinks or the heat pipes (cuts and burns).**
   -  **Do not touch the actuators during or shortly after operation (burns).**


.. caution::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **While the robot is powered, do not leave the robot unattended, and keep a hand on the remote E-Stop button when it is moving.**


.. important::
   .. image:: ../images/iso_7010/mandatory.png
      :scale: 12%
      :align: left

   **Always wear safety gloves, boots, overall and goggles when operating the robot for your own safety.**


Electrical System
~~~~~~~~~~~~~~~~~
   
.. danger::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **Do not remove the Battery Management System (BMS) from the battery.**
   **The BMS protects the battery in case of short-circuiting, because short-circuiting the battery will cause the cells to overheat and possibly to catch fire.**

.. warning::
   .. image:: ../images/iso_7010/warning_general.png
      :scale: 50%
      :align: left

   **Please follow these instructions for storing the Lithium ion battery:**

   |

   -  Store the battery at room temperature and charged with half of its capacity.
   -  Do not expose battery pack to direct sunlight (heat) for extended periods.
   -  Store the battery separately from anything hazardous, such as explosives, combustibles, or any other highly flammable material.

   
.. caution::
   .. image:: ../images/iso_7010/warning_fire.png
     :scale: 50 %
     :align: left

   **When feasible, do not leave a battery charging unattended in the event that the battery is damaged and can become unstable, thus overheating.**


.. important::
   .. image:: ../images/iso_7010/warning_electricity.png
     :scale: 50 %
     :align: left

   -  **The battery ground is exposed at the case of the LiDAR Unit, the antennas and the case of the Depth Camera! Do not touch or make contact with these surfaces.**



.. important::
  .. image:: ../images/iso_7010/warning_electricity.png
    :scale: 50 %
    :align: left

   -  **Do not expose the electronics of your robot, its battery, or the charger.**
   -  **Cover the power socket on the robot when the charger is unplugged.**
   -  **The battery management system will protected the battery in case of a short-circuit of the main power bus. Nevertheless, electrical components can get damaged.**

.. important::
   .. image:: ../images/iso_7010/warning_electricity.png
     :scale: 50 %
     :align: left

   -  **Please ensure voltage rating for charger matches standard outlet voltage.**


.. _designated_use_b200:

Designated Use
--------------
*ANYmal* is intended exclusively for use in accordance with the properties described in this documentation.
The manufacturer cannot be held liable for any damage resulting from such use. The risk lies entirely with the user.
Section ":ref:`safety_instructions_b200`" and section ":ref:`designated_use`" must be complied with.
The applicable national laws, regulations, standards and guidelines must be observed and complied with.
*ANYmal* may only be used for research.


Liability
---------
*ANYmal* has been designed, built, and programmed using state-of-the-art technology and in accordance with the recognized safety rules. Nevertheless, improper installation of this system or its employment for a purpose other than the intended one may constitute a risk to life and limb of operating personnel or of third parties, or cause damage to or failure of the entire robot system and other material property.

*ANYmal* may only be used in technically fault-free condition in accordance with its designated use and only by safety-conscious persons who are fully aware of the risks involved in its operation. Use must be carried out in compliance with this documentation.
