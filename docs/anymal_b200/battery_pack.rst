.. _battery_pack_b200:

Battery Pack
============

The `battery pack` consists of a battery made of joint Lithium ion (Li-ION) cells and a Battery Management System (BMS). The BMS is protecting the battery from operating outside its safe operating area, is monitoring the battery's state, and balances the cells.

.. important::

  .. image:: ../images/iso_7010/mandatory_manual.png
    :scale: 50%
    :align: left

  * **Carefully read first** :numref:`safety_instructions_b200` **before you handle the battery.**

  |


.. _installing_battery_pack_b200:

Installing the Battery Pack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Make sure that the robot is not powered and not connected to any external device (including the charger).

#. Unscrew and open the front hatch as depicted in the middle image in :numref:`fig_battery_pack_b200`.

#. Slide in the battery case with the label ``This side up for operation`` on top until it is connected with the robot.

#. Close the hatch and tighten the **M4x12 screws** with 2 - 2.3 Nm.


.. _disconnecting_battery_pack_b200:

Disconnecting the Battery Pack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Make sure that the robot is not powered and not connected to any external device (including the charger).

#. Unscrew and open the front hatch.

#. Pull out the battery case as shown in the right image in :numref:`fig_battery_pack_b200`.

#. Optional: To store the battery case without connecting it inside the robot (e.g. for transportation) turn it upside down and slide it back in. The label on top should now read ``This side up for transport``.

#. Close the hatch.

.. _fig_battery_pack_b200:

.. figure:: images/battery.png
    :width: 18cm
    :align: center

    The battery pack in ANYmal shown with a closed hatch, an open hatch and pulled out.
